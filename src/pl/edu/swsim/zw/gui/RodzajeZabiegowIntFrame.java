package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.RodzajeZabiegowRep;
import pl.edu.swsim.zw.entities.RodzajZabieguEnt;

/**
 * Lista rodzajów zabiegów
 *
 */
public class RodzajeZabiegowIntFrame extends JInternalFrame {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private RodzajeZabiegowRep repository = new RodzajeZabiegowRep();

	/**
	 * Sortowanie
	 */
	private final String[] sortOptions = {
			"Nazwa rosnąco [a-z]",
			"Nazwa malejąco [z-a]",
			"Rodzaj rosnąco [a-z]",
			"Rodzaj malejąco [z-a]",
			"Cena rosnąco [min-max]",
			"Cena malejąco [max-min]",
			"Refundacja rosnąco [tak-nie]",
			"Refundacja malejąco [nie-tak]",
			"Punkty rosnąco [min-max]",
			"Punkty malejąco [max-min]",
			"Skierowanie rosnąco [tak-nie]",
			"Skierowanie malejąco [nie-tak]",
			// ▲ ▼ - nie dziala już
	};
	
	private JTextField tfSearch;
	private JTable table;
	private JComboBox<String> cbOrder;


	/**
	 * Create the frame.
	 */
	public RodzajeZabiegowIntFrame() {
		
		// szybkie obejście this - klasy anonimowe, 
		// nadal dostęp do tej klasy przez that
		RodzajeZabiegowIntFrame that = this;
		
		//logger.info(getBorder());
		
		setMaximizable(true);
		setTitle("Rodzaje Zabiegów");
		setResizable(true);
		setClosable(true);
		setBounds(100, 100, 643, 398);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut_1, BorderLayout.EAST);
		
		Component verticalStrut = Box.createVerticalStrut(3);
		panel.add(verticalStrut, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnR = new JButton("");
		btnR.setIcon(new ImageIcon(RodzajeZabiegowIntFrame.class.getResource("/pl/edu/swsim/zw/icon/refresh2.png")));
		btnR.setMargin(new Insets(0, 2, 0, 2));
		btnR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		GridBagConstraints gbc_btnR = new GridBagConstraints();
		gbc_btnR.insets = new Insets(0, 0, 0, 5);
		gbc_btnR.gridx = 0;
		gbc_btnR.gridy = 0;
		panel_1.add(btnR, gbc_btnR);
		
		JLabel lblSortujWedug = new JLabel("Sortuj wg");
		lblSortujWedug.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSortujWedug = new GridBagConstraints();
		gbc_lblSortujWedug.insets = new Insets(0, 0, 0, 5);
		gbc_lblSortujWedug.anchor = GridBagConstraints.EAST;
		gbc_lblSortujWedug.gridx = 1;
		gbc_lblSortujWedug.gridy = 0;
		panel_1.add(lblSortujWedug, gbc_lblSortujWedug);
		
		
		cbOrder = new JComboBox<String>();
		cbOrder.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cbOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex()); // ------------- set Ordersort
			}
		});
		cbOrder.setModel(new DefaultComboBoxModel<String>(sortOptions));
		GridBagConstraints gbc_cbOrder = new GridBagConstraints();
		gbc_cbOrder.insets = new Insets(0, 0, 0, 5);
		gbc_cbOrder.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbOrder.gridx = 2;
		gbc_cbOrder.gridy = 0;
		panel_1.add(cbOrder, gbc_cbOrder);
		
		JLabel lblSzukaj = new JLabel("Szukaj");
		lblSzukaj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSzukaj = new GridBagConstraints();
		gbc_lblSzukaj.insets = new Insets(0, 0, 0, 5);
		gbc_lblSzukaj.anchor = GridBagConstraints.EAST;
		gbc_lblSzukaj.gridx = 3;
		gbc_lblSzukaj.gridy = 0;
		panel_1.add(lblSzukaj, gbc_lblSzukaj);
		
		tfSearch = new JTextField();
		tfSearch.setFont(new Font("Tahoma", Font.PLAIN, 13));
		tfSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
		        	setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		GridBagConstraints gbc_tfSearch = new GridBagConstraints();
		gbc_tfSearch.insets = new Insets(0, 0, 0, 5);
		gbc_tfSearch.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfSearch.gridx = 4;
		gbc_tfSearch.gridy = 0;
		panel_1.add(tfSearch, gbc_tfSearch);
		tfSearch.setColumns(10);
		
		JButton btnDodaj = new JButton("");
		btnDodaj.setIcon(new ImageIcon(RodzajeZabiegowIntFrame.class.getResource("/pl/edu/swsim/zw/icon/add.png")));
		btnDodaj.setMargin(new Insets(0, 2, 0, 2));
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewItem();
			}
		});
		GridBagConstraints gbc_btnDodaj = new GridBagConstraints();
		gbc_btnDodaj.insets = new Insets(0, 0, 0, 5);
		gbc_btnDodaj.gridx = 5;
		gbc_btnDodaj.gridy = 0;
		panel_1.add(btnDodaj, gbc_btnDodaj);
		
		JButton btnEdytuj = new JButton("");
		btnEdytuj.setIcon(new ImageIcon(RodzajeZabiegowIntFrame.class.getResource("/pl/edu/swsim/zw/icon/edit.png")));
		btnEdytuj.setMargin(new Insets(0, 2, 0, 2));
		btnEdytuj.setEnabled(false);
		btnEdytuj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editItem();
			}
		});
		//btnEdytuj.setEnabled(false);
		GridBagConstraints gbc_btnEdytuj = new GridBagConstraints();
		gbc_btnEdytuj.insets = new Insets(0, 0, 0, 5);
		gbc_btnEdytuj.gridx = 6;
		gbc_btnEdytuj.gridy = 0;
		panel_1.add(btnEdytuj, gbc_btnEdytuj);
		
		JButton btnUsun = new JButton("");
		btnUsun.setIcon(new ImageIcon(RodzajeZabiegowIntFrame.class.getResource("/pl/edu/swsim/zw/icon/del.png")));
		btnUsun.setMargin(new Insets(0, 2, 0, 2));
		btnUsun.setEnabled(false);
		btnUsun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeItem();
			}
		});
		GridBagConstraints gbc_btnUsun = new GridBagConstraints();
		gbc_btnUsun.gridx = 7;
		gbc_btnUsun.gridy = 0;
		panel_1.add(btnUsun, gbc_btnUsun);
		
		Component verticalStrut_1 = Box.createVerticalStrut(5);
		panel.add(verticalStrut_1, BorderLayout.SOUTH);
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		table.setModel(new RodzajeZabieguTableModel());
    	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    	table.getTableHeader().setReorderingAllowed(false);
    	scrollPane.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
			    table.getColumnModel().getColumn(0).setPreferredWidth((int)(that.getWidth() * 0.15));
			    table.getColumnModel().getColumn(1).setPreferredWidth((int)(that.getWidth() * 0.15));
			    table.getColumnModel().getColumn(2).setPreferredWidth((int)(that.getWidth() * 0.10));
			    table.getColumnModel().getColumn(3).setPreferredWidth((int)(that.getWidth() * 0.11));
			    table.getColumnModel().getColumn(4).setPreferredWidth((int)(that.getWidth() * 0.11));
			    table.getColumnModel().getColumn(5).setPreferredWidth((int)(that.getWidth() * 0.11));
			    table.getColumnModel().getColumn(6).setPreferredWidth((int)(that.getWidth() * 0.13));
			}
		});
		
    	/**
    	 *  menu kontekstowe
    	 */
	    table.getTableHeader().addMouseListener(new MouseAdapter() {
	        @Override
	        public void mouseClicked(MouseEvent e) {
	            int col = table.columnAtPoint(e.getPoint());
	            
	            if(cbOrder.getModel().getSize() < (col*2)+2)
	            	return;
	            if(col*2 == cbOrder.getSelectedIndex())
	            	setOrderSort((col*2)+1);
	            else 
	            	setOrderSort(col*2);
	        }
	    });
	    table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(table.getSelectedRow() > -1) {
	            	btnEdytuj.setEnabled(true);
	            	btnUsun.setEnabled(true);
				} else {
	            	btnEdytuj.setEnabled(false);
	            	btnUsun.setEnabled(false);
				}
			}
		});

		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem menuItemView = new JMenuItem("Podgląd");
		JMenuItem menuItemAdd = new JMenuItem("Dodaj nowy rekord");
		JMenuItem menuItemEdit = new JMenuItem("Edytuj");
		JMenuItem menuItemRemove = new JMenuItem("Usuń");
		JMenuItem menuItemRefresh = new JMenuItem("Odśwież");
		popupMenu.add(menuItemView);
		popupMenu.add(menuItemAdd);
		popupMenu.add(menuItemEdit);
		popupMenu.add(menuItemRemove);
		popupMenu.add(menuItemRefresh);

		menuItemView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				viewItem();
			}
		});
		
		menuItemAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
	        	addNewItem();
			}
		});
		
		menuItemEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				editItem();
			}
		});
		
		menuItemRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				removeItem();
			}
		});
		menuItemRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				updateTable();
			}
		});
    	
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event) {
	            int currentRow = table.rowAtPoint(event.getPoint());
	            table.setRowSelectionInterval(currentRow, currentRow);
	            
	            if (event.getButton() == java.awt.event.MouseEvent.BUTTON1) {
	                if(event.getClickCount() >= 2)
	                	viewItem();
	            }
	               
	            if (event.getButton() == java.awt.event.MouseEvent.BUTTON3) {
	                popupMenu.show(table, event.getX(), event.getY());
	            }
			}
		});
		setOrderSort(cbOrder.getSelectedIndex());

	}
	
	/**
	 * Definicja filtrów sortowania
	 * @param orderNumber
	 */
	private void setOrderSort(int orderNumber) {
		
		logger.trace(".");
		
		cbOrder.setSelectedIndex(orderNumber);
		
		ArrayList<Order> order = new ArrayList<Order>();
		
        switch(orderNumber) {
        case 0: 
        	order.add(Order.asc("nazwa"));
        	break;
        case 1:
        	order.add(Order.desc("nazwa"));
	        break;
        case 2: 
        	order.add(Order.asc("rodzaj"));
        	break;
        case 3:
        	order.add(Order.desc("rodzaj"));
	        break; 
        case 4:
        	order.add(Order.asc("cena"));
	        break; 
        case 5:
        	order.add(Order.desc("cena"));
	        break; 
        case 6: 
        	order.add(Order.asc("refundacja"));
        	break;
        case 7:
        	order.add(Order.desc("refundacja"));
	        break; 
        case 8:
        	order.add(Order.asc("punkty"));
	        break; 
        case 9:
        	order.add(Order.desc("punkty"));
	        break; 
        case 10:
        	order.add(Order.asc("skierowanie"));
	        break; 
        case 11:
        	order.add(Order.desc("skierowanie"));
	        break;
        }
        
        repository.setOrderBy(order);
        
        // gdy ciąg wyszukiwania jest > 2 znaki
        if(tfSearch.getText().length() > 2) {
        	repository.setSearchText(tfSearch.getText());
        } else {
        	repository.setSearchText(null);
        }
        
		((RodzajeZabieguTableModel)table.getModel()).setModelData(repository.getRodzajeZabiegow()); // przypisanie nowych danych
		logger.trace("SortBy id " + orderNumber);
	
	}
	
	/**
	 * Wyświetlanie danych pracownika bez możliwości ich edycji
	 */
	public void viewItem() {
		logger.trace("Widok Zabiegu");
		RodzajZabieguEnt rodzajZabiegu = ((RodzajeZabieguTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		
		RodzajZabieguDialog rzDialog = new RodzajZabieguDialog((Frame)SwingUtilities.getWindowAncestor(this));
		rzDialog.DisplayDialog(rodzajZabiegu, true);
			
		rzDialog.dispose();
		this.updateTable();
	}

	/**
	 * Akcja dodawania nowego elementu wywołana wewnątrz klasy
	 */
	public void addNewItem() {
		addNewItem((Frame)SwingUtilities.getWindowAncestor(this));
		this.updateTable();
	}
	
	/**
	 * Akcja dodawania nowego elementu wywołana z zewnątrz (bez obiektu)
	 * @param parent
	 */
	public static void addNewItem(Frame parent) {
		logger.trace("Dodawanie nowego rodzaju zabiegu");
		RodzajZabieguDialog rzDialog = new RodzajZabieguDialog(parent);
		rzDialog.DisplayDialog();

		if(rzDialog.result) {
			logger.trace("Próba utworzenia nowego zabiegu " + rzDialog.rodzajZabiegu.getNazwa() );
			new RodzajeZabiegowRep().setRodzajZabiegu(rzDialog.rodzajZabiegu).AddRodzajZabiegu();
		}
			
		rzDialog.dispose();
		
	}
	
	/**
	 * Edycja zaznaczonego elementu
	 * @param rowNum
	 */
	public void editItem() {
		logger.trace("Edycja rodzaju zabiegu");
		RodzajZabieguEnt rodzajZabiegu = ((RodzajeZabieguTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
	
		RodzajZabieguDialog rzDialog = new RodzajZabieguDialog((Frame)SwingUtilities.getWindowAncestor(this));
		rzDialog.DisplayDialog(rodzajZabiegu);
				
		if(rzDialog.result) {
			logger.trace("Edytowany rodzaj zabiegu" + rzDialog.rodzajZabiegu.getNazwa() );
			new RodzajeZabiegowRep().setRodzajZabiegu(new RodzajZabieguEnt(rzDialog.rodzajZabiegu)).EditRodzajZabiegu();
		}
			
		rzDialog.dispose();
		this.updateTable();
	}
	
	/**
	 * Usuwanie zaznaczonego elementu
	 * @param rowNum
	 */
	public void removeItem() {
		logger.trace("Usuwanie rodzaju zabiegu");
		
		RodzajZabieguEnt rodzajZabiegu = ((RodzajeZabieguTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		logger.trace("Usuwanie: " + rodzajZabiegu.getNazwa());
		
		if(JOptionPane.showConfirmDialog((Frame)SwingUtilities.getWindowAncestor(this), 
				"Czy na pewno chesz usunąć " 
						+ rodzajZabiegu.getNazwa() + " ? ", 
				"Usuwanie zabiegu", 
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			new RodzajeZabiegowRep().setRodzajZabiegu(new RodzajZabieguEnt(rodzajZabiegu)).DeleteRodzajZabiegu();
			this.updateTable();
		}
		
	}	
	
	/**
	 * Aktualizacja tabeli
	 */
	public void updateTable() {
		((RodzajeZabieguTableModel)table.getModel()).setModelData(repository.getRodzajeZabiegow());
	}
	
	/**
	 * Model danych tabeli
	 */
	public class RodzajeZabieguTableModel extends AbstractTableModel {
		 
		private static final long serialVersionUID = 1L;
		
		private ArrayList<RodzajZabieguEnt> data = null; // lista danych
		private final Object[] columnNames = {"Nazwa", "Rodzaj", "Cena", "Czas trwania", "Refundacja", "Punkty", "Skierowanie"}; // nazwy w Header
	     
	    private final static int NAZWA_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int RODZAJ_IDX = 1;
	    private final static int CENA_IDX = 2;
	    private final static int CZASTRWANIA_IDX = 3;
	    private final static int REFUNDACJA_IDX = 4;
	    private final static int PUNKTY_IDX = 5;
	    private final static int SKIEROWANIE_IDX = 6;

	 
	    public RodzajeZabieguTableModel() {}
	     
	    @Override
	    public int getRowCount() {
	        if(data==null) return 0;
	        return data.size();
	    }
	 
	    @Override
	    public int getColumnCount() {
	        return columnNames.length;
	    }
	     
	    @Override // wyciąganie danych z listy
	    public Object getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        RodzajZabieguEnt record = data.get(rowIndex);
	        switch (columnIndex) {
	            case NAZWA_IDX:
	                return record.getNazwa();
	            case RODZAJ_IDX:
	                return record.getRodzaj();
	            case CENA_IDX:
	            	return String.format("%.2f", record.getCena());
	            case REFUNDACJA_IDX:
	            	return (record.isRefundacja()?"Tak":"Nie");
	            case PUNKTY_IDX:
	            	return record.getPunkty();
	            case SKIEROWANIE_IDX:
	            	return (record.isSkierowanie()?"Tak":"Nie");
	            case CZASTRWANIA_IDX:
	        		Calendar cal = Calendar.getInstance();
	        		cal.setTime(record.getCzastrwania());
	        		String godz = Integer.toString(cal.get(Calendar.HOUR_OF_DAY));
	            	String min  = cal.get(Calendar.MINUTE)<10?"0"+Integer.toString(cal.get(Calendar.MINUTE)):Integer.toString(cal.get(Calendar.MINUTE));
	        		return godz	+ " g " + min + " m";
	            default:
	                return "";
	        }
	    }
	 
	    @Override
	    public String getColumnName(int column) {
	        return columnNames[column].toString();
	    }
	 
	    @Override
	    public boolean isCellEditable(int row, int column) {
	        return false;
	    }	     
	    // ustawienie przekazanych danych 
	    public void setModelData(ArrayList<RodzajZabieguEnt> rodzajZabiegu) {
	    	logger.trace( rodzajZabiegu.size());
	    	this.data =  rodzajZabiegu;
	    	this.fireTableDataChanged();
	    }
	    
	    public RodzajZabieguEnt getDataRecord(int position) {
	        return this.data.get(position);
	    }
	}
}
