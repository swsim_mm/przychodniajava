package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;
import org.jdatepicker.DateModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import pl.edu.swsim.zw.dao.TerminyRep;
import pl.edu.swsim.zw.entities.TerminEnt;

/**
 * Lista terminów 
 * @author Michał
 *
 */
public class TerminIntFrame extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private TerminyRep repository = new TerminyRep();
	@SuppressWarnings("unused")
	private ArrayList<TerminEnt> wizyty = null;
	//private GrupaEnt grupy = null;
	
	private final String[] sortOptions = {
			"Data i czas wizyty rosnąco [..10,11,12..]",
			"Data i czas wizyty malejąco [..7,6,5..]",
			"Wizyta/Zabieg",
			"Zabieg/Wizyta",
			"Rodzaj wizyty rosnąco [a-z]",
			"Rodzaj wizyty malejąco [a-z]",
			"Pacjent rosnąco [a-z]",
			"Pacjent malejąco [z-a]",
			"Lekarz rosnąco [a-z]",
			"Lekarz malejąco [z-a]",
	};

	private JTextField tfSearch;
	private JTable table;
	private JComboBox<String> cbOrder;
	private JComboBox<String> cbTypTerminu;
	//private JTextField textFieldDATE;
	
	private JDatePanelImpl datePanel;
	private JDatePickerImpl datePicker;


	/**
	 * Create the frame.
	 */
	public TerminIntFrame() {

		// szybkie obejście this w klasach anonimowych, 
		// jest nadal dostęp do tej klasy przez that
		TerminIntFrame that = this;
	
		logger.info(getBorder());
		// nadają wygląd dla okna jak by było z systemu
		setTitle("Terminy");
		setResizable(true);
		setMaximizable(true);
		setClosable(true);
		setBounds(100, 100, 622, 386);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut_1, BorderLayout.EAST);
		
		Component verticalStrut = Box.createVerticalStrut(5);
		panel.add(verticalStrut, BorderLayout.NORTH);
		
		Component verticalStrut_1 = Box.createVerticalStrut(5);
		panel.add(verticalStrut_1, BorderLayout.SOUTH);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0, -16, 139, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblSortujWedug = new JLabel("Sortuj według");
		lblSortujWedug.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSortujWedug = new GridBagConstraints();
		gbc_lblSortujWedug.gridwidth = 2;
		gbc_lblSortujWedug.insets = new Insets(0, 0, 5, 5);
		gbc_lblSortujWedug.anchor = GridBagConstraints.EAST;
		gbc_lblSortujWedug.gridx = 0;
		gbc_lblSortujWedug.gridy = 0;
		panel_1.add(lblSortujWedug, gbc_lblSortujWedug);
		
		cbOrder = new JComboBox<String>();
		cbOrder.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cbOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		cbOrder.setModel(new DefaultComboBoxModel<String>(sortOptions));
		GridBagConstraints gbc_cbGrupa = new GridBagConstraints();
		gbc_cbGrupa.gridwidth = 3;
		gbc_cbGrupa.insets = new Insets(0, 0, 5, 5);
		gbc_cbGrupa.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbGrupa.gridx = 2;
		gbc_cbGrupa.gridy = 0;
		panel_1.add(cbOrder, gbc_cbGrupa);
		
		JButton btnDodaj = new JButton("");
		btnDodaj.setIcon(new ImageIcon(TerminIntFrame.class.getResource("/pl/edu/swsim/zw/icon/add.png")));
		btnDodaj.setMargin(new Insets(0, 2, 0, 2));
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewItem();
			}
		});
		
		JLabel lblLekarz = new JLabel("Typ");
		lblLekarz.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblLekarz = new GridBagConstraints();
		gbc_lblLekarz.anchor = GridBagConstraints.EAST;
		gbc_lblLekarz.insets = new Insets(0, 0, 5, 5);
		gbc_lblLekarz.gridx = 5;
		gbc_lblLekarz.gridy = 0;
		panel_1.add(lblLekarz, gbc_lblLekarz);
		
		cbTypTerminu = new JComboBox<String>();
		cbTypTerminu.setModel(new DefaultComboBoxModel<String>(new String[] {"Wszystkie", "Wizyta", "Zabieg"}));
		cbTypTerminu.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cbTypTerminu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		GridBagConstraints gbc_cbTypTerminu = new GridBagConstraints();
		gbc_cbTypTerminu.insets = new Insets(0, 0, 5, 5);
		gbc_cbTypTerminu.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbTypTerminu.gridx = 6;
		gbc_cbTypTerminu.gridy = 0;
		panel_1.add(cbTypTerminu, gbc_cbTypTerminu);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 7;
		gbc_btnNewButton.gridy = 0;
		panel_1.add(btnDodaj, gbc_btnNewButton);
		
		JButton btnEdytuj = new JButton("");
		btnEdytuj.setIcon(new ImageIcon(TerminIntFrame.class.getResource("/pl/edu/swsim/zw/icon/edit.png")));
		btnEdytuj.setMargin(new Insets(0, 2, 0, 2));
		btnEdytuj.setEnabled(false);
		btnEdytuj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editItem();
			}
		});
		GridBagConstraints gbc_btnE = new GridBagConstraints();
		gbc_btnE.insets = new Insets(0, 0, 5, 5);
		gbc_btnE.gridx = 8;
		gbc_btnE.gridy = 0;
		panel_1.add(btnEdytuj, gbc_btnE);
		
		JButton btnUsun = new JButton("");
		btnUsun.setIcon(new ImageIcon(TerminIntFrame.class.getResource("/pl/edu/swsim/zw/icon/del.png")));
		btnUsun.setMargin(new Insets(0, 2, 0, 2));
		btnUsun.setEnabled(false);
		btnUsun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeItem();
			}
		});
		GridBagConstraints gbc_btnU = new GridBagConstraints();
		gbc_btnU.insets = new Insets(0, 0, 5, 0);
		gbc_btnU.gridx = 9;
		gbc_btnU.gridy = 0;
		panel_1.add(btnUsun, gbc_btnU);
		
		JButton btnR = new JButton("");
		btnR.setMargin(new Insets(0, 2, 0, 2));
		btnR.setIcon(new ImageIcon(TerminIntFrame.class.getResource("/pl/edu/swsim/zw/icon/refresh2.png")));
		btnR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex()); // wymuszenie przeładowania z repozytorium
			}
		});
		GridBagConstraints gbc_btnR = new GridBagConstraints();
		gbc_btnR.insets = new Insets(0, 0, 0, 5);
		gbc_btnR.gridx = 0;
		gbc_btnR.gridy = 1;
		panel_1.add(btnR, gbc_btnR);
		
		{
			UtilDateModel model = new UtilDateModel();
			Properties p = new Properties();
			p.put("text.today", "Today");
			p.put("text.month", "Month");
			p.put("text.year", "Year");

			datePanel = new JDatePanelImpl(model, p);
			datePicker = new JDatePickerImpl(
					datePanel, new DateLabelFormatter());
			datePicker.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					setOrderSort(cbOrder.getSelectedIndex());
				}
			});

			datePicker.setShowYearButtons(true);
			datePicker.getJFormattedTextField().setFont(
					new Font("Tahoma", Font.PLAIN, 15));
			
			 
			GridBagConstraints gbc_textFieldDATE = new GridBagConstraints();
			gbc_textFieldDATE.gridwidth = 2;
			gbc_textFieldDATE.insets = new Insets(0, 0, 0, 5);
			gbc_textFieldDATE.fill = GridBagConstraints.HORIZONTAL;
			gbc_textFieldDATE.gridx = 1;
			gbc_textFieldDATE.gridy = 1;
			
			panel_1.add(datePicker, gbc_textFieldDATE);
		}
		
		tfSearch = new JTextField();
		tfSearch.setFont(new Font("Tahoma", Font.PLAIN, 13));
		tfSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
		        	setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		
		JLabel lblSzukaj = new JLabel("Szukaj");
		lblSzukaj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSzukaj = new GridBagConstraints();
		gbc_lblSzukaj.insets = new Insets(0, 0, 0, 5);
		gbc_lblSzukaj.anchor = GridBagConstraints.EAST;
		gbc_lblSzukaj.gridx = 3;
		gbc_lblSzukaj.gridy = 1;
		panel_1.add(lblSzukaj, gbc_lblSzukaj);
		GridBagConstraints gbc_textFieldDATE1 = new GridBagConstraints();
		gbc_textFieldDATE1.gridwidth = 3;
		gbc_textFieldDATE1.insets = new Insets(0, 0, 0, 5);
		gbc_textFieldDATE1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldDATE1.gridx = 4;
		gbc_textFieldDATE1.gridy = 1;
		panel_1.add(tfSearch, gbc_textFieldDATE1);
		tfSearch.setColumns(10);
		
		JButton btnPodglad = new JButton("Podgląd");
		btnPodglad.setEnabled(false);
		btnPodglad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				viewItem();
			}
		});
		GridBagConstraints gbc_btnPodglad = new GridBagConstraints();
		gbc_btnPodglad.anchor = GridBagConstraints.WEST;
		gbc_btnPodglad.gridwidth = 3;
		gbc_btnPodglad.gridx = 7;
		gbc_btnPodglad.gridy = 1;
		panel_1.add(btnPodglad, gbc_btnPodglad);
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		// praktycznie cały ręcznie robiony model JTable z własnymi podczepieniami
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table); // tego nie da się zrobić przez WindowsBuilder tylko ręcznie
		
		table.setModel(new WizytyTableModel()); // model danych
    	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); // dostosowanie rozmiarów ręczne
    	table.getTableHeader().setReorderingAllowed(false);
		scrollPane.addComponentListener(new ComponentAdapter() {
			// w procentach opisane rozmiary kolumn 50% = 0.50
			@Override
			public void componentResized(ComponentEvent arg0) {
			    table.getColumnModel().getColumn(0).setPreferredWidth((int)(that.getWidth() * 0.20));
			    table.getColumnModel().getColumn(1).setPreferredWidth((int)(that.getWidth() * 0.10));
			    table.getColumnModel().getColumn(2).setPreferredWidth((int)(that.getWidth() * 0.20));
			    table.getColumnModel().getColumn(3).setPreferredWidth((int)(that.getWidth() * 0.15));
			}
		});
		
		// klinięcie na nagłówek uruchamia sortowanie
	    table.getTableHeader().addMouseListener(new MouseAdapter() {
	        @Override 
	        public void mouseClicked(MouseEvent e) {
	            int col = table.columnAtPoint(e.getPoint());
	            
	            if(cbOrder.getModel().getSize() < (col*2)+2)
	            	return;
	            if(col*2 == cbOrder.getSelectedIndex())
	            	setOrderSort((col*2)+1);
	            else 
	            	setOrderSort(col*2);
	        }
	    });
	    
	    // wybranie/odznaczenie elementu z JTable = blokowanie lub odblokowanie przycisków edycji i usuwania
	    table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(table.getSelectedRow() > -1) {
	            	btnEdytuj.setEnabled(true);
	            	btnUsun.setEnabled(true);
	            	btnPodglad.setEnabled(true);
				} else {
	            	btnEdytuj.setEnabled(false);
	            	btnUsun.setEnabled(false);
	            	btnPodglad.setEnabled(false);
				}
			}
		});
	    
	    // PopUp menu
		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem menuItemView = new JMenuItem("Zobacz informacje o pacjencie");
		JMenuItem menuItemAdd = new JMenuItem("Dodaj nowego pacjenta");
		JMenuItem menuItemEdit = new JMenuItem("Edytuj");
		JMenuItem menuItemRemove = new JMenuItem("Usuń");
		JMenuItem menuItemRefresh = new JMenuItem("Odśwież");
		popupMenu.add(menuItemView);
		popupMenu.add(menuItemAdd);
		popupMenu.add(menuItemEdit);
		popupMenu.add(menuItemRemove);
		popupMenu.add(menuItemRefresh);
		ActionListener popupListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				if(e.getSource() == menuItemView)
					viewItem();
				else if(e.getSource() == menuItemAdd)
					addNewItem();
				else if(e.getSource() == menuItemEdit)
					editItem();
				else if(e.getSource() == menuItemRemove)
					removeItem();
				else if(e.getSource() == menuItemRefresh)
					updateTable();
			}
		};
		menuItemView.addActionListener(popupListener);
		menuItemAdd.addActionListener(popupListener);
		menuItemEdit.addActionListener(popupListener);
		menuItemRemove.addActionListener(popupListener);
		menuItemRefresh.addActionListener(popupListener);
		
		table.addMouseListener(new MouseAdapter() {
	        @Override
			public void mouseClicked(MouseEvent event) {
	            int currentRow = table.rowAtPoint(event.getPoint());
	            table.setRowSelectionInterval(currentRow, currentRow);

	            if (event.getButton() == java.awt.event.MouseEvent.BUTTON1) {
	                if(event.getClickCount() >= 2)
	                	viewItem();
	            }
	            if (event.getButton() == java.awt.event.MouseEvent.BUTTON3) {
	                popupMenu.show(table, event.getX(), event.getY());
	            }
			}
	        
		});
	    		
	    // pierwsze przesortowanie informacji z bazy danych i tym samym pobranie ich
	    setOrderSort(cbOrder.getSelectedIndex());
	    
	}
	
	/**
	 * Definicja filtrów sortowania
	 * @param orderNumber
	 */
	private void setOrderSort(int orderNumber) {
		logger.trace(".");
		
		cbOrder.setSelectedIndex(orderNumber);
		
		ArrayList<Order> order = new ArrayList<Order>();
		
        switch(orderNumber) {
        case 0: 
        	order.add(Order.asc("termin"));
        	break;
        case 1:
        	order.add(Order.desc("termin"));
	        break;
        case 2: 
        	order.add(Order.asc("zabieg"));
        	break;
        case 3:
        	order.add(Order.desc("zabieg"));
	        break; 
        case 4: 
        	;
        	break;
        case 5:
        	;
	        break;
        case 6: 
        	order.add(Order.asc("pa.nazwisko"));
        	order.add(Order.asc("pa.imie"));
        	break;
        case 7:
        	order.add(Order.desc("pa.nazwisko"));
        	order.add(Order.desc("pa.imie"));
	        break;
        case 8: 
        	order.add(Order.asc("pr.nazwisko"));
        	order.add(Order.asc("pr.imie"));
        	break;
        case 9:
        	order.add(Order.desc("pr.nazwisko"));
        	order.add(Order.desc("pr.imie"));
	        break;
        }
        repository.setOrderBy(order);
        
        // gdy ciąg wyszukiwania jest > 2 znaki
        if(tfSearch.getText().length() > 2) {
        	repository.setSearchText(tfSearch.getText());
        } else {
        	repository.setSearchText(null);
        }
        
        if(cbTypTerminu.getSelectedIndex() == 0) {
        	repository.setTypTerminu(TerminyRep.TYP_DOWOLNY);
        } else if (cbTypTerminu.getSelectedIndex() == 1) {
        	repository.setTypTerminu(TerminyRep.TYP_WIZYTA);
        }else if (cbTypTerminu.getSelectedIndex() == 2) {
        	repository.setTypTerminu(TerminyRep.TYP_ZABIEG);
        }
        
        if(!datePicker.getModel().isSelected()) {
    		Calendar cal = Calendar.getInstance();
    		cal.setTime(new Date());
    		
    		DateModel<?> dateModel = datePicker.getModel();
    		dateModel.setYear(cal.get(Calendar.YEAR));
    		dateModel.setMonth(cal.get(Calendar.MONTH));
    		dateModel.setDay(cal.get(Calendar.DAY_OF_MONTH));   
    		datePicker.updateUI();

        	datePicker.getModel().setSelected(true);
        }
        
        repository.setThatDate((Date)datePicker.getModel().getValue());
        
		((WizytyTableModel)table.getModel()).setModelData(repository.getTerminy()); // przypisanie nowych danych
		logger.trace("SortBy id " + orderNumber);
	}
	/**
	 * Wyświetlanie danych pracownika bez możliwości ich edycji
	 */
	public void viewItem() {
		logger.trace("Widok pacjenta");
		TerminEnt record = ((WizytyTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		
		TerminDialog dataDialog = new TerminDialog((Frame)SwingUtilities.getWindowAncestor(this));
		dataDialog.DisplayDialog(record, true);
			
		dataDialog.dispose();
		this.updateTable();
	}
	
	
	/**
	 * Akcja dodawania nowego elementu wywołana wewnątrz klasy
	 */
	public void addNewItem() {
		addNewItem((Frame)SwingUtilities.getWindowAncestor(this));
		this.updateTable();
	}
	
	/**
	 * Akcja dodawania nowego elementu wywołana z zewnątrz (bez obiektu)
	 * @param parent
	 */
	public static void addNewItem(Frame parent) {
		logger.trace("Dodawanie nowego terminu");
		TerminDialog dataDialog = new TerminDialog(parent);
		dataDialog.DisplayDialog();

		if(dataDialog.result) {
			logger.trace("Próba utworzenia nowego terminu ");
			new TerminyRep().setTermin(dataDialog.termin).AddTermin();
		}
			
		dataDialog.dispose();
	}
	
	/**
	 * Edycja zaznaczonego elementu
	 * @param rowNum
	 */
	public void editItem() {
		logger.trace("Edycja terminu");
		TerminEnt record = ((WizytyTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		
		TerminDialog dataDialog = new TerminDialog((Frame)SwingUtilities.getWindowAncestor(this));
		dataDialog.DisplayDialog(record);
		
		if(dataDialog.result) {
			logger.trace("Edytowana wizyta");
			new TerminyRep().setTermin(dataDialog.termin).EditTermin();
		}
			
		dataDialog.dispose();
		this.updateTable();
	}
	
	/**
	 * Usuwanie zaznaczonego elementu
	 * @param rowNum
	 */
	public void removeItem() {
		logger.trace("Usuwanie terminu");
		
		TerminEnt record = ((WizytyTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		//logger.trace("Usuwanie wizyty: " + record.getImie() + " " + record.getNazwisko());
		Calendar cal = Calendar.getInstance();
		cal.setTime(record.getTermin());
		
		String text = "Czy na pewno chesz usunąć termin " + (record.isZabieg()?"zabiegu":"wizyty");
		text += cal.get(Calendar.YEAR) + "." + cal.get(Calendar.MONTH) + "." + cal.get(Calendar.DAY_OF_MONTH) + " ";
		text += "o godzinie " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + " ";
		text += "pacjenta " + record.getPacjent().getImie() + " " + record.getPacjent().getNazwisko();
		
		
		if(JOptionPane.showConfirmDialog((Frame)SwingUtilities.getWindowAncestor(this), 
				text, 
				"Usuwanie terminu", 
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			new TerminyRep().setTermin(record).DeleteTermin();
			this.updateTable();
		}
			
		
	}
	
	/**
	 * Aktualizacja całej zawartości tabeli
	 */
	public void updateTable() {
		((WizytyTableModel)table.getModel()).setModelData(repository.getTerminy());
	}
	
	/**
	 * Model danych tabeli
	 * @author Michał
	 *
	 */
	public class WizytyTableModel extends AbstractTableModel {
		 
		private static final long serialVersionUID = 1L;
		
		private ArrayList<TerminEnt> data = null; // lista danych
		private final Object[] columnNames = {"Termin", "Wiz/Zab", "Rodzaj", "Pacjent", "Lekarz"}; // nazwy w Header
	     
	    private final static int TERMIN_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int WIZYTA_ZABIEG_IDX = 1;
	    private final static int RODZAJ_IDX = 2;
	    private final static int PACJENT_IDX = 3;
	    private final static int LEKARZ_IDX = 4;
	 
	    public WizytyTableModel() {}
	     
	    @Override
	    public int getRowCount() {
	        if(data==null) return 0;
	        return data.size();
	    }
	 
	    @Override
	    public int getColumnCount() {
	        return columnNames.length;
	    }
	     
	    @Override // wyciąganie danych z listy
	    public Object getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        TerminEnt record = data.get(rowIndex);
	        switch (columnIndex) {
	            case TERMIN_IDX:
	        		Calendar cal = Calendar.getInstance();
	        		cal.setTime(record.getTermin());
	        		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");	        		
	                return format1.format(cal.getTime()); 
	            case WIZYTA_ZABIEG_IDX:
	            	return record.isZabieg()?"Zabieg":"Wizyta";
	            case RODZAJ_IDX:
	            	if(record.isZabieg())
	            		return record.getRodzajzabiegu().getNazwa();
	            	else
	            		return record.getRodzajwizyty().getNazwa();
	            case PACJENT_IDX:
	                return record.getPacjent().getNazwisko() + " " + record.getPacjent().getImie();
	            case LEKARZ_IDX:
	            	return record.getPracownik().getNazwisko() + " " + record.getPracownik().getImie();
	            default:
	                return "";
	        }
	    }
	 
	    @Override
	    public String getColumnName(int column) {
	        return columnNames[column].toString();
	    }
	 
	    @Override
	    public boolean isCellEditable(int row, int column) {
	        return false;
	    }
	     
	    // ustawienie przekazanych danych 
	    public void setModelData(ArrayList<TerminEnt> data) {
	       this.data =  data;
	       this.fireTableDataChanged();
	    }
	    
	    public TerminEnt getDataRecord(int position) {
	        return this.data.get(position);
	    }
	}
	
	public class DateLabelFormatter extends AbstractFormatter {

		private static final long serialVersionUID = 1L;
		private String datePattern = "yyyy-MM-dd";
		private SimpleDateFormat dateFormatter = new SimpleDateFormat(
				datePattern);

		@Override
		public Object stringToValue(String text) throws ParseException {
			return dateFormatter.parseObject(text);
		}

		@Override
		public String valueToString(Object value) throws ParseException {
			if (value != null) {
				Calendar cal = (Calendar) value;
				return dateFormatter.format(cal.getTime());
			}

			return "";
		}

	}


}
