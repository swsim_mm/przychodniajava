package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.GabinetyRep;
import pl.edu.swsim.zw.entities.GabinetEnt;
import pl.edu.swsim.zw.entities.RodzajWizytyEnt;

/**
 * Okno dodawania/modyfkacji rodzaju wizyty
 *
 */
public class RodzajWizytyDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	public RodzajWizytyEnt rodzajWizyty = null;
	public boolean result = false;
	private ArrayList<GabinetEnt> gabinety = null;

	private final JPanel contentPanel = new JPanel();
	private JTextField tfNazwa;
	private JSpinner spGodziny;
	private JSpinner spMinuty;
	private JComboBox<String> cbGabinety;

	/**
	 * Create the dialog.
	 */
	public RodzajWizytyDialog(Frame parent) {
		super(parent);
		setTitle("Rodzaj Wizyty");
		setResizable(false);
			
		this.setModal(true);
		setBounds(100, 100, 480, 140);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.WEST);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.EAST);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.NORTH);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.SOUTH);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				JLabel lblNazwa = new JLabel("Nazwa");
				lblNazwa.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNazwa = new GridBagConstraints();
				gbc_lblNazwa.anchor = GridBagConstraints.EAST;
				gbc_lblNazwa.insets = new Insets(0, 0, 5, 5);
				gbc_lblNazwa.gridx = 0;
				gbc_lblNazwa.gridy = 0;
				panel.add(lblNazwa, gbc_lblNazwa);
			}
			{
				tfNazwa = new JTextField();
				tfNazwa.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_tfNazwa = new GridBagConstraints();
				gbc_tfNazwa.gridwidth = 7;
				gbc_tfNazwa.insets = new Insets(0, 0, 5, 0);
				gbc_tfNazwa.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfNazwa.gridx = 1;
				gbc_tfNazwa.gridy = 0;
				panel.add(tfNazwa, gbc_tfNazwa);
				tfNazwa.setColumns(10);
			}
			{
				JLabel lblCzasTrwania = new JLabel("Czas trwania");
				lblCzasTrwania.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblCzasTrwania = new GridBagConstraints();
				gbc_lblCzasTrwania.insets = new Insets(0, 0, 0, 5);
				gbc_lblCzasTrwania.gridx = 0;
				gbc_lblCzasTrwania.gridy = 1;
				panel.add(lblCzasTrwania, gbc_lblCzasTrwania);
			}
			{
				spGodziny = new JSpinner();
				spGodziny.setFont(new Font("Tahoma", Font.PLAIN, 13));
				spGodziny.setModel(new SpinnerNumberModel(0, 0, 23, 1));
				GridBagConstraints gbc_spGodziny = new GridBagConstraints();
				gbc_spGodziny.insets = new Insets(0, 0, 0, 5);
				gbc_spGodziny.gridx = 1;
				gbc_spGodziny.gridy = 1;
				panel.add(spGodziny, gbc_spGodziny);
			}
			{
				JLabel lblH = new JLabel("godz.");
				lblH.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblH = new GridBagConstraints();
				gbc_lblH.insets = new Insets(0, 0, 0, 5);
				gbc_lblH.gridx = 2;
				gbc_lblH.gridy = 1;
				panel.add(lblH, gbc_lblH);
			}
			{
				spMinuty = new JSpinner();
				spMinuty.setFont(new Font("Tahoma", Font.PLAIN, 13));
				spMinuty.setModel(new SpinnerNumberModel(0, 0, 59, 1));
				GridBagConstraints gbc_spMinuty = new GridBagConstraints();
				gbc_spMinuty.insets = new Insets(0, 0, 0, 5);
				gbc_spMinuty.gridx = 3;
				gbc_spMinuty.gridy = 1;
				panel.add(spMinuty, gbc_spMinuty);
			}
			{
				JLabel lblM = new JLabel("min.");
				lblM.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblM = new GridBagConstraints();
				gbc_lblM.insets = new Insets(0, 0, 0, 5);
				gbc_lblM.gridx = 4;
				gbc_lblM.gridy = 1;
				panel.add(lblM, gbc_lblM);
			}
			{
				JLabel lblNewLabel = new JLabel("Gabinet");
				lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
				gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
				gbc_lblNewLabel.gridx = 6;
				gbc_lblNewLabel.gridy = 1;
				panel.add(lblNewLabel, gbc_lblNewLabel);
			}
			{
				cbGabinety = new JComboBox<String>();
				cbGabinety.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_cbGabinety = new GridBagConstraints();
				gbc_cbGabinety.fill = GridBagConstraints.HORIZONTAL;
				gbc_cbGabinety.gridx = 7;
				gbc_cbGabinety.gridy = 1;
				panel.add(cbGabinety, gbc_cbGabinety);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okAction();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelAction();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
	}
	
	private void setDialogModal() {
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
		//this.setModal(true); --> konstruktor
		this.setVisible(true);
		
	}
	
	/**
	 * Reguluje formę wyświetlania okna
	 */	
	public void DisplayDialog() {
		this.rodzajWizyty = new RodzajWizytyEnt();
		setTitle("Nowy rodzaj wizyty");
		updateGabinety();
		this.setDialogModal();
	}
	
	public void DisplayDialog(RodzajWizytyEnt wizytaInit) {
		ReadyData(wizytaInit);
		setTitle(wizytaInit.getNazwa() + "(RodzajWizyty)");
		this.setDialogModal();
	}
	
	/**
	 * Prekonfiuracja okna z wartościami
	 * @param wizytaInit
	 */
	public void ReadyData(RodzajWizytyEnt wizytaInit) {
		logger.trace(".");
		this.rodzajWizyty = wizytaInit;
		this.tfNazwa.setText(wizytaInit.getNazwa());	
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(wizytaInit.getCzastrwania());
		this.spGodziny.setValue(cal.get(Calendar.HOUR_OF_DAY));
		this.spMinuty.setValue(cal.get(Calendar.MINUTE));
		
		presetGabinet(wizytaInit.getGabinet());
		
		setVisible(false);
	}
	
	// ustawienie wartości gabinet
	private void presetGabinet(GabinetEnt gabinet) {

		updateGabinety();

		if (gabinet == null)
			return;

		long pracID = gabinet.getId();

		for (int i = 0; i < gabinety.size(); i++) {
			if (gabinety.get(i).getId() == pracID) {
				cbGabinety.setSelectedIndex(i);
				break;
			}
		}

	}

	/**
	 * Aktualizacja listy gabinetów
	 */
	public void updateGabinety() {
		((DefaultComboBoxModel<String>) cbGabinety.getModel())
				.removeAllElements();

		ArrayList<Order> order = new ArrayList<Order>();
		Order.asc("numer");

		gabinety = new GabinetyRep().setOrderBy(order).getGabinety();
		for (GabinetEnt gab : gabinety) {
			cbGabinety.addItem("(" + gab.getNumer() + ") " + gab.getNazwa());
		}
	}

	/**
	 * Zatwierdzenie zmian
	 */
	public void okAction() {
		result = true;
		
		Frame parent = (Frame)SwingUtilities.getWindowAncestor(this);
		
		if(tfNazwa.getText().length()<1) {
			// okno uzupelnienie danych
			JOptionPane.showMessageDialog(parent,
				    "Wszystkie pola muszą zostać uzupełnione!",
				    "Brak zawartości pól",
				    JOptionPane.ERROR_MESSAGE);	
			logger.info("brak zawartości pól");
			result = false;
			return;
		}
			
		if(cbGabinety.getSelectedIndex() == -1) {
			JOptionPane.showMessageDialog(parent,
				    "Do utworzenia rodzaju wizyty konieczny jest zdefiniowany przynajmniej jeden gabinet",
				    "Brak wskazanego gabinetu",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("brak wybranego gabinetu");
		}
			
		
		if(result) {
			this.rodzajWizyty.setNazwa(this.tfNazwa.getText());
			
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, (int)this.spGodziny.getValue());
			cal.set(Calendar.MINUTE, (int)this.spMinuty.getValue());
			cal.set(Calendar.SECOND, 0);
			
			this.rodzajWizyty.setCzastrwania(new java.sql.Time(cal.getTimeInMillis()));
			
			this.rodzajWizyty.setGabinet(gabinety.get(cbGabinety.getSelectedIndex()));
			
			setVisible(false);
		}
	}
	
	/**
	 * Porzucienie zmian
	 */
	public void cancelAction() {
		result = false;
		setVisible(false);
	}
}
