package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.LokalizacjeRep;
import pl.edu.swsim.zw.entities.MiastoEnt;
import pl.edu.swsim.zw.entities.UlicaEnt;
import pl.edu.swsim.zw.entities.WojewodztwoEnt;

/**
 * Okno modyfikacji/dodawania rekordu lokalizacji
 *
 */
public class LokalizacjaDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private ArrayList<MiastoEnt> miasta = null;
	private ArrayList<WojewodztwoEnt> wojewodztwa = null;
	public UlicaEnt ulica = null;
	public boolean result = false;

	private final JPanel contentPanel = new JPanel();
	private JTextField tfUlica;
	private JComboBox<String> cbWojewodztwa;
	private JComboBox<String> cbMiasta;

	/**
	 * Create the dialog.
	 */
	public LokalizacjaDialog(Frame parent, UlicaEnt lokalizacjaInit) {
		super(parent);
		setTitle("Lokalizacje");
		setResizable(false);
		
		if(lokalizacjaInit == null)
			setTitle("Nowa lokalizacja");
		else
			setTitle("Edycja lokalizacji");
		
		setBounds(100, 100, 539, 178);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.WEST);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.EAST);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.NORTH);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.SOUTH);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				JLabel lblWojewodztwo = new JLabel("Wojewodztwo");
				lblWojewodztwo.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblWojewodztwo = new GridBagConstraints();
				gbc_lblWojewodztwo.anchor = GridBagConstraints.EAST;
				gbc_lblWojewodztwo.insets = new Insets(0, 0, 5, 5);
				gbc_lblWojewodztwo.gridx = 0;
				gbc_lblWojewodztwo.gridy = 0;
				panel.add(lblWojewodztwo, gbc_lblWojewodztwo);
			}
			{
				cbWojewodztwa = new JComboBox<String>();
				cbWojewodztwa.setFont(new Font("Tahoma", Font.PLAIN, 13));
				cbWojewodztwa.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						updateMiasta();
					}
				});
				GridBagConstraints gbc_cbWojewodztwa = new GridBagConstraints();
				gbc_cbWojewodztwa.insets = new Insets(0, 0, 5, 5);
				gbc_cbWojewodztwa.fill = GridBagConstraints.HORIZONTAL;
				gbc_cbWojewodztwa.gridx = 1;
				gbc_cbWojewodztwa.gridy = 0;
				panel.add(cbWojewodztwa, gbc_cbWojewodztwa);
			}
			{
				JButton btnNewButton = new JButton("");
				btnNewButton.setIcon(new ImageIcon(LokalizacjaDialog.class.getResource("/pl/edu/swsim/zw/icon/add.png")));
				btnNewButton.setMargin(new Insets(0, 2, 0, 2));
				btnNewButton.setToolTipText("Dodaj nowe miasto");
				btnNewButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						logger.trace("Add miasto");
						MiastoDialog nmiastDialog = new MiastoDialog(parent, null);

						if(nmiastDialog.result) {
							logger.trace("Próba utworzenia nowego miasta");
							logger.trace(nmiastDialog.miasto.getNazwa());
							new LokalizacjeRep().setMiasto(nmiastDialog.miasto).AddMiasto().commit();
						}
							
						nmiastDialog.dispose();
						updateMiasta();
					}
				});
				GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
				gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
				gbc_btnNewButton.gridx = 2;
				gbc_btnNewButton.gridy = 0;
				panel.add(btnNewButton, gbc_btnNewButton);
			}
			{
				JLabel lblMiasto = new JLabel("Miasto");
				lblMiasto.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblMiasto = new GridBagConstraints();
				gbc_lblMiasto.anchor = GridBagConstraints.EAST;
				gbc_lblMiasto.insets = new Insets(0, 0, 5, 5);
				gbc_lblMiasto.gridx = 0;
				gbc_lblMiasto.gridy = 1;
				panel.add(lblMiasto, gbc_lblMiasto);
			}
			{
				cbMiasta = new JComboBox<String>();
				cbMiasta.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_cbMiasta = new GridBagConstraints();
				gbc_cbMiasta.insets = new Insets(0, 0, 5, 5);
				gbc_cbMiasta.fill = GridBagConstraints.HORIZONTAL;
				gbc_cbMiasta.gridx = 1;
				gbc_cbMiasta.gridy = 1;
				panel.add(cbMiasta, gbc_cbMiasta);
			}
			{
				JButton btnE = new JButton("");
				btnE.setIcon(new ImageIcon(LokalizacjaDialog.class.getResource("/pl/edu/swsim/zw/icon/edit.png")));
				btnE.setMargin(new Insets(0, 2, 0, 2));
				btnE.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						logger.trace("Edit miasto");
						MiastoDialog nmiastDialog = new MiastoDialog(parent, miasta.get(cbMiasta.getSelectedIndex()));

						if(nmiastDialog.result) {
							logger.trace("Próba edycji miasta" + nmiastDialog.miasto.getNazwa());
							new LokalizacjeRep().setMiasto(nmiastDialog.miasto).EditMiasto().commit();
						}
							
						nmiastDialog.dispose();
						updateMiasta();
					}
				});
				btnE.setToolTipText("Edytuj wskazane miasto");
				GridBagConstraints gbc_btnE = new GridBagConstraints();
				gbc_btnE.insets = new Insets(0, 0, 5, 0);
				gbc_btnE.gridx = 2;
				gbc_btnE.gridy = 1;
				panel.add(btnE, gbc_btnE);
			}
			{
				JLabel lblUlica = new JLabel("Ulica");
				lblUlica.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblUlica = new GridBagConstraints();
				gbc_lblUlica.anchor = GridBagConstraints.EAST;
				gbc_lblUlica.insets = new Insets(0, 0, 0, 5);
				gbc_lblUlica.gridx = 0;
				gbc_lblUlica.gridy = 2;
				panel.add(lblUlica, gbc_lblUlica);
			}
			{
				tfUlica = new JTextField();
				tfUlica.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_textField = new GridBagConstraints();
				gbc_textField.insets = new Insets(0, 0, 0, 5);
				gbc_textField.fill = GridBagConstraints.HORIZONTAL;
				gbc_textField.gridx = 1;
				gbc_textField.gridy = 2;
				panel.add(tfUlica, gbc_textField);
				tfUlica.setColumns(10);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okAction();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelAction();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		ReadyData(lokalizacjaInit);
		
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
		this.setModal(true);
		this.setVisible(true);
	}
	
	public void ReadyData(UlicaEnt lokalizacjaInit) {
		logger.trace(".");
		
		ArrayList<Order> order = new ArrayList<Order>();
		Order.asc("nazwa");
		
		wojewodztwa = new LokalizacjeRep().setOrderBy(order).getWojewodztwa();
		for(WojewodztwoEnt woj: wojewodztwa) {
			cbWojewodztwa.addItem(woj.getNazwa());
		}
		
		this.updateMiasta();
		
		if(lokalizacjaInit == null)
			this.ulica = new UlicaEnt();
		else {
			ulica = lokalizacjaInit;
			
			if(ulica.getMiasto() == null || ulica.getMiasto().getWojewodztwo() == null ) {
				logger.error("Wartość dla wpisu ulicy " + ulica.getId() + "ma uszkodzoną relacje!");
				return;
			}
			
			long wojID = ulica.getMiasto().getWojewodztwo().getId();
			
			for(int i = 0; i< wojewodztwa.size(); i++) {
				if(wojewodztwa.get(i).getId() == wojID) {
					cbWojewodztwa.setSelectedIndex(i);
					break;
				}	
			}
			
			if(cbWojewodztwa.getSelectedIndex() == -1) {
				logger.error("Nie znaleziono wojewodztwa id " 
						+ ulica.getMiasto().getWojewodztwo().getId() 
						+ " nazwa " 
						+ ulica.getMiasto().getWojewodztwo().getNazwa());
				return;
			}
			
			
			
			/*miasta = new LokalizacjaRep()
				.setOrderBy(Order.asc("nazwa"))
				.setWojewodztwo(ulica.getMiasto().getWojewodztwo())
				.getMiasta();
			
			for(MiastoEnt miast: miasta) {
				cbMiasta.addItem(miast.getNazwa() + "(" + miast.getKod_pocztowy() + ")");
			}*/
			this.updateMiasta();
			
			long miastoID = ulica.getMiasto().getId();
			
			for(int i = 0; i< miasta.size(); i++) {
				if(miasta.get(i).getId() == miastoID) {
					cbMiasta.setSelectedIndex(i);
					break;
				}	
			}
			
			this.tfUlica.setText(ulica.getNazwa());	

		}
		
	}
	
	public void updateMiasta() {
		if(wojewodztwa.size() > 0) {
			
			((DefaultComboBoxModel<String>)cbMiasta.getModel()).removeAllElements();
			
			ArrayList<Order> order = new ArrayList<Order>();
			Order.asc("nazwa");
			
			miasta = new LokalizacjeRep()
				.setOrderBy(order)
				.setWojewodztwo(wojewodztwa.get(cbWojewodztwa.getSelectedIndex()))
				.getMiasta();
		
			for(MiastoEnt miast: miasta) {
				cbMiasta.addItem(miast.getNazwa() + " (" + miast.getKod_pocztowy() + ")");
			}
		}
	}
	
	public void okAction() {
		result = true;
		Frame parent = (Frame)SwingUtilities.getWindowAncestor(this);
		
		if(tfUlica.getText().length()<1) {
			// okno uzupelnienie danych
			JOptionPane.showMessageDialog(parent,
				    "Wszystkie pola muszą zostać uzupełnione!",
				    "Brak zawartości pól",
				    JOptionPane.ERROR_MESSAGE);	
			logger.info("brak zawartości pól");
			result = false;
			return;
		}
		
		
		String nazwa = this.tfUlica.getText();
		
		if (Pattern.matches("^([0-9A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\\s\\-])+$", nazwa) == false) {
			JOptionPane.showMessageDialog(parent,
				    "Format nazwy miasta jest niepoprawny!\n\nNazwa nie powinna zawierać znaków specjalnych",
				    "Zła nazwa ulicy",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("zła nazwa");
		}
		
		if(cbWojewodztwa.getSelectedIndex() < 0 || cbMiasta.getSelectedIndex() < 0) {
			JOptionPane.showMessageDialog(parent,
				    "Należy wskazać miasto oraz województwo",
				    "Uzupełnij dane",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("Brak wybranego wojewodztwa lub miasta");
		}
		
		
		if(result) {
			this.ulica.setNazwa(this.tfUlica.getText());
			this.ulica.setMiasto(miasta.get(cbMiasta.getSelectedIndex()));			
			setVisible(false);
		}
	}
	
	public void cancelAction() {
		result = false;
		setVisible(false);
	}
}
