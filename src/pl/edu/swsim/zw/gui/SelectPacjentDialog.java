package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.PacjenciRep;
import pl.edu.swsim.zw.entities.PacjentEnt;
import javax.swing.ImageIcon;

/**
 * Okno szybkiego wyboru pacjenta
 *
 */
public class SelectPacjentDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private PacjenciRep repository = new PacjenciRep();
	public PacjentEnt pacjent = null;
	public boolean result = false;
	private int order = 0;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField tfSearch;
	private JTable table;

	/**
	 * Create the dialog.
	 */
	public SelectPacjentDialog() {
		setTitle("Wybierz pacjenta");
		setModal(true);
		SelectPacjentDialog that = this;
		setBounds(100, 100, 519, 361);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.WEST);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.EAST);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.NORTH);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.SOUTH);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				JLabel lblSzukaj = new JLabel("Szukaj");
				lblSzukaj.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblSzukaj = new GridBagConstraints();
				gbc_lblSzukaj.insets = new Insets(0, 0, 5, 5);
				gbc_lblSzukaj.anchor = GridBagConstraints.EAST;
				gbc_lblSzukaj.gridx = 0;
				gbc_lblSzukaj.gridy = 0;
				panel.add(lblSzukaj, gbc_lblSzukaj);
			}
			{
				tfSearch = new JTextField();
				tfSearch.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_tfSearch = new GridBagConstraints();
				gbc_tfSearch.insets = new Insets(0, 0, 5, 5);
				gbc_tfSearch.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfSearch.gridx = 1;
				gbc_tfSearch.gridy = 0;
				panel.add(tfSearch, gbc_tfSearch);
				tfSearch.setColumns(10);
			}
			{
				JButton button = new JButton("");
				button.setIcon(new ImageIcon(SelectPacjentDialog.class.getResource("/pl/edu/swsim/zw/icon/add.png")));
				button.setMargin(new Insets(0, 2, 0, 2));
				GridBagConstraints gbc_button = new GridBagConstraints();
				gbc_button.insets = new Insets(0, 0, 5, 0);
				gbc_button.gridx = 2;
				gbc_button.gridy = 0;
				panel.add(button, gbc_button);
			}
			{
				JScrollPane scrollPane = new JScrollPane();
				GridBagConstraints gbc_scrollPane = new GridBagConstraints();
				gbc_scrollPane.gridwidth = 3;
				gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
				gbc_scrollPane.fill = GridBagConstraints.BOTH;
				gbc_scrollPane.gridx = 0;
				gbc_scrollPane.gridy = 1;
				panel.add(scrollPane, gbc_scrollPane);
				{
					table = new JTable();
					table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					scrollPane.setViewportView(table);
					
					table.setModel(new QuickPacjenciTableModel()); // model danych
			    	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); // dostosowanie rozmiarów ręczne
			    	table.getTableHeader().setReorderingAllowed(false);
					scrollPane.addComponentListener(new ComponentAdapter() {
						// w procentach opisane rozmiary kolumn 50% = 0.50
						@Override
						public void componentResized(ComponentEvent arg0) {
						    table.getColumnModel().getColumn(0).setPreferredWidth((int)(that.getWidth() * 0.20));
						    table.getColumnModel().getColumn(1).setPreferredWidth((int)(that.getWidth() * 0.20));
						    table.getColumnModel().getColumn(2).setPreferredWidth((int)(that.getWidth() * 0.40));
						    table.getColumnModel().getColumn(3).setPreferredWidth((int)(that.getWidth() * 0.15));
						}
					});
					
					// klinięcie na nagłówek uruchamia sortowanie
				    table.getTableHeader().addMouseListener(new MouseAdapter() {
				        @Override 
				        public void mouseClicked(MouseEvent e) {
				            int col = table.columnAtPoint(e.getPoint());
				            
				            if(col*2 == order)
				            	setOrderSort((col*2)+1);
				            else 
				            	setOrderSort(col*2);
				        }
				    });
				    
					table.addMouseListener(new MouseAdapter() {
				        @Override
						public void mouseClicked(MouseEvent event) {
				            int currentRow = table.rowAtPoint(event.getPoint());
				            table.setRowSelectionInterval(currentRow, currentRow);

				            if (event.getButton() == java.awt.event.MouseEvent.BUTTON1) {
				                if(event.getClickCount() >= 2)
				                	selectItem();
				            }
						}
				        
					});
					
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						okAction();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelAction();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				cancelAction();
			}
		});
		
		setOrderSort(order);
		
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
	}
	
	/**
	 * Definicja filtrów sortowania
	 * @param orderNumber
	 */
	private void setOrderSort(int orderNumber) {
		logger.trace(".");
		
		order = orderNumber;
		
		ArrayList<Order> order = new ArrayList<Order>();
		
        switch(orderNumber) {
        case 0: 
        	order.add(Order.asc("imie"));
        	break;
        case 1:
        	order.add(Order.desc("imie"));
	        break;
        case 2: 
        	order.add(Order.asc("nazwisko"));
        	break;
        case 3:
        	order.add(Order.desc("nazwisko"));
	        break; 
        case 4: 
        	order.add(Order.asc("m.nazwa"));
        	order.add(Order.asc("u.nazwa"));
        	order.add(Order.asc("w.nazwa"));
        	break;
        case 5:
        	order.add(Order.desc("m.nazwa"));
        	order.add(Order.desc("u.nazwa"));
        	order.add(Order.desc("w.nazwa"));
	        break;
        }
        repository.setOrderBy(order);
        
        // gdy ciąg wyszukiwania jest > 2 znaki
        if(tfSearch.getText().length() > 2) {
        	repository.setSearchText(tfSearch.getText());
        } else {
        	repository.setSearchText(null);
        }
        
		((QuickPacjenciTableModel)table.getModel()).setModelData(repository.getPacjenci()); // przypisanie nowych danych
		logger.trace("SortBy id " + orderNumber);
	}
	
	/**
	 * Aktualizacja całej zawartości tabeli
	 */
	public void updateTable() {
		((QuickPacjenciTableModel)table.getModel()).setModelData(repository.getPacjenci());
	}
	
	
	/**
	 * Zatwiedzenie wyboru
	 */
	private void okAction() {
		if(table.getSelectedRow() > -1) {
			selectItem();
		}
	}
	
	/**
	 * anulowanie wyboru
	 */
	private void cancelAction() {
		result = false;
		this.setVisible(false);
	}
	
	
	private void selectItem() {
		result = true;
		pacjent = ((QuickPacjenciTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		this.setVisible(false);
	}
	
	/**
	 * Model danych tabeli
	 */
	public class QuickPacjenciTableModel extends AbstractTableModel {
		 
		private static final long serialVersionUID = 1L;
		
		private ArrayList<PacjentEnt> data = null; // lista danych
		private final Object[] columnNames = {"Imię", "Nazwisko", "Adres", "Lekarz"}; // nazwy w Header
	     
	    private final static int IMIE_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int NAZWISKO_IDX = 1;
	    private final static int ADRES_IDX = 2;
	    private final static int LEKARZ_IDX = 3;
	 
	    public QuickPacjenciTableModel() {}
	     
	    @Override
	    public int getRowCount() {
	        if(data==null) return 0;
	        return data.size();
	    }
	 
	    @Override
	    public int getColumnCount() {
	        return columnNames.length;
	    }
	     
	    @Override // wyciąganie danych z listy
	    public Object getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        PacjentEnt record = data.get(rowIndex);
	        switch (columnIndex) {
	            case IMIE_IDX:
	                return record.getImie();
	            case NAZWISKO_IDX:
	                return record.getNazwisko();
	            case ADRES_IDX:
	                return (
	                		record.getUlica().getMiasto().getNazwa() 
	                		+ " " + record.getUlica().getMiasto().getKod_pocztowy() 
	                		+ " (" + record.getUlica().getMiasto().getWojewodztwo().getNazwa() + ")"
	                		+ ", " + record.getUlica().getNazwa() 
	                		+ " " + record.getBudynek() 
	                		+ " " + record.getMieszkanie()
	                		);
	            case LEKARZ_IDX:
	                return (
	                		record.getPracownik().getImie() 
	                		+ " " +record.getPracownik().getNazwisko() 	                		
	                		); 
	            default:
	                return "";
	        }
	    }
	 
	    @Override
	    public String getColumnName(int column) {
	        return columnNames[column].toString();
	    }
	 
	    @Override
	    public boolean isCellEditable(int row, int column) {
	        return false;
	    }
	     
	    // ustawienie przekazanych danych 
	    public void setModelData(ArrayList<PacjentEnt> pacjenci) {
	       this.data =  pacjenci;
	       this.fireTableDataChanged();
	    }
	    
	    public PacjentEnt getDataRecord(int position) {
	        return this.data.get(position);
	    }
	}

}