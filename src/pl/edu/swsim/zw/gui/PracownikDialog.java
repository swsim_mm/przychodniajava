package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.LokalizacjeRep;
import pl.edu.swsim.zw.entities.MiastoEnt;
import pl.edu.swsim.zw.entities.PracownikEnt;
import pl.edu.swsim.zw.entities.UlicaEnt;
import pl.edu.swsim.zw.entities.WojewodztwoEnt;
import pl.edu.swsim.zw.mutils.PeselValidator;
import pl.edu.swsim.zw.mutils.PwzValidator;

/**
 * Dodawanie/modyfikacja pracownika w oknie
 *
 */
public class PracownikDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	public PracownikEnt pracownik = null;

	private ArrayList<WojewodztwoEnt> wojewodztwa = null;
	private ArrayList<MiastoEnt> miasta = null;
	private ArrayList<UlicaEnt> ulice = null;
	//public UlicaEnt ulica = null;
	private boolean dialogLocked = false;
	public boolean result = false;
	
	
	private final JPanel contentPanel = new JPanel();
	private JTextField tfNumer;
	private JTextField tfImie;
	private JTextField tfNazwisko;
	private JTextField tfPESEL;
	private JTextField tfTelefon;
	private JTextField tfBudynek;
	private JTextField tfMieszkanie;
	private JTextField tfPWZ;
	private JComboBox<String> cbWojewodztwa;
	private JComboBox<String> cbMiasta;
	private JComboBox<String> cbUlice;
	private JTextArea taDodatkowe;
	private JCheckBox chbNiedotyczy;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnNowyAdres;
	private JCheckBox chckbxLekarz;
	
	/**
	 * Create the dialog.
	 */
	public PracownikDialog(Frame parent) {
		super(parent);
		setModal(true);


		setTitle("Pracownik");
		setBounds(100, 100, 630, 408);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.WEST);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.EAST);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.NORTH);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.SOUTH);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 110, 0, 52, 0, 77, 0, 0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				JLabel lblDanePersonalne = new JLabel("Dane personalne");
				lblDanePersonalne.setFont(new Font("Tahoma", Font.PLAIN, 15));
				GridBagConstraints gbc_lblDanePersonalne = new GridBagConstraints();
				gbc_lblDanePersonalne.anchor = GridBagConstraints.WEST;
				gbc_lblDanePersonalne.gridwidth = 5;
				gbc_lblDanePersonalne.insets = new Insets(0, 0, 5, 5);
				gbc_lblDanePersonalne.gridx = 0;
				gbc_lblDanePersonalne.gridy = 0;
				panel.add(lblDanePersonalne, gbc_lblDanePersonalne);
			}
			{
				JLabel lblNumer = new JLabel("Numer");
				lblNumer.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNumer = new GridBagConstraints();
				gbc_lblNumer.insets = new Insets(0, 0, 5, 5);
				gbc_lblNumer.anchor = GridBagConstraints.EAST;
				gbc_lblNumer.gridx = 5;
				gbc_lblNumer.gridy = 0;
				panel.add(lblNumer, gbc_lblNumer);
			}
			{
				tfNumer = new JTextField();
				tfNumer.setFont(new Font("Tahoma", Font.PLAIN, 13));
				tfNumer.setEditable(false);
				GridBagConstraints gbc_tfNumer = new GridBagConstraints();
				gbc_tfNumer.gridwidth = 2;
				gbc_tfNumer.insets = new Insets(0, 0, 5, 0);
				gbc_tfNumer.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfNumer.gridx = 6;
				gbc_tfNumer.gridy = 0;
				panel.add(tfNumer, gbc_tfNumer);
				tfNumer.setColumns(10);
			}
			{
				JLabel lblImi = new JLabel("Imię");
				lblImi.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblImi = new GridBagConstraints();
				gbc_lblImi.anchor = GridBagConstraints.EAST;
				gbc_lblImi.insets = new Insets(0, 0, 5, 5);
				gbc_lblImi.gridx = 0;
				gbc_lblImi.gridy = 1;
				panel.add(lblImi, gbc_lblImi);
			}
			{
				tfImie = new JTextField();
				tfImie.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_tfImie = new GridBagConstraints();
				gbc_tfImie.gridwidth = 7;
				gbc_tfImie.insets = new Insets(0, 0, 5, 0);
				gbc_tfImie.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfImie.gridx = 1;
				gbc_tfImie.gridy = 1;
				panel.add(tfImie, gbc_tfImie);
				tfImie.setColumns(10);
			}
			{
				JLabel lblNazwisko = new JLabel("Nazwisko");
				lblNazwisko.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNazwisko = new GridBagConstraints();
				gbc_lblNazwisko.insets = new Insets(0, 0, 5, 5);
				gbc_lblNazwisko.anchor = GridBagConstraints.EAST;
				gbc_lblNazwisko.gridx = 0;
				gbc_lblNazwisko.gridy = 2;
				panel.add(lblNazwisko, gbc_lblNazwisko);
			}
			{
				tfNazwisko = new JTextField();
				tfNazwisko.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_tfNazwisko = new GridBagConstraints();
				gbc_tfNazwisko.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfNazwisko.insets = new Insets(0, 0, 5, 0);
				gbc_tfNazwisko.gridwidth = 7;
				gbc_tfNazwisko.gridx = 1;
				gbc_tfNazwisko.gridy = 2;
				panel.add(tfNazwisko, gbc_tfNazwisko);
				tfNazwisko.setColumns(10);
			}
			{
				JLabel lblPesel = new JLabel("PESEL");
				lblPesel.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblPesel = new GridBagConstraints();
				gbc_lblPesel.anchor = GridBagConstraints.EAST;
				gbc_lblPesel.insets = new Insets(0, 0, 5, 5);
				gbc_lblPesel.gridx = 0;
				gbc_lblPesel.gridy = 3;
				panel.add(lblPesel, gbc_lblPesel);
			}
			{
				tfPESEL = new JTextField();
				tfPESEL.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_tfPESEL = new GridBagConstraints();
				gbc_tfPESEL.gridwidth = 2;
				gbc_tfPESEL.insets = new Insets(0, 0, 5, 5);
				gbc_tfPESEL.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfPESEL.gridx = 1;
				gbc_tfPESEL.gridy = 3;
				panel.add(tfPESEL, gbc_tfPESEL);
				tfPESEL.setColumns(10);
			}
			{
				JLabel lblPwz = new JLabel("PWZ");
				lblPwz.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblPwz = new GridBagConstraints();
				gbc_lblPwz.anchor = GridBagConstraints.EAST;
				gbc_lblPwz.insets = new Insets(0, 0, 5, 5);
				gbc_lblPwz.gridx = 3;
				gbc_lblPwz.gridy = 3;
				panel.add(lblPwz, gbc_lblPwz);
			}
			{
				tfPWZ = new JTextField();
				tfPWZ.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_tfPWZ = new GridBagConstraints();
				gbc_tfPWZ.gridwidth = 2;
				gbc_tfPWZ.insets = new Insets(0, 0, 5, 5);
				gbc_tfPWZ.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfPWZ.gridx = 4;
				gbc_tfPWZ.gridy = 3;
				panel.add(tfPWZ, gbc_tfPWZ);
				tfPWZ.setColumns(10);
			}
			{
				chbNiedotyczy = new JCheckBox("n/d");
				chbNiedotyczy.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_chbNiedotyczy = new GridBagConstraints();
				gbc_chbNiedotyczy.anchor = GridBagConstraints.WEST;
				gbc_chbNiedotyczy.insets = new Insets(0, 0, 5, 5);
				gbc_chbNiedotyczy.gridx = 6;
				gbc_chbNiedotyczy.gridy = 3;
				panel.add(chbNiedotyczy, gbc_chbNiedotyczy);
			}
			{
				chckbxLekarz = new JCheckBox("Lekarz");
				GridBagConstraints gbc_chckbxLekarz = new GridBagConstraints();
				gbc_chckbxLekarz.insets = new Insets(0, 0, 5, 0);
				gbc_chckbxLekarz.gridx = 7;
				gbc_chckbxLekarz.gridy = 3;
				panel.add(chckbxLekarz, gbc_chckbxLekarz);
			}
			{
				JLabel lblAdresZamieszkaniakoresopondencyjny = new JLabel("Adres zamieszkania/koresopondencyjny");
				lblAdresZamieszkaniakoresopondencyjny.setFont(new Font("Tahoma", Font.PLAIN, 15));
				GridBagConstraints gbc_lblAdresZamieszkaniakoresopondencyjny = new GridBagConstraints();
				gbc_lblAdresZamieszkaniakoresopondencyjny.anchor = GridBagConstraints.WEST;
				gbc_lblAdresZamieszkaniakoresopondencyjny.insets = new Insets(0, 0, 5, 0);
				gbc_lblAdresZamieszkaniakoresopondencyjny.gridwidth = 8;
				gbc_lblAdresZamieszkaniakoresopondencyjny.gridx = 0;
				gbc_lblAdresZamieszkaniakoresopondencyjny.gridy = 4;
				panel.add(lblAdresZamieszkaniakoresopondencyjny, gbc_lblAdresZamieszkaniakoresopondencyjny);
			}
			{
				JLabel lblWojewodztwo = new JLabel("Wojewodztwo");
				lblWojewodztwo.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblWojewodztwo = new GridBagConstraints();
				gbc_lblWojewodztwo.anchor = GridBagConstraints.EAST;
				gbc_lblWojewodztwo.insets = new Insets(0, 0, 5, 5);
				gbc_lblWojewodztwo.gridx = 0;
				gbc_lblWojewodztwo.gridy = 5;
				panel.add(lblWojewodztwo, gbc_lblWojewodztwo);
			}
			{
				cbWojewodztwa = new JComboBox<String>();
				cbWojewodztwa.setFont(new Font("Tahoma", Font.PLAIN, 13));
				cbWojewodztwa.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						updateMiasta();
					}
				});
				GridBagConstraints gbc_cbWojewodztwa = new GridBagConstraints();
				gbc_cbWojewodztwa.gridwidth = 3;
				gbc_cbWojewodztwa.insets = new Insets(0, 0, 5, 5);
				gbc_cbWojewodztwa.fill = GridBagConstraints.HORIZONTAL;
				gbc_cbWojewodztwa.gridx = 1;
				gbc_cbWojewodztwa.gridy = 5;
				panel.add(cbWojewodztwa, gbc_cbWojewodztwa);
			}
			{
				JLabel lblTelefon = new JLabel("Telefon");
				lblTelefon.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblTelefon = new GridBagConstraints();
				gbc_lblTelefon.anchor = GridBagConstraints.EAST;
				gbc_lblTelefon.insets = new Insets(0, 0, 5, 5);
				gbc_lblTelefon.gridx = 4;
				gbc_lblTelefon.gridy = 5;
				panel.add(lblTelefon, gbc_lblTelefon);
			}
			{
				tfTelefon = new JTextField();
				GridBagConstraints gbc_tfTelefon = new GridBagConstraints();
				gbc_tfTelefon.gridwidth = 3;
				gbc_tfTelefon.insets = new Insets(0, 0, 5, 0);
				gbc_tfTelefon.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfTelefon.gridx = 5;
				gbc_tfTelefon.gridy = 5;
				panel.add(tfTelefon, gbc_tfTelefon);
				tfTelefon.setColumns(10);
			}
			{
				JLabel lblMiasto = new JLabel("Miasto");
				lblMiasto.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblMiasto = new GridBagConstraints();
				gbc_lblMiasto.anchor = GridBagConstraints.EAST;
				gbc_lblMiasto.insets = new Insets(0, 0, 5, 5);
				gbc_lblMiasto.gridx = 0;
				gbc_lblMiasto.gridy = 6;
				panel.add(lblMiasto, gbc_lblMiasto);
			}
			{
				cbMiasta = new JComboBox<String>();
				cbMiasta.setFont(new Font("Tahoma", Font.PLAIN, 13));
				cbMiasta.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						updateUlice();
					}
				});
				GridBagConstraints gbc_cbMiasta = new GridBagConstraints();
				gbc_cbMiasta.gridwidth = 3;
				gbc_cbMiasta.insets = new Insets(0, 0, 5, 5);
				gbc_cbMiasta.fill = GridBagConstraints.HORIZONTAL;
				gbc_cbMiasta.gridx = 1;
				gbc_cbMiasta.gridy = 6;
				panel.add(cbMiasta, gbc_cbMiasta);
			}
			{
				btnNowyAdres = new JButton("+");
				btnNowyAdres.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {							
						LokalizacjaDialog lokDialog = new LokalizacjaDialog(parent, null);

						if(lokDialog.result) {
							logger.trace("Próba utworzenia nowej lokalizacji miasta " + lokDialog.ulica.getNazwa() );
							new LokalizacjeRep().setUlica(lokDialog.ulica).AddUlica();
							
							presetLocalization(lokDialog.ulica);
						}
						
						lokDialog.dispose();
					}
				});
				GridBagConstraints gbc_btnNowyAdres = new GridBagConstraints();
				gbc_btnNowyAdres.anchor = GridBagConstraints.WEST;
				gbc_btnNowyAdres.insets = new Insets(0, 0, 5, 5);
				gbc_btnNowyAdres.gridx = 4;
				gbc_btnNowyAdres.gridy = 6;
				panel.add(btnNowyAdres, gbc_btnNowyAdres);
			}
			{
				JLabel lblUlica = new JLabel("Ulica");
				lblUlica.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblUlica = new GridBagConstraints();
				gbc_lblUlica.anchor = GridBagConstraints.EAST;
				gbc_lblUlica.insets = new Insets(0, 0, 5, 5);
				gbc_lblUlica.gridx = 0;
				gbc_lblUlica.gridy = 7;
				panel.add(lblUlica, gbc_lblUlica);
			}
			{
				cbUlice = new JComboBox<String>();
				cbUlice.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_cbUlice = new GridBagConstraints();
				gbc_cbUlice.insets = new Insets(0, 0, 5, 5);
				gbc_cbUlice.fill = GridBagConstraints.HORIZONTAL;
				gbc_cbUlice.gridx = 1;
				gbc_cbUlice.gridy = 7;
				panel.add(cbUlice, gbc_cbUlice);
			}
			{
				JLabel lblBudynek = new JLabel("Budynek");
				lblBudynek.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblBudynek = new GridBagConstraints();
				gbc_lblBudynek.anchor = GridBagConstraints.EAST;
				gbc_lblBudynek.insets = new Insets(0, 0, 5, 5);
				gbc_lblBudynek.gridx = 2;
				gbc_lblBudynek.gridy = 7;
				panel.add(lblBudynek, gbc_lblBudynek);
			}
			{
				tfBudynek = new JTextField();
				tfBudynek.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_tfBudynek = new GridBagConstraints();
				gbc_tfBudynek.gridwidth = 2;
				gbc_tfBudynek.insets = new Insets(0, 0, 5, 5);
				gbc_tfBudynek.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfBudynek.gridx = 3;
				gbc_tfBudynek.gridy = 7;
				panel.add(tfBudynek, gbc_tfBudynek);
				tfBudynek.setColumns(10);
			}
			{
				JLabel lblMieszkanie = new JLabel("Mieszkanie");
				lblMieszkanie.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblMieszkanie = new GridBagConstraints();
				gbc_lblMieszkanie.insets = new Insets(0, 0, 5, 5);
				gbc_lblMieszkanie.anchor = GridBagConstraints.EAST;
				gbc_lblMieszkanie.gridx = 5;
				gbc_lblMieszkanie.gridy = 7;
				panel.add(lblMieszkanie, gbc_lblMieszkanie);
			}
			{
				tfMieszkanie = new JTextField();
				tfMieszkanie.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_tfMieszkanie = new GridBagConstraints();
				gbc_tfMieszkanie.gridwidth = 2;
				gbc_tfMieszkanie.insets = new Insets(0, 0, 5, 0);
				gbc_tfMieszkanie.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfMieszkanie.gridx = 6;
				gbc_tfMieszkanie.gridy = 7;
				panel.add(tfMieszkanie, gbc_tfMieszkanie);
				tfMieszkanie.setColumns(10);
			}
			{
				JLabel lblDodatkowe = new JLabel("Dodatkowe");
				lblDodatkowe.setFont(new Font("Tahoma", Font.PLAIN, 15));
				GridBagConstraints gbc_lblDodatkowe = new GridBagConstraints();
				gbc_lblDodatkowe.insets = new Insets(0, 0, 5, 0);
				gbc_lblDodatkowe.anchor = GridBagConstraints.WEST;
				gbc_lblDodatkowe.gridwidth = 8;
				gbc_lblDodatkowe.gridx = 0;
				gbc_lblDodatkowe.gridy = 8;
				panel.add(lblDodatkowe, gbc_lblDodatkowe);
			}
			{
				taDodatkowe = new JTextArea();
				taDodatkowe.setFont(new Font("Tahoma", Font.PLAIN, 13));
				taDodatkowe.setBorder(UIManager.getBorder("TextField.border"));
				taDodatkowe.setLineWrap(true);
				taDodatkowe.setWrapStyleWord(true);
				GridBagConstraints gbc_taDodatkowe = new GridBagConstraints();
				gbc_taDodatkowe.gridwidth = 8;
				gbc_taDodatkowe.fill = GridBagConstraints.BOTH;
				gbc_taDodatkowe.gridx = 0;
				gbc_taDodatkowe.gridy = 9;
				panel.add(taDodatkowe, gbc_taDodatkowe);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				btnOK = new JButton("OK");
				btnOK.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okAction();
					}
				});
				btnOK.setActionCommand("OK");
				buttonPane.add(btnOK);
				getRootPane().setDefaultButton(btnOK);
			}
			{
				btnCancel = new JButton("Anuluj");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelAction();
					}
				});
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
		}
		
	}
	
	private void setDialogModal() {
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
		//this.setModal(true); --> konstruktor
		this.setVisible(true);
	}
	
	/**
	 * Reguluje formę wyświetlania okna
	 */	
	public void DisplayDialog() {
		this.pracownik = new PracownikEnt();
		presetLocalization(null); //bo nie jest wywoływane readyData, a trzeba wyświetlić listę województw
		setTitle("Nowy Pracownik");
		this.lockDialog(false);
		this.setDialogModal();
	}
	
	public void DisplayDialog(PracownikEnt pracownikInit) {
		ReadyData(pracownikInit);
		setTitle(pracownikInit.getImie() + " " + pracownikInit.getNazwisko() + " (Pracownik)");
		this.lockDialog(false);
		this.setDialogModal();
	}
	
	public void DisplayDialog(PracownikEnt pracownikInit, boolean lockDialog) {
		ReadyData(pracownikInit);
		setTitle(pracownikInit.getImie() + " " + pracownikInit.getNazwisko() + " (Pracownik)");
		this.lockDialog(lockDialog);
		this.setDialogModal();
	}

	/**
	 * Przygotowuje dane okna z podanego pracownika
	 * @param pracownikInit null = nowy pracownik
	 */
	public void ReadyData(PracownikEnt pracownikInit) {
		logger.trace(".");

		pracownik = new PracownikEnt(pracownikInit); // uwolnieie od instancji sesji Hibernate
		
		this.tfNumer.setText(Long.toString(pracownik.getId()));
		this.tfImie.setText(pracownik.getImie());
		this.tfNazwisko.setText(pracownik.getNazwisko());
		this.tfPESEL.setText(pracownik.getPesel());
		this.tfPWZ.setText(pracownik.getPwz());
		if(pracownik.getPwz() == null || pracownik.getPwz().length() < 1)
			this.chbNiedotyczy.setSelected(true);
		this.tfBudynek.setText(pracownik.getBudynek());
		this.tfMieszkanie.setText(pracownik.getMieszkanie());
		this.tfTelefon.setText(pracownik.getTelefon());
		this.taDodatkowe.setText(pracownik.getDodatkowe());
		this.chckbxLekarz.setSelected(pracownik.isLekarz());
		
		if(pracownik.getUlica() == null || pracownik.getUlica().getMiasto() == null || pracownik.getUlica().getMiasto().getWojewodztwo() == null ) {
			logger.error("Wartość dla wpisu pracownika " + pracownik.getId() + "ma uszkodzoną relacje!");
			return;
		}
		
		presetLocalization(pracownik.getUlica());	

	}
	
	/**
	 * Ustawia przekazane wartości lokalizacji w polach
	 * @param lokalizacja
	 */
	public void presetLocalization(UlicaEnt lokalizacja) {
		
		updateWojewodztwa();
		
		if(lokalizacja == null)
			return;
		
		long wojID = lokalizacja.getMiasto().getWojewodztwo().getId();
		
		for(int i = 0; i< wojewodztwa.size(); i++) {
			if(wojewodztwa.get(i).getId() == wojID) {
				cbWojewodztwa.setSelectedIndex(i);
				break;
			}	
		}
		
		if(cbWojewodztwa.getSelectedIndex() == -1) {
			logger.error("Nie znaleziono wojewodztwa id " 
					+ lokalizacja.getMiasto().getWojewodztwo().getId() 
					+ " nazwa " 
					+ lokalizacja.getMiasto().getWojewodztwo().getNazwa());
			return;
		}
		
		
		updateMiasta();
		
		long miastoID = lokalizacja.getMiasto().getId();
		
		for(int i = 0; i< miasta.size(); i++) {
			if(miasta.get(i).getId() == miastoID) {
				cbMiasta.setSelectedIndex(i);
				break;
			}	
		}
		
		if(cbWojewodztwa.getSelectedIndex() == -1) {
			logger.error("Nie znaleziono wojewodztwa id " 
					+ lokalizacja.getMiasto().getId() 
					+ " nazwa " 
					+ lokalizacja.getMiasto().getNazwa());
			return;
		}
		
		updateUlice();
		
		long ulicaID = lokalizacja.getId();
		
		for(int i = 0; i< ulice.size(); i++) {
			if(ulice.get(i).getId() == ulicaID) {
				cbUlice.setSelectedIndex(i);
				break;
			}	
		}
		
		if(cbUlice.getSelectedIndex() == -1) {
			logger.error("Nie znaleziono wojewodztwa id " 
					+ lokalizacja.getId() 
					+ " nazwa " 
					+ lokalizacja.getNazwa());
			return;
		}
		
	}
	
	/**
	 * Aktualizuje listę województw
	 */
	public void updateWojewodztwa() {
		((DefaultComboBoxModel<String>)cbWojewodztwa.getModel()).removeAllElements();
		
		ArrayList<Order> order = new ArrayList<Order>();
		Order.asc("nazwa");
		
		wojewodztwa = new LokalizacjeRep().setOrderBy(order).getWojewodztwa();
		for(WojewodztwoEnt woj: wojewodztwa) {
			cbWojewodztwa.addItem(woj.getNazwa());
		}
	}
	
	/**
	 * Aktualizuje listę miast
	 */
	public void updateMiasta() {
		logger.trace("Wojewodztwa: " + wojewodztwa.size() + ", Sel index: " + cbWojewodztwa.getSelectedIndex());
		if(wojewodztwa.size() > 0 
				&& cbWojewodztwa.getSelectedIndex() < wojewodztwa.size()
				&& cbWojewodztwa.getSelectedIndex() > -1) {
			
			((DefaultComboBoxModel<String>)cbMiasta.getModel()).removeAllElements();
			
			ArrayList<Order> order = new ArrayList<Order>();
			Order.asc("nazwa");
			
			miasta = new LokalizacjeRep()
				.setOrderBy(order)
				.setWojewodztwo(wojewodztwa.get(cbWojewodztwa.getSelectedIndex()))
				.getMiasta();

			for(MiastoEnt miast: miasta) {
				cbMiasta.addItem(miast.getNazwa() + " (" + miast.getKod_pocztowy() + ")");
			}
		}
	}
	
	/**
	 * Aktualizuje listę ulic
	 */
	public void updateUlice() {
		logger.trace("Miasta: " + miasta.size() + ", Sel index: " + cbMiasta.getSelectedIndex());
		if(miasta.size() > 0 
				&& cbMiasta.getSelectedIndex() < miasta.size()
				&& cbMiasta.getSelectedIndex() > -1) {
			logger.trace("...");
			
			((DefaultComboBoxModel<String>)cbUlice.getModel()).removeAllElements();
			
			ArrayList<Order> order = new ArrayList<Order>();
			Order.asc("nazwa");
			
			ulice = new LokalizacjeRep()
				.setOrderBy(order)
				.setMiasto(miasta.get(cbMiasta.getSelectedIndex()))
				.getUlice();
			logger.trace("...");
			for(UlicaEnt ulica: ulice) {
				cbUlice.addItem(ulica.getNazwa());
			}
		}
	}
	
	public void okAction() {
		result = true;
		
		if(dialogLocked) { // dialog tylko do wyświetlenia informacji
			setVisible(false);
			return;
		}
		
		Frame parent = (Frame)SwingUtilities.getWindowAncestor(this);
		
		// Testy poprawoności danych
		
		// Imię i nazwisko
		if ( tfImie.getText().length() < 1 || tfNazwisko.getText().length() < 1
				|| !Pattern.matches("^([A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\\s\\-])+$", tfImie.getText())
				|| !Pattern.matches("^([A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\\s\\-])+$", tfNazwisko.getText()) ) {
			JOptionPane.showMessageDialog(parent,
				    "Imię i nazwisko są niepoprawne!",
				    "Złe imię i nazwisko",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("zła nazwa");
			return;
		}
		
		//pesel
		PeselValidator psv = new PeselValidator(tfPESEL.getText());
		if (!psv.isValid()) {
			JOptionPane.showMessageDialog(parent,
				    "Numer pesel jest niepoprawny!",
				    "Zły numer PESEL",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("zły PESEL");
			return;
		}
		
		// prawo wykonywania zawodu
		if(!chbNiedotyczy.isSelected()) {
			
			if(tfPWZ.getText().length() < 1) {
				JOptionPane.showMessageDialog(parent,
					    "Brak numeru Prawa Wykonywania Zawodu !\nJeśli nie chcesz go definiować zaznacz niedotyczy.",
					    "Brak numeru PWZ",
					    JOptionPane.ERROR_MESSAGE);	
				result = false;
				logger.info("zły PWZ");
				return;
			}
			
			PwzValidator pwv = new PwzValidator(tfPWZ.getText());
			if(!pwv.isValid()) {
				JOptionPane.showMessageDialog(parent,
					    "Numeru Prawa Wykonywania Zawodu jest niepoprawny!.",
					    "Zły numeru PWZ",
					    JOptionPane.ERROR_MESSAGE);	
				result = false;
				logger.info("zły PWZ");
				return;
			}
			
		}
		
		// wybrana lokalizacja
		if(cbWojewodztwa.getSelectedIndex() == -1 
				|| cbMiasta.getSelectedIndex() == -1
				|| cbUlice.getSelectedIndex() == -1) {
			JOptionPane.showMessageDialog(parent,
				    "Należy wskazać miasto, województwo oraz ulicę adresu zamieszkania\nJeśli taka lokalizacja nie istnieje na liście trzeba ją doać",
				    "Uzupełnij dane",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("Brak wybranego wojewodztwa, miasta lub ulicy");
			return;
		}


		if(result) {
			
			this.pracownik.setImie(this.tfImie.getText());
			this.pracownik.setNazwisko(this.tfNazwisko.getText());
			this.pracownik.setPesel(this.tfPESEL.getText());
			
			if(!chbNiedotyczy.isSelected())
				this.pracownik.setPwz(this.tfPWZ.getText());
			else
				this.pracownik.setPwz(null);
			
			this.pracownik.setBudynek(this.tfBudynek.getText());
			this.pracownik.setMieszkanie(this.tfMieszkanie.getText());
			this.pracownik.setTelefon(this.tfTelefon.getText());
			this.pracownik.setDodatkowe(this.taDodatkowe.getText()); //TODO limitacja znaków
			this.pracownik.setLekarz(this.chckbxLekarz.isSelected());
			
			this.pracownik.setUlica(new UlicaEnt(this.ulice.get(this.cbUlice.getSelectedIndex())));
				
			setVisible(false);
		}
	}
	
	/**
	 * Anulowanie okna
	 */
	public void cancelAction() {
		result = false;
		setVisible(false);
	}
	
	/**
	 * Blokuje kotrolki edycji jeśli okno jest tylko do podglądu
	 * @param doLock
	 */
	private void lockDialog(boolean doLock) {
		this.dialogLocked = doLock;
		
		boolean enableControl = !doLock;
		
		this.tfImie.setEditable(enableControl);
		this.tfNazwisko.setEditable(enableControl);
		this.tfPESEL.setEditable(enableControl);
		this.tfPWZ.setEditable(enableControl);
		this.chbNiedotyczy.setEnabled(enableControl);
		this.tfBudynek.setEditable(enableControl);
		this.tfMieszkanie.setEditable(enableControl);
		this.tfTelefon.setEditable(enableControl);
		this.cbWojewodztwa.setEnabled(enableControl);
		this.cbUlice.setEnabled(enableControl);
		this.cbMiasta.setEnabled(enableControl);
		this.taDodatkowe.setEditable(enableControl);
		
		this.btnCancel.setEnabled(enableControl);
		this.chckbxLekarz.setEnabled(enableControl);
		this.btnNowyAdres.setEnabled(enableControl);
	}

}
