package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.GabinetyRep;
import pl.edu.swsim.zw.entities.GabinetEnt;
import java.awt.Font;
import javax.swing.ImageIcon;

public class GabinetyIntFrame extends JInternalFrame {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private GabinetyRep repository = new GabinetyRep();
	
	private final String[] sortOptions = {
			"Numer rosnąco [a-z]",
			"Numer malejąco [z-a]",
			"Nazwa rosnąco [a-z]",
			"Nazwa malejąco [z-a]",
	};
	
	private JTextField tfSearch;
	private JTable table;
	private JComboBox<String> cbOrder;


	/**
	 * Create the frame.
	 */
	public GabinetyIntFrame() {
		
		GabinetyIntFrame that = this;
		
		setTitle("Gabinety");
		setResizable(true);
		setMaximizable(true);
		setClosable(true);
		setBounds(100, 100, 645, 447);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut_2, BorderLayout.EAST);
		
		Component verticalStrut_1 = Box.createVerticalStrut(5);
		panel.add(verticalStrut_1, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 116, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnR = new JButton("");
		btnR.setMargin(new Insets(0, 2, 0, 2));
		btnR.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnR.setIcon(new ImageIcon(GabinetyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/refresh2.png")));
		btnR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		GridBagConstraints gbc_btnR = new GridBagConstraints();
		gbc_btnR.insets = new Insets(0, 0, 0, 5);
		gbc_btnR.gridx = 0;
		gbc_btnR.gridy = 0;
		panel_1.add(btnR, gbc_btnR);
		
		JLabel lblSortujWedug = new JLabel("Sortuj według");
		lblSortujWedug.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSortujWedug = new GridBagConstraints();
		gbc_lblSortujWedug.insets = new Insets(0, 0, 0, 5);
		gbc_lblSortujWedug.anchor = GridBagConstraints.EAST;
		gbc_lblSortujWedug.gridx = 1;
		gbc_lblSortujWedug.gridy = 0;
		panel_1.add(lblSortujWedug, gbc_lblSortujWedug);
		
		cbOrder = new JComboBox<String>();
		cbOrder.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cbOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		cbOrder.setModel(new DefaultComboBoxModel<String>(sortOptions));
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 0, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 2;
		gbc_comboBox.gridy = 0;
		panel_1.add(cbOrder, gbc_comboBox);
		
		JLabel lblSzukaj = new JLabel("Szukaj");
		lblSzukaj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSzukaj = new GridBagConstraints();
		gbc_lblSzukaj.insets = new Insets(0, 0, 0, 5);
		gbc_lblSzukaj.anchor = GridBagConstraints.EAST;
		gbc_lblSzukaj.gridx = 3;
		gbc_lblSzukaj.gridy = 0;
		panel_1.add(lblSzukaj, gbc_lblSzukaj);
		
		tfSearch = new JTextField();
		tfSearch.setFont(new Font("Tahoma", Font.PLAIN, 13));
		tfSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
		        	setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 0, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 4;
		gbc_textField.gridy = 0;
		panel_1.add(tfSearch, gbc_textField);
		tfSearch.setColumns(10);
		
		JButton btnDodaj = new JButton("");
		btnDodaj.setMargin(new Insets(0, 2, 0, 2));
		btnDodaj.setIcon(new ImageIcon(GabinetyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/add.png")));
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewItem();
			}
		});
		GridBagConstraints gbc_btnR1 = new GridBagConstraints();
		gbc_btnR1.insets = new Insets(0, 0, 0, 5);
		gbc_btnR1.gridx = 5;
		gbc_btnR1.gridy = 0;
		panel_1.add(btnDodaj, gbc_btnR1);
		
		JButton btnEdytuj = new JButton("");
		btnEdytuj.setMargin(new Insets(0, 2, 0, 2));
		btnEdytuj.setIcon(new ImageIcon(GabinetyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/edit.png")));
		btnEdytuj.setEnabled(false);
		btnEdytuj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editItem();
			}
		});
		GridBagConstraints gbc_btnE = new GridBagConstraints();
		gbc_btnE.insets = new Insets(0, 0, 0, 5);
		gbc_btnE.gridx = 6;
		gbc_btnE.gridy = 0;
		panel_1.add(btnEdytuj, gbc_btnE);
		
		JButton btnUsun = new JButton("");
		btnUsun.setMargin(new Insets(0, 2, 0, 2));
		btnUsun.setIcon(new ImageIcon(GabinetyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/del.png")));
		btnUsun.setEnabled(false);
		btnUsun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeItem();
			}
		});
		GridBagConstraints gbc_btnU = new GridBagConstraints();
		gbc_btnU.gridx = 7;
		gbc_btnU.gridy = 0;
		panel_1.add(btnUsun, gbc_btnU);
		
		Component verticalStrut = Box.createVerticalStrut(5);
		panel.add(verticalStrut, BorderLayout.SOUTH);
    	
    	JPanel panel_2 = new JPanel();
    	getContentPane().add(panel_2, BorderLayout.CENTER);
    	panel_2.setLayout(new BorderLayout(0, 0));
    	
    	Component verticalStrut_2 = Box.createVerticalStrut(5);
    	panel_2.add(verticalStrut_2, BorderLayout.NORTH);
    	
    	Component horizontalStrut_1 = Box.createHorizontalStrut(5);
    	panel_2.add(horizontalStrut_1, BorderLayout.EAST);
    	
    	Component horizontalStrut_3 = Box.createHorizontalStrut(5);
    	panel_2.add(horizontalStrut_3, BorderLayout.WEST);
    	
    	Component verticalStrut_3 = Box.createVerticalStrut(5);
    	panel_2.add(verticalStrut_3, BorderLayout.SOUTH);
    	
    	JScrollPane scrollPane = new JScrollPane();
    	panel_2.add(scrollPane, BorderLayout.CENTER);
    	
    	table = new JTable();
    	table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    	scrollPane.setViewportView(table);
    	
    	table.setModel(new GabinetyTableModel());
    	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    	table.getTableHeader().setReorderingAllowed(false);
		scrollPane.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
			    table.getColumnModel().getColumn(0).setPreferredWidth((int)(that.getWidth() * 0.10));
			    table.getColumnModel().getColumn(1).setPreferredWidth((int)(that.getWidth() * 0.80));
			}
		});
		
	    table.getTableHeader().addMouseListener(new MouseAdapter() {
	        @Override
	        public void mouseClicked(MouseEvent e) {
	            int col = table.columnAtPoint(e.getPoint());
	            
	            if(cbOrder.getModel().getSize() < (col*2)+2)
	            	return;
	            if(col*2 == cbOrder.getSelectedIndex())
	            	setOrderSort((col*2)+1);
	            else 
	            	setOrderSort(col*2);
	        }
	    });
	    table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(table.getSelectedRow() > -1) {
	            	btnEdytuj.setEnabled(true);
	            	btnUsun.setEnabled(true);
				} else {
	            	btnEdytuj.setEnabled(false);
	            	btnUsun.setEnabled(false);
				}
			}
		});
	    
		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem menuItemAdd = new JMenuItem("Dodaj nowy rekord");
		JMenuItem menuItemEdit = new JMenuItem("Edytuj");
		JMenuItem menuItemRemove = new JMenuItem("Usuń");
		JMenuItem menuItemRefresh = new JMenuItem("Odśwież");
		popupMenu.add(menuItemAdd);
		popupMenu.add(menuItemEdit);
		popupMenu.add(menuItemRemove);
		popupMenu.add(menuItemRefresh);

		menuItemAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
	        	addNewItem();
			}
		});
		
		menuItemEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				editItem();
			}
		});
		
		menuItemRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				removeItem();
			}
		});
		menuItemRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				updateTable();
			}
		});
		
	    table.addMouseListener(new MouseAdapter() {
	        @Override
	    	public void mouseClicked(MouseEvent event) {
	            int currentRow = table.rowAtPoint(event.getPoint());
	            table.setRowSelectionInterval(currentRow, currentRow);

	            if (event.getButton() == java.awt.event.MouseEvent.BUTTON3) {
	                popupMenu.show(table, event.getX(), event.getY());
	            }
	    	}
	        
	    });
	    
	    setOrderSort(cbOrder.getSelectedIndex());
	    
	}
	
	/**
	 * Definicja filtrów sortowania
	 * @param orderNumber
	 */
	private void setOrderSort(int orderNumber) {
		logger.trace(".");
		
		cbOrder.setSelectedIndex(orderNumber);
		
		ArrayList<Order> order = new ArrayList<Order>();
		
        switch(orderNumber) {
        case 0: 
        	order.add( Order.asc("numer"));
        	break;
        case 1:
        	order.add(Order.desc("numer"));
	        break; 
        case 2: 
        	order.add(Order.asc("nazwa"));
        	break;
        case 3:
        	order.add(Order.desc("nazwa"));
	        break;
 
        }
        repository.setOrderBy(order);
        
        if(tfSearch.getText().length() > 2) {
        	repository.setSearchText(tfSearch.getText());
        } else {
        	repository.setSearchText(null);
        }
        
		((GabinetyTableModel)table.getModel()).setModelData(repository.getGabinety());
		logger.trace("SortBy id " + orderNumber);
	}

	/**
	 * Akcja dodawania nowego elementu wywołana wewnątrz klasy
	 */
	public void addNewItem() {
		addNewItem((Frame)SwingUtilities.getWindowAncestor(this));
		this.updateTable();
	}
	
	/**
	 * Akcja dodawania nowego elementu wywołana z zewnątrz (bez obiektu)
	 * @param parent
	 */
	public static void addNewItem(Frame parent) {
		logger.trace("Dodawanie nowego gabinetu");
		GabinetDialog gabinetDialog = new GabinetDialog(parent);
		gabinetDialog.DisplayDialog();
		
		if(gabinetDialog.result) {
			logger.trace("Próba utworzenia nowego gabinetua " + gabinetDialog.gabinet.getNazwa() );
			new GabinetyRep().setGabinet(gabinetDialog.gabinet).AddGabinet();
		}
			
		gabinetDialog.dispose();
	}
	
	/**
	 * Edycja zaznaczonego elementu
	 * @param rowNum
	 */
	public void editItem() {
		logger.trace("Edycja gabinetu");
		GabinetEnt gabinet = ((GabinetyTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		
		GabinetDialog gabinetDialog = new GabinetDialog((Frame)SwingUtilities.getWindowAncestor(this));
		gabinetDialog.DisplayDialog(gabinet);
		
		if(gabinetDialog.result) {
			logger.trace("Edytowany gabinet " + gabinetDialog.gabinet.getNazwa());
			new GabinetyRep().setGabinet(gabinetDialog.gabinet).EditGabinet();
		}
			
		gabinetDialog.dispose();
		this.updateTable();
	}
	
	/**
	 * Usuwanie zaznaczonego elementu
	 * @param rowNum
	 */
	public void removeItem() {
		logger.trace("Usuwanie gabinetu");
		
		GabinetEnt gabinet = ((GabinetyTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		logger.trace("Usuwanie: " + gabinet.getNazwa());
		
		if(JOptionPane.showConfirmDialog((Frame)SwingUtilities.getWindowAncestor(this), 
				"Czy na pewno chesz usunąć " 
						+ gabinet.getNazwa() + " ?" , 
				"Usuwanie rekordu", 
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			GabinetEnt gab = new GabinetEnt(gabinet);
			new GabinetyRep().setGabinet(gab).DeleteGabinet();
		}
			
		this.updateTable();
	}
	
	/**
	 * Aktualizacja całej zawartości tabeli
	 */
	public void updateTable() {
		((GabinetyTableModel)table.getModel()).setModelData(repository.getGabinety());
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * Model danych tabeli
	 *
	 */
	public class GabinetyTableModel extends AbstractTableModel {
		 
		private static final long serialVersionUID = 1L;
		
		private ArrayList<GabinetEnt> data = null;
		private final Object[] columnNames = {"Numer","Nazwa"};
	     
	    private final static int NUMER_IDX = 0;
	    private final static int NAZWA_IDX = 1;

	    public GabinetyTableModel() {}
	     
	    @Override
	    public int getRowCount() {
	        if(data==null) return 0;
	        return data.size();
	    }
	 
	    @Override
	    public int getColumnCount() {
	        return columnNames.length;
	    }
	     
	    @Override
	    public Object getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        GabinetEnt record = data.get(rowIndex);
	        switch (columnIndex) {
            	case NUMER_IDX:
            		return record.getNumer();
	            case NAZWA_IDX:
	                return record.getNazwa();
	            default:
	                return "";
	        }
	    }
	 
	    @Override
	    public String getColumnName(int column) {
	        return columnNames[column].toString();
	    }
	 
	    @Override
	    public boolean isCellEditable(int row, int column) {
	        return false;
	    }
	     
	    public void setModelData(ArrayList<GabinetEnt> gabinety) {
	    	logger.trace(gabinety.size());
	    	this.data =  gabinety;
	    	this.fireTableDataChanged();
	    }
	    
	    public GabinetEnt getDataRecord(int position) {
	        return this.data.get(position);
	    }
	  
	}

}
