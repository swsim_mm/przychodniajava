package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.RozpoznaniaRep;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.RozpoznanieEnt;

import javax.swing.ImageIcon;

import java.awt.Font;

import javax.swing.JTextArea;
import javax.swing.UIManager;

/**
 * Lista rozpoznań
 */
public class RozpoznaniaIntFrame extends JInternalFrame {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private RozpoznaniaRep repository = new RozpoznaniaRep();
	private PacjentEnt pacjent = null;

	/*
	 * Sortowanie
	 */
	private final String[] sortOptions = {
			"Data [min-max]",
			"Data [max-min]",
			"Pacjent rosnąco [a-z]",
			"Pacjent malejąco [z-a]",
			"Lekarz rosnąco [a-z]",
			"Lekarz malejąco [z-a]",		
	};
	
	private JTextField tfSearch;
	private JTable table;
	private JComboBox<String> cbOrder;
	private JTextArea taDanePacjenta;


	/**
	 * Create the frame.
	 */
	public RozpoznaniaIntFrame() {
		
		// szybkie obejście this - klasy anonimowe, 
		// nadal dostęp do tej klasy przez that
		RozpoznaniaIntFrame that = this;
				
		setMaximizable(true);
		setTitle("Rozpoznania");
		setResizable(true);
		setClosable(true);
		setBounds(100, 100, 643, 398);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut_1, BorderLayout.EAST);
		
		Component verticalStrut = Box.createVerticalStrut(3);
		panel.add(verticalStrut, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 150, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnDodaj = new JButton("");
		btnDodaj.setIcon(new ImageIcon(RozpoznaniaIntFrame.class.getResource("/pl/edu/swsim/zw/icon/add.png")));
		btnDodaj.setMargin(new Insets(0, 2, 0, 2));
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewItem();
			}
		});
		
		
		cbOrder = new JComboBox<String>();
		cbOrder.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cbOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex()); 
			}
		});
		
		taDanePacjenta = new JTextArea();
		taDanePacjenta.setFont(new Font("Tahoma", Font.PLAIN, 12));
		taDanePacjenta.setText("Wskaż pacjenta");
		taDanePacjenta.setRows(3);
		taDanePacjenta.setEditable(false);
		taDanePacjenta.setBorder(UIManager.getBorder("TextField.border"));
		GridBagConstraints gbc_taDanePacjenta = new GridBagConstraints();
		gbc_taDanePacjenta.gridheight = 2;
		gbc_taDanePacjenta.insets = new Insets(0, 0, 0, 5);
		gbc_taDanePacjenta.fill = GridBagConstraints.BOTH;
		gbc_taDanePacjenta.gridx = 1;
		gbc_taDanePacjenta.gridy = 0;
		panel_1.add(taDanePacjenta, gbc_taDanePacjenta);
		
		JButton btnPacjent = new JButton("Pacjent");
		btnPacjent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectPacjentAction();
			}
		});
		btnPacjent.setMargin(new Insets(2, 5, 2, 5));
		GridBagConstraints gbc_btnPacjent = new GridBagConstraints();
		gbc_btnPacjent.fill = GridBagConstraints.VERTICAL;
		gbc_btnPacjent.gridheight = 2;
		gbc_btnPacjent.insets = new Insets(0, 0, 0, 5);
		gbc_btnPacjent.gridx = 2;
		gbc_btnPacjent.gridy = 0;
		panel_1.add(btnPacjent, gbc_btnPacjent);
		
		JLabel lblSortujWedug = new JLabel("Sortuj wg");
		lblSortujWedug.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSortujWedug = new GridBagConstraints();
		gbc_lblSortujWedug.insets = new Insets(0, 0, 5, 5);
		gbc_lblSortujWedug.anchor = GridBagConstraints.EAST;
		gbc_lblSortujWedug.gridx = 3;
		gbc_lblSortujWedug.gridy = 0;
		panel_1.add(lblSortujWedug, gbc_lblSortujWedug);
		cbOrder.setModel(new DefaultComboBoxModel<String>(sortOptions));
		GridBagConstraints gbc_cbOrder = new GridBagConstraints();
		gbc_cbOrder.insets = new Insets(0, 0, 5, 5);
		gbc_cbOrder.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbOrder.gridx = 4;
		gbc_cbOrder.gridy = 0;
		panel_1.add(cbOrder, gbc_cbOrder);
		setOrderSort(cbOrder.getSelectedIndex());
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 5;
		gbc_btnNewButton.gridy = 0;
		panel_1.add(btnDodaj, gbc_btnNewButton);
		
		JButton btnEdytuj = new JButton("");
		btnEdytuj.setIcon(new ImageIcon(RozpoznaniaIntFrame.class.getResource("/pl/edu/swsim/zw/icon/edit.png")));
		btnEdytuj.setMargin(new Insets(0, 2, 0, 2));
		btnEdytuj.setEnabled(false);
		btnEdytuj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editItem();
			}
		});
		GridBagConstraints gbc_btnEdytuj = new GridBagConstraints();
		gbc_btnEdytuj.insets = new Insets(0, 0, 5, 5);
		gbc_btnEdytuj.gridx = 6;
		gbc_btnEdytuj.gridy = 0;
		panel_1.add(btnEdytuj, gbc_btnEdytuj);
		
		JButton btnUsun = new JButton("");
		btnUsun.setIcon(new ImageIcon(RozpoznaniaIntFrame.class.getResource("/pl/edu/swsim/zw/icon/del.png")));
		btnUsun.setMargin(new Insets(0, 2, 0, 2));
		btnUsun.setEnabled(false);
		btnUsun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeItem();
			}
		});
		GridBagConstraints gbc_btnUsun = new GridBagConstraints();
		gbc_btnUsun.insets = new Insets(0, 0, 5, 0);
		gbc_btnUsun.gridx = 7;
		gbc_btnUsun.gridy = 0;
		panel_1.add(btnUsun, gbc_btnUsun);
		
		JButton btnPodglad = new JButton("Podgląd");
		btnPodglad.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnPodglad.setEnabled(false);
		btnPodglad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				viewItem();
			}
		});
		
		tfSearch = new JTextField();
		tfSearch.setFont(new Font("Tahoma", Font.PLAIN, 13));
		tfSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
		        	setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		
		JButton btnR = new JButton("");
		btnR.setIcon(new ImageIcon(RozpoznaniaIntFrame.class.getResource("/pl/edu/swsim/zw/icon/refresh2.png")));
		btnR.setMargin(new Insets(0, 2, 0, 2));
		btnR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		GridBagConstraints gbc_btnR = new GridBagConstraints();
		gbc_btnR.insets = new Insets(0, 0, 0, 5);
		gbc_btnR.gridx = 0;
		gbc_btnR.gridy = 1;
		panel_1.add(btnR, gbc_btnR);
		
		JLabel lblSzukaj = new JLabel("Szukaj");
		lblSzukaj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSzukaj = new GridBagConstraints();
		gbc_lblSzukaj.insets = new Insets(0, 0, 0, 5);
		gbc_lblSzukaj.anchor = GridBagConstraints.EAST;
		gbc_lblSzukaj.gridx = 3;
		gbc_lblSzukaj.gridy = 1;
		panel_1.add(lblSzukaj, gbc_lblSzukaj);
		GridBagConstraints gbc_tfSearch = new GridBagConstraints();
		gbc_tfSearch.insets = new Insets(0, 0, 0, 5);
		gbc_tfSearch.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfSearch.gridx = 4;
		gbc_tfSearch.gridy = 1;
		panel_1.add(tfSearch, gbc_tfSearch);
		tfSearch.setColumns(10);
		btnPodglad.setEnabled(false);
		GridBagConstraints gbc_btnPodglad = new GridBagConstraints();
		gbc_btnPodglad.gridwidth = 3;
		gbc_btnPodglad.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPodglad.gridx = 5;
		gbc_btnPodglad.gridy = 1;
		panel_1.add(btnPodglad, gbc_btnPodglad);
		
		Component verticalStrut_1 = Box.createVerticalStrut(0);
		panel.add(verticalStrut_1, BorderLayout.SOUTH);
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		table.setModel(new RozpoznaniaTableModel());
    	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    	table.getTableHeader().setReorderingAllowed(false);
    	scrollPane.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
			    table.getColumnModel().getColumn(0).setPreferredWidth((int)(that.getWidth() * 0.20));
			    table.getColumnModel().getColumn(1).setPreferredWidth((int)(that.getWidth() * 0.20));
			    table.getColumnModel().getColumn(2).setPreferredWidth((int)(that.getWidth() * 0.15));
			    table.getColumnModel().getColumn(3).setPreferredWidth((int)(that.getWidth() * 0.35));
			}
		});
		
    	/*
    	 *  menu kontekstowe
    	 */
	    table.getTableHeader().addMouseListener(new MouseAdapter() {
	        @Override
	        public void mouseClicked(MouseEvent e) {
	            int col = table.columnAtPoint(e.getPoint());
	            
	            if(cbOrder.getModel().getSize() < (col*2)+2)
	            	return;
	            if(col*2 == cbOrder.getSelectedIndex())
	            	setOrderSort((col*2)+1);
	            else 
	            	setOrderSort(col*2);
	        }
	    });
	    table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(table.getSelectedRow() > -1) {
	            	btnEdytuj.setEnabled(true);
	            	btnUsun.setEnabled(true);
	            	btnPodglad.setEnabled(true);
				} else {
	            	btnEdytuj.setEnabled(false);
	            	btnUsun.setEnabled(false);
	            	btnPodglad.setEnabled(false);
				}
			}
		});

		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem menuItemView = new JMenuItem("Podgląd");
		JMenuItem menuItemAdd = new JMenuItem("Dodaj nowy rekord");
		JMenuItem menuItemEdit = new JMenuItem("Edytuj");
		JMenuItem menuItemRemove = new JMenuItem("Usuń");
		JMenuItem menuItemRefresh = new JMenuItem("Odśwież");
		popupMenu.add(menuItemView);
		popupMenu.add(menuItemAdd);
		popupMenu.add(menuItemEdit);
		popupMenu.add(menuItemRemove);
		popupMenu.add(menuItemRefresh);

		menuItemView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				viewItem();
			}
		});
		
		menuItemAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
	        	addNewItem();
			}
		});
		
		menuItemEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				editItem();
			}
		});
		
		menuItemRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				removeItem();
			}
		});
		menuItemRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				updateTable();
			}
		});
    	
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event) {
	            int currentRow = table.rowAtPoint(event.getPoint());
	            table.setRowSelectionInterval(currentRow, currentRow);
	            
	            if (event.getButton() == java.awt.event.MouseEvent.BUTTON1) {
	                if(event.getClickCount() >= 2)
	                	viewItem();
	            }
	               
	            if (event.getButton() == java.awt.event.MouseEvent.BUTTON3) {
	                popupMenu.show(table, event.getX(), event.getY());
	            }
			}
		});

	}
	
	/**
	 * Definicja filtrów sortowania
	 * @param orderNumber
	 */
	private void setOrderSort(int orderNumber) {
		
		logger.trace(".");
		
		if(this.pacjent == null)
			return;
		
		cbOrder.setSelectedIndex(orderNumber);
		
		ArrayList<Order> order = new ArrayList<Order>();
		
        switch(orderNumber) {
        case 0: 
        	order.add(Order.asc("w.termin"));
        	break;
        case 1:
        	order.add(Order.desc("w.termin"));
	        break; 
	        
        case 2: 
        	order.add(Order.asc("pa.nazwisko"));
        	order.add(Order.asc("pa.imie"));
        	break;
        case 3:
        	order.add(Order.desc("pa.nazwisko"));
        	order.add(Order.desc("pa.imie"));
	        break;
        case 4: 
        	order.add(Order.asc("pr.nazwisko"));
        	order.add(Order.asc("pr.imie"));
        	break;
        case 5:
        	order.add(Order.desc("pr.nazwisko"));
        	order.add(Order.desc("pr.imie"));
	        break;
        }
        
        repository.setOrderBy(order);
        
        // gdy ciąg wyszukiwania jest > 2 znaki
        if(tfSearch.getText().length() > 2) {
        	repository.setSearchText(tfSearch.getText());
        } else {
        	repository.setSearchText(null);
        }
        
        repository.setPacjent(this.pacjent);
        
		((RozpoznaniaTableModel)table.getModel()).setModelData(repository.getRozpoznania()); // przypisanie nowych danych
		logger.trace("SortBy id " + orderNumber);
	
	}
	
	/**
	 * Wyświetlanie danych rozpoznania bez możliwości ich edycji
	 */
	public void viewItem() {
		logger.trace("Widok Rozpoznania");
		RozpoznanieEnt rozpoznanie = ((RozpoznaniaTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		
		RozpoznanieDialog rozpDialog = new RozpoznanieDialog((Frame)SwingUtilities.getWindowAncestor(this));
		rozpDialog.DisplayDialog(rozpoznanie, true);
			
		rozpDialog.dispose();
		this.updateTable();
	}

	/**
	 * Akcja dodawania nowego elementu wywołana wewnątrz klasy
	 */
	public void addNewItem() {
		addNewItem((Frame)SwingUtilities.getWindowAncestor(this));
		this.updateTable();
	}
	
	/**
	 * Akcja dodawania nowego elementu wywołana z zewnątrz (bez obiektu)
	 * @param parent
	 */
	public static void addNewItem(Frame parent) {
		logger.trace("Nowe Rozpozanie");
		RozpoznanieDialog rozpDialog = new RozpoznanieDialog(parent);
		rozpDialog.DisplayDialog();

		if(rozpDialog.result) {
			logger.trace("Próba utworzenia nowego rozpoznania " );
			new RozpoznaniaRep().setRozpoznanie(rozpDialog.rozpoznanie).AddRozpoznanie();
		}
			
		rozpDialog.dispose();
		
	}
	
	/**
	 * Edycja zaznaczonego elementu
	 * @param rowNum
	 */
	public void editItem() {
		logger.trace("Edycja Rozpoznania");
		RozpoznanieEnt rozpoznanie = ((RozpoznaniaTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
	
		RozpoznanieDialog rozpDialog = new RozpoznanieDialog((Frame)SwingUtilities.getWindowAncestor(this));
		rozpDialog.DisplayDialog(rozpoznanie);
		
		if(rozpDialog.result) {
			logger.trace("Edytowane rozpoznanie"  + rozpoznanie.getChoroba().getNazwa() );
			new RozpoznaniaRep().setRozpoznanie(rozpDialog.rozpoznanie).EditRozpoznanie();
		}
			
		rozpDialog.dispose();
		this.updateTable();
	}
	
	/**
	 * Usuwanie zaznaczonego elementu
	 * @param rowNum
	 */
	public void removeItem() {
		logger.trace("Usuwanie Rozpoznania");
		
		RozpoznanieEnt rozpoznanie = ((RozpoznaniaTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		logger.trace("Usuwanie: " );
		
		if(JOptionPane.showConfirmDialog((Frame)SwingUtilities.getWindowAncestor(this), 
				"Czy na pewno chesz usunąć rozpoznanie " 
						 + rozpoznanie.getChoroba().getNazwa() + " ? ", 
				"Usuwanie rozpoznania", 
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			new RozpoznaniaRep().setRozpoznanie(rozpoznanie).DeleteRozpoznanie();
			this.updateTable();
		}
		
	}	
	
	/**
	 * Okno wyboru pacjenta 
	 */
	private void selectPacjentAction() {
		SelectPacjentDialog spd = new SelectPacjentDialog();
		spd.setVisible(true);
		
		if(spd.pacjent != null) {
			presetPacjent(spd.pacjent);
			this.pacjent = spd.pacjent;
		}
		
		setOrderSort(cbOrder.getSelectedIndex());
		
	}
	
	/**
	 * Przesłanie pacjenta przez okno wyboru
	 * @param pacjent
	 */
	public void presetPacjent(PacjentEnt pacjent) {
	
		if(pacjent != null) {
			
			logger.trace(pacjent.toString());
			
			String textData = "";
	
			textData += pacjent.getNazwisko() + " " + pacjent.getImie()
					+ System.lineSeparator();
			textData += pacjent.getUlica().getNazwa() + " " + pacjent.getBudynek()
					+ " " + pacjent.getMieszkanie() + System.lineSeparator();
			textData += pacjent.getUlica().getMiasto().getKod_pocztowy() + " "
					+ pacjent.getUlica().getMiasto().getNazwa()
					+ System.lineSeparator();
			textData += pacjent.getUlica().getMiasto().getWojewodztwo().getNazwa();
	
			this.taDanePacjenta.setText(textData);		
		}

	}
	
	/**
	 * Aktualizacja tabeli
	 */
	public void updateTable() {
		((RozpoznaniaTableModel)table.getModel()).setModelData(repository.getRozpoznania());
	}
	
	/**
	 * Model danych tabeli
	 */
	public class RozpoznaniaTableModel extends AbstractTableModel {
		 
		private static final long serialVersionUID = 1L;
		
		private ArrayList<RozpoznanieEnt> data = null; // lista danych
		private final Object[] columnNames = {"Termin", "Rozpoznanie", "Lekarz", "Opis",}; // nazwy w Header
	     
		// idx dla identyfikacji kolumn pod numerze
	    private final static int TERMIN_IDX = 0;
	    private final static int ROZPOZNANIE_IDX = 1;
	    private final static int LEKARZ_IDX = 2;
	    private final static int OPIS_IDX = 3;
	 
	    public RozpoznaniaTableModel() {}
	     
	    @Override
	    public int getRowCount() {
	        if(data==null) return 0;
	        return data.size();
	    }
	 
	    @Override
	    public int getColumnCount() {
	        return columnNames.length;
	    }
	    
	    @Override // wyciąganie danych z listy
	    public Object getValueAt(int rowIndex, int columnIndex) {
	    	// TODO do poprawki później
	    	
	        if(data == null) return null;
	        RozpoznanieEnt rozpoznanie = data.get(rowIndex);
	        switch (columnIndex) {
	        
	            case TERMIN_IDX:	        		
	            	Calendar cal = Calendar.getInstance();
	        		cal.setTime(rozpoznanie.getWizyta().getTermin());
	        		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");	        		
	                return format1.format(cal.getTime()); 
	            case ROZPOZNANIE_IDX:
	            	return rozpoznanie.getChoroba().getNazwa() + (rozpoznanie.getChoroba().isPrzewlekla()?" Przewlekła":"");
	            case LEKARZ_IDX:
	            	return rozpoznanie.getWizyta().getPracownik().getNazwisko() + " " + rozpoznanie.getWizyta().getPracownik().getImie();
	            case OPIS_IDX:
	            	return rozpoznanie.getOpis();
	            default:
	                return "";
	        }
	    }
	    
	 
	    @Override
	    public String getColumnName(int column) {
	        return columnNames[column].toString();
	    }
	 
	    @Override
	    public boolean isCellEditable(int row, int column) {
	        return false;
	    }	     
	    // ustawienie przekazanych danych 
	    public void setModelData(ArrayList<RozpoznanieEnt> rozpoznanie) {
	    	//logger.trace(rozpoznanie.size());
	    	this.data =  rozpoznanie;
	    	this.fireTableDataChanged();
	    }
	    
	    public RozpoznanieEnt getDataRecord(int position) {
	        return this.data.get(position);
	    }
	}

}