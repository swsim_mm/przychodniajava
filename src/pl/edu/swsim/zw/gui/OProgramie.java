package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

/**
 * Okno z informacjami o programie
 *
 */
public class OProgramie extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 */
	public OProgramie() {
		setResizable(false);
		setFont(new Font("Tahoma", Font.PLAIN, 13));
		OProgramie that = this;
		setTitle("O programie Przychodnia ŻW");
		setModal(true);
		setBounds(100, 100, 702, 386);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.WEST);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						that.setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		{
			JPanel panel = new JPanel();
			getContentPane().add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				JLabel lblPrzychodniaw = new JLabel("Przychodnia Żak - Witkowicz");
				lblPrzychodniaw.setFont(new Font("Tahoma", Font.PLAIN, 15));
				GridBagConstraints gbc_lblPrzychodniaw = new GridBagConstraints();
				gbc_lblPrzychodniaw.anchor = GridBagConstraints.WEST;
				gbc_lblPrzychodniaw.gridwidth = 2;
				gbc_lblPrzychodniaw.insets = new Insets(0, 0, 5, 0);
				gbc_lblPrzychodniaw.gridx = 0;
				gbc_lblPrzychodniaw.gridy = 0;
				panel.add(lblPrzychodniaw, gbc_lblPrzychodniaw);
			}
			{
				Component verticalStrut = Box.createVerticalStrut(20);
				GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
				gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
				gbc_verticalStrut.gridx = 0;
				gbc_verticalStrut.gridy = 1;
				panel.add(verticalStrut, gbc_verticalStrut);
			}
			{
				JLabel lblAutorzy = new JLabel("Autorzy:");
				lblAutorzy.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblAutorzy = new GridBagConstraints();
				gbc_lblAutorzy.anchor = GridBagConstraints.NORTHEAST;
				gbc_lblAutorzy.insets = new Insets(0, 0, 5, 5);
				gbc_lblAutorzy.gridx = 0;
				gbc_lblAutorzy.gridy = 2;
				panel.add(lblAutorzy, gbc_lblAutorzy);
			}
			{
				JTextArea txtrMichaakOraz = new JTextArea();
				txtrMichaakOraz.setEditable(false);
				txtrMichaakOraz.setBackground(SystemColor.menu);
				txtrMichaakOraz.setFont(new Font("Tahoma", Font.PLAIN, 13));
				txtrMichaakOraz.setLineWrap(true);
				txtrMichaakOraz.setWrapStyleWord(true);
				txtrMichaakOraz.setText("Michał Żak oraz Mariusz Witkowicz");
				GridBagConstraints gbc_txtrMichaakOraz = new GridBagConstraints();
				gbc_txtrMichaakOraz.insets = new Insets(0, 0, 5, 0);
				gbc_txtrMichaakOraz.fill = GridBagConstraints.BOTH;
				gbc_txtrMichaakOraz.gridx = 1;
				gbc_txtrMichaakOraz.gridy = 2;
				panel.add(txtrMichaakOraz, gbc_txtrMichaakOraz);
			}
			{
				Component verticalStrut = Box.createVerticalStrut(20);
				GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
				gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
				gbc_verticalStrut.gridx = 0;
				gbc_verticalStrut.gridy = 3;
				panel.add(verticalStrut, gbc_verticalStrut);
			}
			{
				JLabel lblCelProgramu = new JLabel("Cel programu:");
				lblCelProgramu.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblCelProgramu = new GridBagConstraints();
				gbc_lblCelProgramu.anchor = GridBagConstraints.NORTHEAST;
				gbc_lblCelProgramu.insets = new Insets(0, 0, 5, 5);
				gbc_lblCelProgramu.gridx = 0;
				gbc_lblCelProgramu.gridy = 4;
				panel.add(lblCelProgramu, gbc_lblCelProgramu);
			}
			{
				JTextArea txtrAplikacjaBazodanowaWykorzystujca = new JTextArea();
				txtrAplikacjaBazodanowaWykorzystujca.setEditable(false);
				txtrAplikacjaBazodanowaWykorzystujca.setBackground(SystemColor.menu);
				txtrAplikacjaBazodanowaWykorzystujca.setLineWrap(true);
				txtrAplikacjaBazodanowaWykorzystujca.setWrapStyleWord(true);
				txtrAplikacjaBazodanowaWykorzystujca.setFont(new Font("Tahoma", Font.PLAIN, 13));
				txtrAplikacjaBazodanowaWykorzystujca.setText("Aplikacja bazodanowa wykorzystująca Database ORM. \r\nPrzychodnia (pacjenci, lekarze, wizyty, choroby)");
				GridBagConstraints gbc_txtrAplikacjaBazodanowaWykorzystujca = new GridBagConstraints();
				gbc_txtrAplikacjaBazodanowaWykorzystujca.insets = new Insets(0, 0, 5, 0);
				gbc_txtrAplikacjaBazodanowaWykorzystujca.fill = GridBagConstraints.BOTH;
				gbc_txtrAplikacjaBazodanowaWykorzystujca.gridx = 1;
				gbc_txtrAplikacjaBazodanowaWykorzystujca.gridy = 4;
				panel.add(txtrAplikacjaBazodanowaWykorzystujca, gbc_txtrAplikacjaBazodanowaWykorzystujca);
			}
			{
				JLabel lblNarzdzia = new JLabel("Narzędzia:");
				lblNarzdzia.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNarzdzia = new GridBagConstraints();
				gbc_lblNarzdzia.anchor = GridBagConstraints.NORTHEAST;
				gbc_lblNarzdzia.insets = new Insets(0, 0, 5, 5);
				gbc_lblNarzdzia.gridx = 0;
				gbc_lblNarzdzia.gridy = 5;
				panel.add(lblNarzdzia, gbc_lblNarzdzia);
			}
			{
				JTextArea txtrJavaSwingHibernate = new JTextArea();
				txtrJavaSwingHibernate.setEditable(false);
				txtrJavaSwingHibernate.setText("Java, Swing, Hibernate, Eclipse, MySQL, phpMyAdmin, Log4J2, jDatePicker, MySQL Workbench");
				txtrJavaSwingHibernate.setFont(new Font("Tahoma", Font.PLAIN, 13));
				txtrJavaSwingHibernate.setBackground(SystemColor.menu);
				txtrJavaSwingHibernate.setWrapStyleWord(true);
				txtrJavaSwingHibernate.setLineWrap(true);
				GridBagConstraints gbc_txtrJavaSwingHibernate = new GridBagConstraints();
				gbc_txtrJavaSwingHibernate.insets = new Insets(0, 0, 5, 0);
				gbc_txtrJavaSwingHibernate.fill = GridBagConstraints.BOTH;
				gbc_txtrJavaSwingHibernate.gridx = 1;
				gbc_txtrJavaSwingHibernate.gridy = 5;
				panel.add(txtrJavaSwingHibernate, gbc_txtrJavaSwingHibernate);
			}
			{
				Component verticalStrut = Box.createVerticalStrut(20);
				GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
				gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
				gbc_verticalStrut.gridx = 0;
				gbc_verticalStrut.gridy = 6;
				panel.add(verticalStrut, gbc_verticalStrut);
			}
			{
				JLabel lblPrzedmiot = new JLabel("Przedmiot:");
				lblPrzedmiot.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblPrzedmiot = new GridBagConstraints();
				gbc_lblPrzedmiot.anchor = GridBagConstraints.NORTHEAST;
				gbc_lblPrzedmiot.insets = new Insets(0, 0, 5, 5);
				gbc_lblPrzedmiot.gridx = 0;
				gbc_lblPrzedmiot.gridy = 7;
				panel.add(lblPrzedmiot, gbc_lblPrzedmiot);
			}
			{
				JTextArea txtrProjektAplikacjiBazodanowej = new JTextArea();
				txtrProjektAplikacjiBazodanowej.setEditable(false);
				txtrProjektAplikacjiBazodanowej.setText("Projekt Aplikacji Bazodanowej");
				txtrProjektAplikacjiBazodanowej.setLineWrap(true);
				txtrProjektAplikacjiBazodanowej.setWrapStyleWord(true);
				txtrProjektAplikacjiBazodanowej.setFont(new Font("Tahoma", Font.PLAIN, 13));
				txtrProjektAplikacjiBazodanowej.setBackground(SystemColor.menu);
				GridBagConstraints gbc_txtrProjektAplikacjiBazodanowej = new GridBagConstraints();
				gbc_txtrProjektAplikacjiBazodanowej.insets = new Insets(0, 0, 5, 0);
				gbc_txtrProjektAplikacjiBazodanowej.fill = GridBagConstraints.BOTH;
				gbc_txtrProjektAplikacjiBazodanowej.gridx = 1;
				gbc_txtrProjektAplikacjiBazodanowej.gridy = 7;
				panel.add(txtrProjektAplikacjiBazodanowej, gbc_txtrProjektAplikacjiBazodanowej);
			}
			{
				JLabel lblSemestr = new JLabel("Semestr:");
				lblSemestr.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblSemestr = new GridBagConstraints();
				gbc_lblSemestr.anchor = GridBagConstraints.NORTHEAST;
				gbc_lblSemestr.insets = new Insets(0, 0, 5, 5);
				gbc_lblSemestr.gridx = 0;
				gbc_lblSemestr.gridy = 8;
				panel.add(lblSemestr, gbc_lblSemestr);
			}
			{
				JTextArea txtrlskaWysza = new JTextArea();
				txtrlskaWysza.setEditable(false);
				txtrlskaWysza.setText("6 (2015 r.)");
				txtrlskaWysza.setToolTipText("");
				txtrlskaWysza.setFont(new Font("Tahoma", Font.PLAIN, 13));
				txtrlskaWysza.setWrapStyleWord(true);
				txtrlskaWysza.setLineWrap(true);
				txtrlskaWysza.setBackground(SystemColor.menu);
				GridBagConstraints gbc_txtrlskaWysza = new GridBagConstraints();
				gbc_txtrlskaWysza.insets = new Insets(0, 0, 5, 0);
				gbc_txtrlskaWysza.fill = GridBagConstraints.BOTH;
				gbc_txtrlskaWysza.gridx = 1;
				gbc_txtrlskaWysza.gridy = 8;
				panel.add(txtrlskaWysza, gbc_txtrlskaWysza);
			}
			{
				JLabel lblKierunek = new JLabel("Kierunek:");
				lblKierunek.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblKierunek = new GridBagConstraints();
				gbc_lblKierunek.anchor = GridBagConstraints.NORTHEAST;
				gbc_lblKierunek.insets = new Insets(0, 0, 5, 5);
				gbc_lblKierunek.gridx = 0;
				gbc_lblKierunek.gridy = 9;
				panel.add(lblKierunek, gbc_lblKierunek);
			}
			{
				JTextArea txtrInformatyka = new JTextArea();
				txtrInformatyka.setEditable(false);
				txtrInformatyka.setFont(new Font("Tahoma", Font.PLAIN, 13));
				txtrInformatyka.setBackground(SystemColor.menu);
				txtrInformatyka.setWrapStyleWord(true);
				txtrInformatyka.setLineWrap(true);
				txtrInformatyka.setText("Informatyka");
				GridBagConstraints gbc_txtrInformatyka = new GridBagConstraints();
				gbc_txtrInformatyka.insets = new Insets(0, 0, 5, 0);
				gbc_txtrInformatyka.fill = GridBagConstraints.BOTH;
				gbc_txtrInformatyka.gridx = 1;
				gbc_txtrInformatyka.gridy = 9;
				panel.add(txtrInformatyka, gbc_txtrInformatyka);
			}
			{
				JLabel lblUczelnia = new JLabel("Uczelnia:");
				lblUczelnia.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblUczelnia = new GridBagConstraints();
				gbc_lblUczelnia.anchor = GridBagConstraints.NORTHEAST;
				gbc_lblUczelnia.insets = new Insets(0, 0, 0, 5);
				gbc_lblUczelnia.gridx = 0;
				gbc_lblUczelnia.gridy = 10;
				panel.add(lblUczelnia, gbc_lblUczelnia);
			}
			{
				JTextArea txtrlskaWyszaSzkoa = new JTextArea();
				txtrlskaWyszaSzkoa.setEditable(false);
				txtrlskaWyszaSzkoa.setWrapStyleWord(true);
				txtrlskaWyszaSzkoa.setLineWrap(true);
				txtrlskaWyszaSzkoa.setBackground(SystemColor.menu);
				txtrlskaWyszaSzkoa.setFont(new Font("Tahoma", Font.PLAIN, 13));
				txtrlskaWyszaSzkoa.setText("Śląska Wyższa Szkoła Informatyczno - Medyczna");
				GridBagConstraints gbc_txtrlskaWyszaSzkoa = new GridBagConstraints();
				gbc_txtrlskaWyszaSzkoa.fill = GridBagConstraints.BOTH;
				gbc_txtrlskaWyszaSzkoa.gridx = 1;
				gbc_txtrlskaWyszaSzkoa.gridy = 10;
				panel.add(txtrlskaWyszaSzkoa, gbc_txtrlskaWyszaSzkoa);
			}
		}
		contentPanel.setLayout(new BorderLayout(0, 0));
		
		JLabel picLabel = new JLabel(new ImageIcon(OProgramie.class.getResource("/pl/edu/swsim/zw/image/eskulap.png")));
		contentPanel.add(picLabel);
		picLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		this.setLocationRelativeTo(null);
	}

}
