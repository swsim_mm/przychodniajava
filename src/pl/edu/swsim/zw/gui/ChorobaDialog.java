package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.zw.entities.ChorobaEnt;

/**
 * Okno dodawania, edycji oraz podglądu choroby
 */
public class ChorobaDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	
	public ChorobaEnt choroba = null;
	public boolean result = false;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField tfNazwa;
	private JTextArea taOpis;
	private JCheckBox chckbxPrzewlekla;
	private JButton okButton;
	private JButton cancelButton;
	

	/**
	 * Create the dialog.
	 */
	public ChorobaDialog (Frame parent) {
		super(parent);
		setTitle("Choroba");
		setModal(true);
		setResizable(false);
		
		setBounds(100, 100, 440, 200);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.NORTH);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.SOUTH);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.WEST);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.EAST);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 8, 0};
			gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				JLabel lblNazwa = new JLabel("Nazwa");
				lblNazwa.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNazwa = new GridBagConstraints();
				gbc_lblNazwa.anchor = GridBagConstraints.EAST;
				gbc_lblNazwa.insets = new Insets(0, 0, 5, 5);
				gbc_lblNazwa.gridx = 0;
				gbc_lblNazwa.gridy = 0;
				panel.add(lblNazwa, gbc_lblNazwa);
			}
			{
				tfNazwa = new JTextField();
				tfNazwa.setFont(new Font("Tahoma", Font.PLAIN, 13));
				tfNazwa.setColumns(10);
				GridBagConstraints gbc_tfNazwa = new GridBagConstraints();
				gbc_tfNazwa.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfNazwa.insets = new Insets(0, 0, 5, 0);
				gbc_tfNazwa.gridx = 1;
				gbc_tfNazwa.gridy = 0;
				panel.add(tfNazwa, gbc_tfNazwa);
			}
			{
				JLabel lblOpis = new JLabel("Opis");
				lblOpis.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblOpis = new GridBagConstraints();
				gbc_lblOpis.anchor = GridBagConstraints.NORTHEAST;
				gbc_lblOpis.insets = new Insets(0, 0, 5, 5);
				gbc_lblOpis.gridx = 0;
				gbc_lblOpis.gridy = 1;
				panel.add(lblOpis, gbc_lblOpis);
			}
			{
				taOpis = new JTextArea();
				taOpis.setFont(new Font("Tahoma", Font.PLAIN, 13));
				taOpis.setBorder(UIManager.getBorder("TextField.border"));
				taOpis.setLineWrap(true);
				taOpis.setWrapStyleWord(true);
				GridBagConstraints gbc_taOpis = new GridBagConstraints();
				gbc_taOpis.insets = new Insets(0, 0, 5, 0);
				gbc_taOpis.fill = GridBagConstraints.BOTH;
				gbc_taOpis.gridx = 1;
				gbc_taOpis.gridy = 1;
				panel.add(taOpis, gbc_taOpis);
			}
			{
				JLabel lblPrzewlekla = new JLabel("Przewlekła");
				lblPrzewlekla.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblPrzewlekla = new GridBagConstraints();
				gbc_lblPrzewlekla.fill = GridBagConstraints.VERTICAL;
				gbc_lblPrzewlekla.anchor = GridBagConstraints.EAST;
				gbc_lblPrzewlekla.insets = new Insets(0, 0, 0, 5);
				gbc_lblPrzewlekla.gridx = 0;
				gbc_lblPrzewlekla.gridy = 2;
				panel.add(lblPrzewlekla, gbc_lblPrzewlekla);
			}
			{
				chckbxPrzewlekla = new JCheckBox("Tak");
				GridBagConstraints gbc_chckbxPrzewlekla = new GridBagConstraints();
				gbc_chckbxPrzewlekla.fill = GridBagConstraints.VERTICAL;
				gbc_chckbxPrzewlekla.anchor = GridBagConstraints.WEST;
				gbc_chckbxPrzewlekla.gridx = 1;
				gbc_chckbxPrzewlekla.gridy = 2;
				panel.add(chckbxPrzewlekla, gbc_chckbxPrzewlekla);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okAction();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelAction();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		this.setLocationRelativeTo(null); 
			
	}

	/**
	 * Przygotowanie danych na formie
	 * @param chorobaInit
	 */
	public void ReadyData(ChorobaEnt chorobaInit) {
		logger.trace(".");
		choroba = new ChorobaEnt(chorobaInit); // uwolnieie od instancji sesji Hibernate
				
		this.tfNazwa.setText(choroba.getNazwa());
		this.taOpis.setText(choroba.getOpis());
		this.chckbxPrzewlekla.setSelected(choroba.isPrzewlekla());
	}
	
	
	/**
	 * Pokazanie okna w formie modal 
	 * @see DisplayDialog()
	 */
	private void setDialogModal() {
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
		//this.setModal(true); --> konstruktor
		this.setVisible(true);
	}
	
	/**
	 * Wyświetlenie dialogu bez inicjowania zawartością blank
	 */	
	public void DisplayDialog() {
		this.choroba = new ChorobaEnt();
		setTitle("Nowa choroba");
		this.setDialogModal();
	}
	
	/**
	 * Dialog z startową zawartością
	 * @param chorobaInit
	 */
	public void DisplayDialog(ChorobaEnt chorobaInit) {
		ReadyData(chorobaInit);
		setTitle(chorobaInit.getNazwa() + " (Choroba)");
		this.setDialogModal();
	}
		
	/**
	 * Po zatwierdzeniu przyciskiem ok
	 */
	public void okAction() {
		result = true;
		Frame parent = (Frame)SwingUtilities.getWindowAncestor(this);
		
		if(tfNazwa.getText().length()<1) {
			// okno uzupelnienie danych
			JOptionPane.showMessageDialog(parent,
				    "Nazwa musi zostać podana!",
				    "Brak zawartości pola nazwa",
				    JOptionPane.ERROR_MESSAGE);	
			logger.info("brak zawartości pól (nazwa)");
			result = false;
			return;
		}
	
		if(result) {
			this.choroba.setNazwa(this.tfNazwa.getText());
			this.choroba.setOpis(this.taOpis.getText());
			this.choroba.setPrzewlekla(this.chckbxPrzewlekla.isSelected());

			setVisible(false);
		}
	}
		
	/**
	 * Po rezygrancji przyciskiem anuluj
	 */
	public void cancelAction() {
		result = false;
		setVisible(false);
	}
}
