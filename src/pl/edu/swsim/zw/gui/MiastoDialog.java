package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.LokalizacjeRep;
import pl.edu.swsim.zw.entities.MiastoEnt;
import pl.edu.swsim.zw.entities.WojewodztwoEnt;
import java.awt.Font;

/**
 * Modyfikacja/dodawanie rekordu Miasta
 *
 */
public class MiastoDialog extends JDialog {
	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();
	
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private ArrayList<WojewodztwoEnt> wojewodztwa = null;
	public MiastoEnt miasto = null;
	public boolean result = false;
	
	
	private JTextField textFieldMiasto;
	private JComboBox<String> comboBoxWojewodztwo;
	private JTextField textFieldKodPocztowy;
	private JPanel panel;

	/**
	 * Create the dialog.
	 */
	public MiastoDialog(Frame parent, MiastoEnt miastoInit) {
		super(parent);
		setTitle("Dodaj nowe Miasto");
		setBounds(100, 100, 450, 173);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.WEST);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.EAST);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.NORTH);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.SOUTH);
		}
		{
			panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				JLabel lblNewLabel_1 = new JLabel("Wojewodztwo");
				lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
				gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_1.gridx = 0;
				gbc_lblNewLabel_1.gridy = 0;
				panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
			}
			comboBoxWojewodztwo = new JComboBox<String>();
			comboBoxWojewodztwo.setFont(new Font("Tahoma", Font.PLAIN, 13));
			GridBagConstraints gbc_comboBoxWojewodztwo = new GridBagConstraints();
			gbc_comboBoxWojewodztwo.insets = new Insets(0, 0, 5, 0);
			gbc_comboBoxWojewodztwo.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBoxWojewodztwo.gridx = 1;
			gbc_comboBoxWojewodztwo.gridy = 0;
			panel.add(comboBoxWojewodztwo, gbc_comboBoxWojewodztwo);
			{
				JLabel lblNewLabel = new JLabel("Nazwa Miasta");
				lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
				gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel.gridx = 0;
				gbc_lblNewLabel.gridy = 1;
				panel.add(lblNewLabel, gbc_lblNewLabel);
			}
			{
				textFieldMiasto = new JTextField();
				textFieldMiasto.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_textFieldMiasto = new GridBagConstraints();
				gbc_textFieldMiasto.insets = new Insets(0, 0, 5, 0);
				gbc_textFieldMiasto.fill = GridBagConstraints.HORIZONTAL;
				gbc_textFieldMiasto.gridx = 1;
				gbc_textFieldMiasto.gridy = 1;
				panel.add(textFieldMiasto, gbc_textFieldMiasto);
				textFieldMiasto.setColumns(10);
			}
			{
				JLabel lblKodPocztowy = new JLabel("Kod pocztowy");
				lblKodPocztowy.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblKodPocztowy = new GridBagConstraints();
				gbc_lblKodPocztowy.anchor = GridBagConstraints.EAST;
				gbc_lblKodPocztowy.insets = new Insets(0, 0, 0, 5);
				gbc_lblKodPocztowy.gridx = 0;
				gbc_lblKodPocztowy.gridy = 2;
				panel.add(lblKodPocztowy, gbc_lblKodPocztowy);
			}
			{
				textFieldKodPocztowy = new JTextField();
				textFieldKodPocztowy.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_textFieldKodPocztowy = new GridBagConstraints();
				gbc_textFieldKodPocztowy.fill = GridBagConstraints.HORIZONTAL;
				gbc_textFieldKodPocztowy.gridx = 1;
				gbc_textFieldKodPocztowy.gridy = 2;
				panel.add(textFieldKodPocztowy, gbc_textFieldKodPocztowy);
				textFieldKodPocztowy.setColumns(10);
			}

		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okAction();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						cancelAction();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}

		ReadyData(miastoInit);
		
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
		this.setModal(true);
		this.setVisible(true);

		
	}
	
	/**
	 * Przygotowanie okna z danymi
	 * @param miastoInit
	 */
	public void ReadyData(MiastoEnt miastoInit) {
		ArrayList<Order> order = new ArrayList<Order>();
		Order.asc("id");
		
		wojewodztwa = new LokalizacjeRep().setOrderBy(order).getWojewodztwa();
		for(WojewodztwoEnt woj: wojewodztwa) {
			comboBoxWojewodztwo.addItem(woj.getNazwa());
		}
		
		
		if(miastoInit == null)
			this.miasto = new MiastoEnt();
		else {
			miasto = miastoInit;
			this.textFieldMiasto.setText(this.miasto.getNazwa());
			this.textFieldKodPocztowy.setText(this.miasto.getKod_pocztowy());

			
			for(int i = 0; i< wojewodztwa.size(); i++) {
				
				if(wojewodztwa.get(i).getId() == miastoInit.getWojewodztwo().getId()) {
					comboBoxWojewodztwo.setSelectedIndex(i);
					break;
				}	
			}
		}
	}
	
	/**
	 * Zatwierdzenie informacji
	 */
	public void okAction() {
		result = true;
		Frame parent = (Frame)SwingUtilities.getWindowAncestor(this);
		
		if(textFieldMiasto.getText().length()<1 
				|| textFieldKodPocztowy.getText().length()<1) {
					 
			// okno uzupelnienie danych
			JOptionPane.showMessageDialog(parent,
				    "Wszystkie pola muszą zostać uzupełnione!",
				    "Brak zawartości pól",
				    JOptionPane.ERROR_MESSAGE);	
			logger.info("brak zawartości pól");
			result = false;
			return;
		} 
		
		String nazwa = this.textFieldMiasto.getText();
		String kod = this.textFieldKodPocztowy.getText();
		
		// walidacje za pomocą preg math
		if (Pattern.matches("^([A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\\s\\-])+$", nazwa) == false) {
			JOptionPane.showMessageDialog(parent,
				    "Format nazwy miasta jest niepoprawny!",
				    "Zła nazwa miasta",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("zła nazwa");
		}
		if (Pattern.matches("^([0-9][0-9]\\-[0-9][0-9][0-9])$", kod) == false) {
			JOptionPane.showMessageDialog(parent,
				    "Format kodu pocztowego jest niepoprawny!\nPrawidłowy format 00-000 do 99-999",
				    "Format kodu pocztowego",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("zły format kodu");
		}
		
		
		if(result) {
			miasto.setNazwa(textFieldMiasto.getText());
			miasto.setKod_pocztowy(textFieldKodPocztowy.getText());
			miasto.setWojewodztwo(wojewodztwa.get(comboBoxWojewodztwo.getSelectedIndex()));
			
			setVisible(false);
		}
	}
	
	// anulowanie akcji
	public void cancelAction() {
		result = false;
		setVisible(false);
	}
}
