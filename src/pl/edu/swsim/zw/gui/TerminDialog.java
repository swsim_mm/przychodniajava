package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import pl.edu.swsim.zw.dao.PracownicyRep;
import pl.edu.swsim.zw.dao.RodzajeWizytRep;
import pl.edu.swsim.zw.dao.RodzajeZabiegowRep;
import pl.edu.swsim.zw.dao.TerminyRep;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.PracownikEnt;
import pl.edu.swsim.zw.entities.RodzajWizytyEnt;
import pl.edu.swsim.zw.entities.RodzajZabieguEnt;
import pl.edu.swsim.zw.entities.TerminEnt;

/**
 * Okno wybieranie/ustalania terminu
 * @author Michał
 */
public class TerminDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	public TerminEnt termin = null;

	private ArrayList<RodzajWizytyEnt> rodzajeWizyt = null;
	private ArrayList<RodzajZabieguEnt> rodzajeZabiegow = null;
	private ArrayList<PracownikEnt> pracownicy = null;
	private ArrayList<WolneTerminy> wolneTerminy = new ArrayList<WolneTerminy>();


	private boolean dialogLocked = false;
	public boolean result = false;

	private final JPanel contentPanel = new JPanel();
	private JButton btnOK;
	private JButton btnCancel;
	private JDatePanelImpl panelData;
	private JSpinner spMinuty;
	private JSpinner spGodzina;
	private JComboBox<String> cbRodzajeTerminu;
	private JCheckBox chckbxObecnosc;
	private JCheckBox chckbxPacjentZNFZ;
	private JTextArea taDanePacjenta;
	private JComboBox<String> cbLekarze;
	private JButton btnWybierzPacjenta;
	private JTextArea taDodatkowe;
	private JRadioButton rdbtnWizyta;
	private JRadioButton rdbtnZabieg;
	private JList<String> listWolneTerminy;
	private JLabel lblWolneTerminy;

	/**
	 * Create the dialog.
	 */
	public TerminDialog(Frame parent) {
		super(parent);
		setModal(true);

		setTitle("Termin");
		setBounds(100, 100, 630, 529);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.WEST);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.EAST);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.NORTH);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.SOUTH);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[] { 0, 0, 190, 0 };
			gbl_panel.rowHeights = new int[] { 0, 0 };
			gbl_panel.columnWeights = new double[] { 1.0, 0.0, 0.0,
					Double.MIN_VALUE };
			gbl_panel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
			panel.setLayout(gbl_panel);
			{
				JPanel panel_separator = new JPanel();
				panel_separator.setPreferredSize(new Dimension(2, 10));
				panel_separator.setMinimumSize(new Dimension(2, 10));
				panel_separator.setBackground(UIManager
						.getColor("Button.shadow"));
				GridBagConstraints gbc_panel_separator = new GridBagConstraints();
				gbc_panel_separator.insets = new Insets(0, 0, 0, 5);
				gbc_panel_separator.fill = GridBagConstraints.BOTH;
				gbc_panel_separator.gridx = 1;
				gbc_panel_separator.gridy = 0;
				panel.add(panel_separator, gbc_panel_separator);
				{
					Component horizontalStrut = Box.createHorizontalStrut(1);
					panel_separator.add(horizontalStrut);
				}
			}
			{
				JPanel panel_1 = new JPanel();
				GridBagConstraints gbc_panel_1 = new GridBagConstraints();
				gbc_panel_1.insets = new Insets(0, 0, 0, 5);
				gbc_panel_1.fill = GridBagConstraints.BOTH;
				gbc_panel_1.gridx = 0;
				gbc_panel_1.gridy = 0;
				panel.add(panel_1, gbc_panel_1);
				GridBagLayout gbl_panel_1 = new GridBagLayout();
				gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 0, 0 };
				gbl_panel_1.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 80, 0, 0, 0, 0 };
				gbl_panel_1.columnWeights = new double[] { 0.0, 1.0, 1.0, 0.0,
						Double.MIN_VALUE };
				gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
						0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
				panel_1.setLayout(gbl_panel_1);
				{
					JLabel lblDanePersonalne = new JLabel("Rodzaj Terminu");
					GridBagConstraints gbc_lblDanePersonalne = new GridBagConstraints();
					gbc_lblDanePersonalne.anchor = GridBagConstraints.WEST;
					gbc_lblDanePersonalne.insets = new Insets(0, 0, 5, 5);
					gbc_lblDanePersonalne.gridx = 0;
					gbc_lblDanePersonalne.gridy = 0;
					panel_1.add(lblDanePersonalne, gbc_lblDanePersonalne);
					lblDanePersonalne
							.setFont(new Font("Tahoma", Font.PLAIN, 15));
				}
				{
					JPanel panel_2 = new JPanel();
					GridBagConstraints gbc_panel_2 = new GridBagConstraints();
					gbc_panel_2.gridwidth = 4;
					gbc_panel_2.insets = new Insets(0, 0, 5, 0);
					gbc_panel_2.fill = GridBagConstraints.BOTH;
					gbc_panel_2.gridx = 0;
					gbc_panel_2.gridy = 1;
					panel_1.add(panel_2, gbc_panel_2);
					GridBagLayout gbl_panel_2 = new GridBagLayout();
					gbl_panel_2.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
					gbl_panel_2.rowHeights = new int[]{0, 0};
					gbl_panel_2.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
					gbl_panel_2.rowWeights = new double[]{0.0, Double.MIN_VALUE};
					panel_2.setLayout(gbl_panel_2);
					{
						rdbtnWizyta = new JRadioButton("Wizyta");
						rdbtnWizyta.setFont(new Font("Tahoma", Font.PLAIN, 13));
						rdbtnWizyta.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								rdbtnWizyta.setSelected(true);
								rdbtnZabieg.setSelected(false);
								termin.setZabieg(false);
								presetRodzajTerminu();
								updateAvailableTerminy();
							}
						});
						rdbtnWizyta.setSelected(true);
						GridBagConstraints gbc_rdbtnWizyta = new GridBagConstraints();
						gbc_rdbtnWizyta.insets = new Insets(0, 0, 0, 5);
						gbc_rdbtnWizyta.gridx = 1;
						gbc_rdbtnWizyta.gridy = 0;
						panel_2.add(rdbtnWizyta, gbc_rdbtnWizyta);
					}
					{
						rdbtnZabieg = new JRadioButton("Zabieg");
						rdbtnZabieg.setFont(new Font("Tahoma", Font.PLAIN, 13));
						rdbtnZabieg.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								rdbtnWizyta.setSelected(false);
								rdbtnZabieg.setSelected(true);
								termin.setZabieg(true);
								presetRodzajTerminu();
								updateAvailableTerminy();
							}
						});
						GridBagConstraints gbc_rdbtnZabieg = new GridBagConstraints();
						gbc_rdbtnZabieg.insets = new Insets(0, 0, 0, 5);
						gbc_rdbtnZabieg.gridx = 3;
						gbc_rdbtnZabieg.gridy = 0;
						panel_2.add(rdbtnZabieg, gbc_rdbtnZabieg);
					}
				}
				{
					cbRodzajeTerminu = new JComboBox<String>();
					cbRodzajeTerminu.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							updateAvailableTerminy();
						}
					});
					cbRodzajeTerminu.setFont(new Font("Tahoma", Font.PLAIN, 13));
					GridBagConstraints gbc_cbRodzajeTerminu = new GridBagConstraints();
					gbc_cbRodzajeTerminu.fill = GridBagConstraints.HORIZONTAL;
					gbc_cbRodzajeTerminu.gridwidth = 4;
					gbc_cbRodzajeTerminu.insets = new Insets(0, 0, 5, 0);
					gbc_cbRodzajeTerminu.gridx = 0;
					gbc_cbRodzajeTerminu.gridy = 2;
					panel_1.add(cbRodzajeTerminu, gbc_cbRodzajeTerminu);
				}
				{
					JLabel lblLekarz = new JLabel("Lekarz");
					GridBagConstraints gbc_lblLekarz = new GridBagConstraints();
					gbc_lblLekarz.anchor = GridBagConstraints.WEST;
					gbc_lblLekarz.insets = new Insets(0, 0, 5, 5);
					gbc_lblLekarz.gridx = 0;
					gbc_lblLekarz.gridy = 3;
					panel_1.add(lblLekarz, gbc_lblLekarz);
					lblLekarz.setFont(new Font("Tahoma", Font.PLAIN, 15));
				}
				{
					cbLekarze = new JComboBox<String>();
					cbLekarze.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							updateAvailableTerminy();
						}
					});
					cbLekarze.setFont(new Font("Tahoma", Font.PLAIN, 13));
					GridBagConstraints gbc_cbLekarze = new GridBagConstraints();
					gbc_cbLekarze.gridwidth = 4;
					gbc_cbLekarze.insets = new Insets(0, 0, 5, 0);
					gbc_cbLekarze.fill = GridBagConstraints.HORIZONTAL;
					gbc_cbLekarze.gridx = 0;
					gbc_cbLekarze.gridy = 4;
					panel_1.add(cbLekarze, gbc_cbLekarze);
				}
				{
					JLabel lblPacjent = new JLabel("Pacjent");
					GridBagConstraints gbc_lblPacjent = new GridBagConstraints();
					gbc_lblPacjent.anchor = GridBagConstraints.WEST;
					gbc_lblPacjent.insets = new Insets(0, 0, 5, 5);
					gbc_lblPacjent.gridx = 0;
					gbc_lblPacjent.gridy = 5;
					panel_1.add(lblPacjent, gbc_lblPacjent);
					lblPacjent.setFont(new Font("Tahoma", Font.PLAIN, 15));
				}
				{
					btnWybierzPacjenta = new JButton("Wybierz");
					btnWybierzPacjenta.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							selectPacjentAction();
						}
					});
					GridBagConstraints gbc_btnWybierzPacjenta = new GridBagConstraints();
					gbc_btnWybierzPacjenta.anchor = GridBagConstraints.EAST;
					gbc_btnWybierzPacjenta.insets = new Insets(0, 0, 5, 5);
					gbc_btnWybierzPacjenta.gridx = 2;
					gbc_btnWybierzPacjenta.gridy = 5;
					panel_1.add(btnWybierzPacjenta, gbc_btnWybierzPacjenta);
				}
				{
					taDanePacjenta = new JTextArea();
					taDanePacjenta.setEditable(false);
					taDanePacjenta.setBorder(UIManager
							.getBorder("TextField.border"));
					taDanePacjenta.setLineWrap(true);
					taDanePacjenta.setWrapStyleWord(true);
					taDanePacjenta.setFont(new Font("Tahoma", Font.PLAIN, 13));
					taDanePacjenta.setRows(4);
					GridBagConstraints gbc_taDanePacjenta = new GridBagConstraints();
					gbc_taDanePacjenta.insets = new Insets(0, 0, 5, 0);
					gbc_taDanePacjenta.gridwidth = 4;
					gbc_taDanePacjenta.fill = GridBagConstraints.BOTH;
					gbc_taDanePacjenta.gridx = 0;
					gbc_taDanePacjenta.gridy = 6;
					panel_1.add(taDanePacjenta, gbc_taDanePacjenta);
				}
				{
					chckbxPacjentZNFZ = new JCheckBox(
							"refundacja nfz");
					chckbxPacjentZNFZ.setFont(new Font("Tahoma", Font.PLAIN, 13));
					chckbxPacjentZNFZ.setSelected(true);
					GridBagConstraints gbc_chckbxPacjentZNFZ = new GridBagConstraints();
					gbc_chckbxPacjentZNFZ.gridwidth = 2;
					gbc_chckbxPacjentZNFZ.insets = new Insets(0, 0, 5, 5);
					gbc_chckbxPacjentZNFZ.gridx = 0;
					gbc_chckbxPacjentZNFZ.gridy = 7;
					panel_1.add(chckbxPacjentZNFZ, gbc_chckbxPacjentZNFZ);
				}
				{
					chckbxObecnosc = new JCheckBox("Obecny na wizycie");
					chckbxObecnosc.setFont(new Font("Tahoma", Font.PLAIN, 13));
					GridBagConstraints gbc_chckbxObecnosc = new GridBagConstraints();
					gbc_chckbxObecnosc.gridwidth = 2;
					gbc_chckbxObecnosc.insets = new Insets(0, 0, 5, 0);
					gbc_chckbxObecnosc.gridx = 2;
					gbc_chckbxObecnosc.gridy = 7;
					panel_1.add(chckbxObecnosc, gbc_chckbxObecnosc);
				}
				{
					JLabel lblInformacjeDodatkowe = new JLabel(
							"Informacje dodatkowe");
					lblInformacjeDodatkowe.setFont(new Font("Tahoma",
							Font.PLAIN, 15));
					GridBagConstraints gbc_lblInformacjeDodatkowe = new GridBagConstraints();
					gbc_lblInformacjeDodatkowe.gridwidth = 2;
					gbc_lblInformacjeDodatkowe.anchor = GridBagConstraints.WEST;
					gbc_lblInformacjeDodatkowe.insets = new Insets(0, 0, 5, 5);
					gbc_lblInformacjeDodatkowe.gridx = 0;
					gbc_lblInformacjeDodatkowe.gridy = 8;
					panel_1.add(lblInformacjeDodatkowe,
							gbc_lblInformacjeDodatkowe);
				}
				{
					taDodatkowe = new JTextArea();
					taDodatkowe.setFont(new Font("Tahoma", Font.PLAIN, 13));
					taDodatkowe.setBorder(UIManager
							.getBorder("TextField.border"));
					GridBagConstraints gbc_taDodatkowe = new GridBagConstraints();
					gbc_taDodatkowe.gridwidth = 4;
					gbc_taDodatkowe.fill = GridBagConstraints.BOTH;
					gbc_taDodatkowe.gridx = 0;
					gbc_taDodatkowe.gridy = 9;
					panel_1.add(taDodatkowe, gbc_taDodatkowe);
				}
			}
			{
				JPanel panel_1 = new JPanel();
				GridBagConstraints gbc_panel_1 = new GridBagConstraints();
				gbc_panel_1.fill = GridBagConstraints.BOTH;
				gbc_panel_1.gridx = 2;
				gbc_panel_1.gridy = 0;
				panel.add(panel_1, gbc_panel_1);
				GridBagLayout gbl_panel_1 = new GridBagLayout();
				gbl_panel_1.columnWidths = new int[] { 0, 0 };
				gbl_panel_1.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
				gbl_panel_1.columnWeights = new double[] { 1.0,
						Double.MIN_VALUE };
				gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
						1.0, Double.MIN_VALUE };
				panel_1.setLayout(gbl_panel_1);
				{
					JLabel lblTermin = new JLabel("Dzień");
					GridBagConstraints gbc_lblTermin = new GridBagConstraints();
					gbc_lblTermin.insets = new Insets(0, 0, 5, 0);
					gbc_lblTermin.gridx = 0;
					gbc_lblTermin.gridy = 0;
					panel_1.add(lblTermin, gbc_lblTermin);
					lblTermin.setFont(new Font("Tahoma", Font.PLAIN, 15));
				}
				{
					JPanel panel_date = new JPanel();
					GridBagConstraints gbc_panel_date = new GridBagConstraints();
					gbc_panel_date.insets = new Insets(0, 0, 5, 0);
					gbc_panel_date.fill = GridBagConstraints.BOTH;
					gbc_panel_date.gridx = 0;
					gbc_panel_date.gridy = 1;
					panel_1.add(panel_date, gbc_panel_date);

					{
						UtilDateModel model = new UtilDateModel();
						Properties p = new Properties();
						p.put("text.today", "Today");
						p.put("text.month", "Month");
						p.put("text.year", "Year");

						panelData = new JDatePanelImpl(model, p);
						panelData.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								updateAvailableTerminy();
							}
						});
						JDatePickerImpl datePicker = new JDatePickerImpl(
								panelData, new DateLabelFormatter());

						datePicker.setShowYearButtons(true);
						datePicker.getJFormattedTextField().setFont(
								new Font("Tahoma", Font.PLAIN, 15));
						panel_date.setLayout(new BorderLayout(0, 0));

						panel_date.add(panelData);
					}

				}
				{
					JLabel lblCzas = new JLabel("Czas");
					GridBagConstraints gbc_lblCzas = new GridBagConstraints();
					gbc_lblCzas.insets = new Insets(0, 0, 5, 0);
					gbc_lblCzas.gridx = 0;
					gbc_lblCzas.gridy = 2;
					panel_1.add(lblCzas, gbc_lblCzas);
					lblCzas.setFont(new Font("Tahoma", Font.PLAIN, 15));
				}
				{
					JPanel panel_time = new JPanel();
					GridBagConstraints gbc_panel_time = new GridBagConstraints();
					gbc_panel_time.insets = new Insets(0, 0, 5, 0);
					gbc_panel_time.fill = GridBagConstraints.BOTH;
					gbc_panel_time.gridx = 0;
					gbc_panel_time.gridy = 3;
					panel_1.add(panel_time, gbc_panel_time);
					GridBagLayout gbl_panel_time = new GridBagLayout();
					gbl_panel_time.columnWidths = new int[] { 0, 0, 0, 0 };
					gbl_panel_time.rowHeights = new int[] { 0, 0 };
					gbl_panel_time.columnWeights = new double[] { 1.0, 0.0,
							1.0, Double.MIN_VALUE };
					gbl_panel_time.rowWeights = new double[] { 0.0,
							Double.MIN_VALUE };
					panel_time.setLayout(gbl_panel_time);
					{
						spGodzina = new JSpinner();
						GridBagConstraints gbc_spGodzina = new GridBagConstraints();
						gbc_spGodzina.anchor = GridBagConstraints.EAST;
						gbc_spGodzina.insets = new Insets(0, 0, 0, 5);
						gbc_spGodzina.gridx = 0;
						gbc_spGodzina.gridy = 0;
						panel_time.add(spGodzina, gbc_spGodzina);
						spGodzina.setModel(new SpinnerNumberModel(0, 0, 23, 1));
						spGodzina.setFont(new Font("Tahoma", Font.PLAIN, 15));
					}
					{
						JLabel label = new JLabel(":");
						GridBagConstraints gbc_label = new GridBagConstraints();
						gbc_label.insets = new Insets(0, 0, 0, 5);
						gbc_label.gridx = 1;
						gbc_label.gridy = 0;
						panel_time.add(label, gbc_label);
						label.setFont(new Font("Tahoma", Font.PLAIN, 15));
					}
					{
						spMinuty = new JSpinner();
						GridBagConstraints gbc_spMinuty = new GridBagConstraints();
						gbc_spMinuty.anchor = GridBagConstraints.WEST;
						gbc_spMinuty.gridx = 2;
						gbc_spMinuty.gridy = 0;
						panel_time.add(spMinuty, gbc_spMinuty);
						spMinuty.setModel(new SpinnerNumberModel(0, 0, 59, 1));
						spMinuty.setFont(new Font("Tahoma", Font.PLAIN, 15));
					}
				}
				{
					lblWolneTerminy = new JLabel("Wolne terminy");
					lblWolneTerminy.setFont(new Font("Tahoma", Font.PLAIN, 15));
					GridBagConstraints gbc_lblWolneTerminy = new GridBagConstraints();
					gbc_lblWolneTerminy.insets = new Insets(0, 0, 5, 0);
					gbc_lblWolneTerminy.gridx = 0;
					gbc_lblWolneTerminy.gridy = 4;
					panel_1.add(lblWolneTerminy, gbc_lblWolneTerminy);
				}
				{
					listWolneTerminy = new JList<String>();
					listWolneTerminy.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							selectAutoCzas();
						}
					});
					listWolneTerminy.setBorder(UIManager
							.getBorder("ComboBox.border"));
					GridBagConstraints gbc_listWolneTerminy = new GridBagConstraints();
					gbc_listWolneTerminy.fill = GridBagConstraints.BOTH;
					gbc_listWolneTerminy.gridx = 0;
					gbc_listWolneTerminy.gridy = 5;
					panel_1.add(listWolneTerminy,
							gbc_listWolneTerminy);
				}

			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				btnOK = new JButton("OK");
				btnOK.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okAction();
					}
				});
				btnOK.setActionCommand("OK");
				buttonPane.add(btnOK);
				getRootPane().setDefaultButton(btnOK);
			}
			{
				btnCancel = new JButton("Anuluj");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelAction();
					}
				});
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				cancelAction();
			}
		});
		
		panelData.getModel().setSelected(true);

	}

	private void setDialogModal() {
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
		// this.setModal(true); --> konstruktor
		this.setVisible(true);

	}

	/**
	 * Reguluje formę wyświetlania okna
	 */
	public void DisplayDialog() {
		this.termin = new TerminEnt();
		presetLekarz(null);
		presetRodzajTerminu();
		
		setTitle("Nowy Termin");
		this.lockDialog(false);
		this.setDialogModal();
	}

	public void DisplayDialog(TerminEnt terminInit) {
		ReadyData(terminInit);
		setTitle("Edycja terminu  (...)");
		this.lockDialog(false);
		this.setDialogModal();
	}

	public void DisplayDialog(TerminEnt terminInit, boolean lockDialog) {
		ReadyData(terminInit);
		setTitle("Termin:  (...)");
		this.lockDialog(lockDialog);
		this.setDialogModal();
	}
	//pacjentInit.getImie() + " " + pacjentInit.getNazwisko()

	/**
	 * Przygotowuje dane okna z podanego pacjenta
	 * 
	 * @param
	 */
	public void ReadyData(TerminEnt terminInit) {
		logger.trace(".");

		//termin = new TerminEnt(terminInit);
		termin = terminInit;
		
		this.chckbxObecnosc.setSelected(termin.isObecnosc());
		this.chckbxPacjentZNFZ.setSelected(termin.isNfz());

		this.taDodatkowe.setText(termin.getDodatkowe());
		
		if(terminInit.isZabieg()) {
			rdbtnZabieg.setSelected(true);
			rdbtnWizyta.setSelected(false);
		} else {
			rdbtnZabieg.setSelected(false);
			rdbtnWizyta.setSelected(true);
		}
		
		presetRodzajTerminu();
		
		presetLekarz(termin.getPracownik());
		
		presetPacjent(termin.getPacjent());
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(termin.getTermin());
		
		this.panelData.getModel().setYear(cal.get(Calendar.YEAR));
		this.panelData.getModel().setMonth(cal.get(Calendar.MONTH));
		this.panelData.getModel().setDay(cal.get(Calendar.DAY_OF_MONTH));
		
		this.spGodzina.setValue(cal.get(Calendar.HOUR_OF_DAY));
		this.spMinuty.setValue(cal.get(Calendar.MINUTE));
		
		


	}
	
	/**
	 * Zaznacza pracownika według wartości ustalonej w ładowanym obiekcie (jeśli nie null)
	 * @param pracownik
	 */
	private void presetLekarz(PracownikEnt pracownik) {

		updateLekarze();

		if (pracownik == null)
			return;

		long pracID = pracownik.getId();

		for (int i = 0; i < pracownicy.size(); i++) {
			if (pracownicy.get(i).getId() == pracID) {
				cbLekarze.setSelectedIndex(i);
				break;
			}
		}

	}

	/**
	 * Aktualizuje listę lekarzy
	 */
	public void updateLekarze() {
		((DefaultComboBoxModel<String>) cbLekarze.getModel())
				.removeAllElements();

		ArrayList<Order> order = new ArrayList<Order>();
		Order.asc("nazwa");

		pracownicy = new PracownicyRep().setOrderBy(order)
				.setRodzajPracownika(PracownikEnt.TYP_LEKARZ).getPracownicy();
		for (PracownikEnt prac : pracownicy) {
			cbLekarze.addItem(prac.getImie() + " " + prac.getNazwisko());
		}
	}

	/**
	 * Wybiera rodzaj terminu - wizyty lub zabiegu w zależności co jest zaznaczone
	 */
	private void presetRodzajTerminu() {

		
		if(termin.isZabieg()) {
			updateRodzajeZabiegow();
			
			if(termin.getRodzajzabiegu() == null)
				return;
			
			long id = termin.getRodzajzabiegu().getId();
			
			for (int i = 0; i < rodzajeZabiegow.size(); i++) {
				if (rodzajeZabiegow.get(i).getId() == id) {
					cbRodzajeTerminu.setSelectedIndex(i);
					break;
				}
			}
			
		} else {
			updateRodzajeWizyt();
			
			if(termin.getRodzajwizyty() == null)
				return;
			
			long id = termin.getRodzajwizyty().getId();
			
			for (int i = 0; i < rodzajeWizyt.size(); i++) {
				if (rodzajeWizyt.get(i).getId() == id) {
					cbRodzajeTerminu.setSelectedIndex(i);
					break;
				}
			}
			
		}

	}

	/**
	 * Aktualizuję listę rodzajów wizyt
	 */
	public void updateRodzajeWizyt() {
		((DefaultComboBoxModel<String>) cbRodzajeTerminu.getModel())
				.removeAllElements();

		ArrayList<Order> order = new ArrayList<Order>();
		Order.asc("nazwa");

		rodzajeWizyt = new RodzajeWizytRep().setOrderBy(order)
				.getRodzajeWizyt();
		for (RodzajWizytyEnt rodz : rodzajeWizyt) {
			cbRodzajeTerminu.addItem(rodz.getNazwa() + " [gab. " 
					+ rodz.getGabinet().getNumer()+ " " + rodz.getGabinet().getNazwa()
					+ "]");
		}
	}
	
	/**
	 * Aktualizuje listę rodzajów zabiegów 
	 */
	public void updateRodzajeZabiegow() {
		((DefaultComboBoxModel<String>) cbRodzajeTerminu.getModel())
				.removeAllElements();

		ArrayList<Order> order = new ArrayList<Order>();
		Order.asc("nazwa");

		rodzajeZabiegow = new RodzajeZabiegowRep().setOrderBy(order).getRodzajeZabiegow();
		for (RodzajZabieguEnt rodz : rodzajeZabiegow) {
			cbRodzajeTerminu.addItem(rodz.getNazwa() + " [gab. " 
					+ rodz.getGabinet().getNumer()+ " " + rodz.getGabinet().getNazwa()
					+ "]");
		}
	}

	/**
	 * Przesłanie pacjenta przez okno wyboru
	 * @param pacjent
	 */
	public void presetPacjent(PacjentEnt pacjent) {
	
		if(pacjent != null) {
			
			logger.trace(pacjent.toString());
			
			String textData = "";
	
			textData += pacjent.getNazwisko() + " " + pacjent.getImie()
					+ System.lineSeparator();
			textData += pacjent.getUlica().getNazwa() + " " + pacjent.getBudynek()
					+ " " + pacjent.getMieszkanie() + System.lineSeparator();
			textData += pacjent.getUlica().getMiasto().getKod_pocztowy() + " "
					+ pacjent.getUlica().getMiasto().getNazwa()
					+ System.lineSeparator();
			textData += pacjent.getUlica().getMiasto().getWojewodztwo().getNazwa();
	
			this.taDanePacjenta.setText(textData);		
		}

	}
	
	/**
	 * Okno wyboru pacjenta 
	 */
	private void selectPacjentAction() {
		SelectPacjentDialog spd = new SelectPacjentDialog();
		spd.setVisible(true);
		
		if(spd.pacjent != null) {
			presetPacjent(spd.pacjent);
			this.termin.setPacjent(new PacjentEnt(spd.pacjent));
		}
	}
	
	/**
	 * Odświeża listę dostępnych terminów
	 */
	private void updateAvailableTerminy() {
		
		//if()
		// domyślnie Wizyta lub Zabieg jest wybrany nie trzeba sprawdzać
		// może jednak nie być nic na liście rodzajów
		
		if(cbRodzajeTerminu.getSelectedIndex() == -1)
			return;
		
		// lekarz może mieć pustą listę
		if(cbLekarze.getSelectedIndex() == -1)
			return;
		
		// panel startuje już z datą dzisiejszą
		
		//if(panelData.getModel())
		
		this.wolneTerminy.clear();
		
		TerminyRep repository = new TerminyRep();
		
		// data wybrana przez użytkownika
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, this.panelData.getModel().getYear());
		cal.set(Calendar.MONTH, this.panelData.getModel().getMonth());
		cal.set(Calendar.DAY_OF_MONTH, this.panelData.getModel().getDay());
		
		repository.setThatDate(cal.getTime());
		repository.setLekarz(this.pracownicy.get(cbLekarze.getSelectedIndex()));
		
		// mimo że przekazywany jest cały wpis typu to wyszukiwanie skupia się na gabientach
		if(this.rdbtnWizyta.isSelected())
			repository.setRodzajWizyty(this.rodzajeWizyt.get(cbRodzajeTerminu.getSelectedIndex()));
		else
			repository.setRodzajZabiegu(this.rodzajeZabiegow.get(cbRodzajeTerminu.getSelectedIndex()));
		
		// sortowanie rosnąco
		ArrayList<Order> orderList = new ArrayList<Order>();
		orderList.add(Order.asc("termin"));
		repository.setOrderBy(orderList);
		
		ArrayList<TerminEnt> terminy = repository.getTerminy();
		
		// w tym momencie posiadamy listę terminów zarejestrowanych na dany dzień
		// dla danego gabinetu, potrzeba jest odwrotna lista wolnych terminów
		
		
		java.sql.Time czasTrwaniaWybranej; // Czas trwania wybranej wizyty w aktualnym oknie

		// zdobycie czastu trwania wybranego terminu
		if(this.rdbtnWizyta.isSelected())
			czasTrwaniaWybranej = ((RodzajWizytyEnt)this.rodzajeWizyt
					.get(cbRodzajeTerminu.getSelectedIndex())).getCzastrwania();
		else
			czasTrwaniaWybranej = ((RodzajZabieguEnt)this.rodzajeZabiegow
					.get(cbRodzajeTerminu.getSelectedIndex())).getCzastrwania();
		
		// liczniki czasu 
		Calendar calRecord = Calendar.getInstance(); // operacje na danych rekordu
		Calendar calCounter = Calendar.getInstance();  // licznik przeglądania czasu w dniu
		Calendar calDiff = Calendar.getInstance();  // obliczanie róznicy
		
		// to nie jest koniecznie potrzebne
		calCounter.set(Calendar.YEAR, this.panelData.getModel().getYear());
		calCounter.set(Calendar.MONTH, this.panelData.getModel().getMonth());
		calCounter.set(Calendar.DAY_OF_MONTH, this.panelData.getModel().getDay());
		
		// odczytanie długości wybranego typu terminu (który stara się zmieścić)
		calRecord.setTime(czasTrwaniaWybranej);
		int wybranaGodzina = calRecord.get(Calendar.HOUR_OF_DAY);
		int wybranaMinuta = calRecord.get(Calendar.MINUTE);
		
		// to już tak, reset od godziny 0:00, od początku dnia
		calCounter.set(Calendar.HOUR_OF_DAY, 0);
		calCounter.set(Calendar.MINUTE, 0);
		calCounter.set(Calendar.SECOND, 0); 
		
		for(int i=0; i<terminy.size(); i++) {
			// Różnica między poprzednim rekordem a bieżącym
			calDiff.setTime( new Date(terminy.get(i).getTermin().getTime() - calCounter.getTimeInMillis()) );
			
			if( (calDiff.get(Calendar.HOUR_OF_DAY) > wybranaGodzina || calDiff.get(Calendar.MINUTE) > wybranaMinuta )
					&& terminy.get(i).getTermin().getTime() > calCounter.getTimeInMillis()
					) {
				// jeśli starczy czasu na wizytę
				WolneTerminy wt = new WolneTerminy();
				wt.terminOd = new Date(calCounter.getTime().getTime()); //od 
				wt.terminDo = new Date(terminy.get(i).getTermin().getTime()); // do	
				
				calRecord.setTime(calCounter.getTime());
				wt.text = Integer.toString(calRecord.get(Calendar.HOUR_OF_DAY)) + ":" + Integer.toString(calRecord.get(Calendar.MINUTE));
				wt.text += " - ";
				calRecord.setTime(terminy.get(i).getTermin());
				wt.text += Integer.toString(calRecord.get(Calendar.HOUR_OF_DAY)) + ":" + Integer.toString(calRecord.get(Calendar.MINUTE));
				
				wolneTerminy.add(wt);
			}
			
			if(rdbtnZabieg.isSelected()) 
				calRecord.setTime(terminy.get(i).getRodzajzabiegu().getCzastrwania());
			else 
				calRecord.setTime(terminy.get(i).getRodzajwizyty().getCzastrwania());
			
			calCounter.setTime(terminy.get(i).getTermin());
			calCounter.add(Calendar.HOUR_OF_DAY, calRecord.get(Calendar.HOUR_OF_DAY));
			calCounter.add(Calendar.MINUTE, calRecord.get(Calendar.MINUTE));

		}
					
		//ostatni wspis do końca czasu
		WolneTerminy wt = new WolneTerminy();
		wt.terminOd = new Date(calCounter.getTime().getTime()); //od 
		
		calDiff.setTime(new Date(calCounter.getTime().getTime()));
		calDiff.set(Calendar.HOUR_OF_DAY, 23);
		calDiff.set(Calendar.MINUTE, 59);
		calDiff.set(Calendar.SECOND, 59);
		
		wt.terminDo = new Date(calDiff.getTimeInMillis()); // do	
		
		calRecord.setTime(calCounter.getTime());
		wt.text = Integer.toString(calRecord.get(Calendar.HOUR_OF_DAY)) + ":" + Integer.toString(calRecord.get(Calendar.MINUTE));
		wt.text += " - +++";

		wolneTerminy.add(wt);
		
		
		
		DefaultListModel<String> model = new DefaultListModel<String>();
		
		for(int i=0; i<wolneTerminy.size(); i++) {
			model.addElement(wolneTerminy.get(i).text);
		}
		
		listWolneTerminy.setModel(model);
		
	}
	
	private void selectAutoCzas() {
		if(listWolneTerminy.getSelectedIndex() > -1) {
			Calendar selRecord = Calendar.getInstance();
			selRecord.setTime(wolneTerminy.get(listWolneTerminy.getSelectedIndex()).terminOd);
			
			spGodzina.setValue(selRecord.get(Calendar.HOUR_OF_DAY));
			spMinuty.setValue(selRecord.get(Calendar.MINUTE));
			
		}
		
	}

	/**
	 * Zatwierdzenie nowego/zmian w terminie
	 */
	public void okAction() {
		
		result = true;

		if(dialogLocked) { // dialog tylko do wyświetlenia informacji
			setVisible(false); 
			return; 
		}

		Frame parent = (Frame)SwingUtilities.getWindowAncestor(this);
		
		if ( cbLekarze.getSelectedIndex() == -1 || cbRodzajeTerminu.getSelectedIndex() == -1 ) {
			JOptionPane.showMessageDialog(parent,
				    "Pola lekarza oraz rodzaju zabiegu muszą zostać określone",
				    "Nie wszystkie dane zostały podane",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("zły lekarz lub rodzaj terminu");
			return;
		}
		
		if ( termin.getPacjent() == null ) {
			JOptionPane.showMessageDialog(parent,
				    "Musisz wskazać pacjenta",
				    "Brak informacji o pacjencie",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("Brak informacji o pacjencie");
			return;
		}

		if(result) {
			
			if(rdbtnZabieg.isSelected()) {
				termin.setZabieg(true);
				termin.setRodzajwizyty(null);
				termin.setRodzajzabiegu(rodzajeZabiegow.get(cbRodzajeTerminu.getSelectedIndex()));
				
			} else {
				termin.setZabieg(false);
				termin.setRodzajwizyty(rodzajeWizyt.get(cbRodzajeTerminu.getSelectedIndex()));
				termin.setRodzajzabiegu(null);
			}
			
			termin.setPracownik(pracownicy.get(cbLekarze.getSelectedIndex()));
			
			termin.setNfz(this.chckbxPacjentZNFZ.isSelected());
			termin.setObecnosc(this.chckbxObecnosc.isSelected());
			
			// pacjent jest ustawiony na bieżąco
			
			termin.setDodatkowe(taDodatkowe.getText());
			
			// ustawianie terminu			
			
			Calendar cal = Calendar.getInstance();
			
			cal.set(Calendar.YEAR, this.panelData.getModel().getYear());
			cal.set(Calendar.MONTH, this.panelData.getModel().getMonth());
			cal.set(Calendar.DAY_OF_MONTH, this.panelData.getModel().getDay());
			
			cal.set(Calendar.HOUR_OF_DAY, (int)this.spGodzina.getValue());
			cal.set(Calendar.MINUTE, (int)this.spMinuty.getValue());
			cal.set(Calendar.SECOND, 0);
			
			termin.setTermin(cal.getTime());
						
			setVisible(false); 
		}
		 
	}

	/**
	 * Anulowanie okna
	 */
	public void cancelAction() {
		result = false;
		setVisible(false);
	}

	/**
	 * Blokuje kotrolki edycji jeśli okno jest tylko do podglądu
	 * 
	 * @param doLock
	 */
	private void lockDialog(boolean doLock) {
		this.dialogLocked = doLock;

		boolean enableControl = !doLock;

		this.cbRodzajeTerminu.setEnabled(enableControl);
		this.cbLekarze.setEnabled(enableControl);
		this.btnWybierzPacjenta.setVisible(enableControl);
		this.chckbxPacjentZNFZ.setEnabled(enableControl);
		this.chckbxObecnosc.setEnabled(enableControl);
		this.taDodatkowe.setEditable(enableControl);

		this.panelData.setEnabled(enableControl);
		this.spGodzina.setEnabled(enableControl);
		this.spMinuty.setEnabled(enableControl);

		this.btnCancel.setEnabled(enableControl);
		this.listWolneTerminy.setVisible(enableControl);
		this.lblWolneTerminy.setVisible(enableControl);
		this.panelData.setEnabled(enableControl);
		
	}

	public class DateLabelFormatter extends AbstractFormatter {

		private static final long serialVersionUID = 1L;
		private String datePattern = "yyyy-MM-dd";
		private SimpleDateFormat dateFormatter = new SimpleDateFormat(
				datePattern);

		@Override
		public Object stringToValue(String text) throws ParseException {
			return dateFormatter.parseObject(text);
		}

		@Override
		public String valueToString(Object value) throws ParseException {
			if (value != null) {
				Calendar cal = (Calendar) value;
				return dateFormatter.format(cal.getTime());
			}

			return "";
		}

	}

	public boolean isDialogLocked() {
		return dialogLocked;
	}
	
	public class WolneTerminy {
		public String text;
		public Date terminOd;
		public Date terminDo;
	}
}
