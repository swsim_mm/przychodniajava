package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.PracownicyRep;
import pl.edu.swsim.zw.entities.PracownikEnt;
import javax.swing.ImageIcon;
import java.awt.Font;

/**
 * Okno z listą pracowników
 *
 */
public class PracownicyIntFrame extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private PracownicyRep repository = new PracownicyRep();
	//private GrupaEnt grupy = null;
	
	private final String[] sortOptions = {
			"Imię rosnąco [a-z]",
			"Imię malejąco [z-a]",
			"Nazwisko rosnąco [a-z]",
			"Nazwisko malejąco [z-a]",
			"Adres rosnąco [a-z]",
			"Adres malejąco [z-a]",
			"Czy lekarz [nie-tak]",
			"Czy lekarz [tak-nie]",
	};

	private JTextField tfSearch;
	private JTable table;
	private JComboBox<String> cbOrder;
	private JCheckBox chckbxTylkoLekarz;

	/**
	 * Create the frame.
	 */
	public PracownicyIntFrame() {

		// szybkie obejście this w klasach anonimowych, 
		// jest nadal dostęp do tej klasy przez that
		PracownicyIntFrame that = this;

		logger.info(getBorder());
		// nadają wygląd dla okna jak by było z systemu
		setTitle("Pracownicy");
		setResizable(true);
		setMaximizable(true);
		setClosable(true);
		setBounds(100, 100, 622, 386);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut_1, BorderLayout.EAST);
		
		Component verticalStrut = Box.createVerticalStrut(5);
		panel.add(verticalStrut, BorderLayout.NORTH);
		
		Component verticalStrut_1 = Box.createVerticalStrut(5);
		panel.add(verticalStrut_1, BorderLayout.SOUTH);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 125, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblSortujWedug = new JLabel("Sortuj według");
		lblSortujWedug.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSortujWedug = new GridBagConstraints();
		gbc_lblSortujWedug.gridwidth = 2;
		gbc_lblSortujWedug.insets = new Insets(0, 0, 5, 5);
		gbc_lblSortujWedug.anchor = GridBagConstraints.EAST;
		gbc_lblSortujWedug.gridx = 0;
		gbc_lblSortujWedug.gridy = 0;
		panel_1.add(lblSortujWedug, gbc_lblSortujWedug);
		
		cbOrder = new JComboBox<String>();
		cbOrder.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cbOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		cbOrder.setModel(new DefaultComboBoxModel<String>(sortOptions));
		GridBagConstraints gbc_cbGrupa = new GridBagConstraints();
		gbc_cbGrupa.insets = new Insets(0, 0, 5, 5);
		gbc_cbGrupa.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbGrupa.gridx = 2;
		gbc_cbGrupa.gridy = 0;
		panel_1.add(cbOrder, gbc_cbGrupa);
		
		JButton btnDodaj = new JButton("");
		btnDodaj.setIcon(new ImageIcon(PracownicyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/add.png")));
		btnDodaj.setMargin(new Insets(0, 2, 0, 2));
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewItem();
			}
		});
		
		chckbxTylkoLekarz = new JCheckBox("Tylko Lekarz");
		chckbxTylkoLekarz.setFont(new Font("Tahoma", Font.PLAIN, 13));
		chckbxTylkoLekarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		GridBagConstraints gbc_chckbxTylkoLekarz = new GridBagConstraints();
		gbc_chckbxTylkoLekarz.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxTylkoLekarz.gridx = 4;
		gbc_chckbxTylkoLekarz.gridy = 0;
		panel_1.add(chckbxTylkoLekarz, gbc_chckbxTylkoLekarz);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 5;
		gbc_btnNewButton.gridy = 0;
		panel_1.add(btnDodaj, gbc_btnNewButton);
		
		JButton btnEdytuj = new JButton("");
		btnEdytuj.setIcon(new ImageIcon(PracownicyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/edit.png")));
		btnEdytuj.setMargin(new Insets(0, 2, 0, 2));
		btnEdytuj.setEnabled(false);
		btnEdytuj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editItem();
			}
		});
		GridBagConstraints gbc_btnE = new GridBagConstraints();
		gbc_btnE.insets = new Insets(0, 0, 5, 5);
		gbc_btnE.gridx = 6;
		gbc_btnE.gridy = 0;
		panel_1.add(btnEdytuj, gbc_btnE);
		
		JButton btnUsun = new JButton("");
		btnUsun.setIcon(new ImageIcon(PracownicyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/del.png")));
		btnUsun.setMargin(new Insets(0, 2, 0, 2));
		btnUsun.setEnabled(false);
		btnUsun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeItem();
			}
		});
		GridBagConstraints gbc_btnU = new GridBagConstraints();
		gbc_btnU.insets = new Insets(0, 0, 5, 0);
		gbc_btnU.gridx = 7;
		gbc_btnU.gridy = 0;
		panel_1.add(btnUsun, gbc_btnU);
		
		JButton btnR = new JButton("");
		btnR.setIcon(new ImageIcon(PracownicyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/refresh2.png")));
		btnR.setMargin(new Insets(0, 2, 0, 2));
		btnR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex()); // wymuszenie przeładowania z repozytorium
			}
		});
		GridBagConstraints gbc_btnR = new GridBagConstraints();
		gbc_btnR.insets = new Insets(0, 0, 0, 5);
		gbc_btnR.gridx = 0;
		gbc_btnR.gridy = 1;
		panel_1.add(btnR, gbc_btnR);
		
		JLabel lblSzukaj = new JLabel("Szukaj");
		lblSzukaj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSzukaj = new GridBagConstraints();
		gbc_lblSzukaj.insets = new Insets(0, 0, 0, 5);
		gbc_lblSzukaj.anchor = GridBagConstraints.EAST;
		gbc_lblSzukaj.gridx = 1;
		gbc_lblSzukaj.gridy = 1;
		panel_1.add(lblSzukaj, gbc_lblSzukaj);
		
		tfSearch = new JTextField();
		tfSearch.setFont(new Font("Tahoma", Font.PLAIN, 13));
		tfSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
		        	setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 3;
		gbc_textField.insets = new Insets(0, 0, 0, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 1;
		panel_1.add(tfSearch, gbc_textField);
		tfSearch.setColumns(10);
		
		JButton btnPodglad = new JButton("Podgląd");
		btnPodglad.setEnabled(false);
		btnPodglad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				viewItem();
			}
		});
		GridBagConstraints gbc_btnPodglad = new GridBagConstraints();
		gbc_btnPodglad.anchor = GridBagConstraints.WEST;
		gbc_btnPodglad.gridwidth = 3;
		gbc_btnPodglad.gridx = 5;
		gbc_btnPodglad.gridy = 1;
		panel_1.add(btnPodglad, gbc_btnPodglad);
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		// praktycznie cały ręcznie robiony model JTable z własnymi podczepieniami
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table); // tego nie da się zrobić przez WindowsBuilder tylko ręcznie
		
		table.setModel(new PracownicyTableModel()); // model danych
    	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); // dostosowanie rozmiarów ręczne
    	table.getTableHeader().setReorderingAllowed(false);
		scrollPane.addComponentListener(new ComponentAdapter() {
			// w procentach opisane rozmiary kolumn 50% = 0.50
			@Override
			public void componentResized(ComponentEvent arg0) {
			    table.getColumnModel().getColumn(0).setPreferredWidth((int)(that.getWidth() * 0.20));
			    table.getColumnModel().getColumn(1).setPreferredWidth((int)(that.getWidth() * 0.20));
			    table.getColumnModel().getColumn(2).setPreferredWidth((int)(that.getWidth() * 0.40));
			    table.getColumnModel().getColumn(3).setPreferredWidth((int)(that.getWidth() * 0.08));
			}
		});
		
		// klinięcie na nagłówek uruchamia sortowanie
	    table.getTableHeader().addMouseListener(new MouseAdapter() {
	        @Override 
	        public void mouseClicked(MouseEvent e) {
	            int col = table.columnAtPoint(e.getPoint());
	            
	            if(cbOrder.getModel().getSize() < (col*2)+2)
	            	return;
	            if(col*2 == cbOrder.getSelectedIndex())
	            	setOrderSort((col*2)+1);
	            else 
	            	setOrderSort(col*2);
	        }
	    });
	    
	    // wybranie/odznaczenie elementu z JTable = blokowanie lub odblokowanie przycisków edycji i usuwania
	    table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if(table.getSelectedRow() > -1) {
	            	btnEdytuj.setEnabled(true);
	            	btnUsun.setEnabled(true);
	            	btnPodglad.setEnabled(true);
				} else {
	            	btnEdytuj.setEnabled(false);
	            	btnUsun.setEnabled(false);
	            	btnPodglad.setEnabled(false);
				}
			}
		});
	    
	    // PopUp menu
		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem menuItemView = new JMenuItem("Zobacz informacje o pracowniku");
		JMenuItem menuItemAdd = new JMenuItem("Dodaj nowego pracownika");
		JMenuItem menuItemEdit = new JMenuItem("Edytuj");
		JMenuItem menuItemRemove = new JMenuItem("Usuń");
		JMenuItem menuItemRefresh = new JMenuItem("Odśwież");
		popupMenu.add(menuItemView);
		popupMenu.add(menuItemAdd);
		popupMenu.add(menuItemEdit);
		popupMenu.add(menuItemRemove);
		popupMenu.add(menuItemRefresh);
		ActionListener popupListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				if(e.getSource() == menuItemView)
					viewItem();
				else if(e.getSource() == menuItemAdd)
					addNewItem();
				else if(e.getSource() == menuItemEdit)
					editItem();
				else if(e.getSource() == menuItemRemove)
					removeItem();
				else if(e.getSource() == menuItemRefresh)
					updateTable();
			}
		};
		menuItemView.addActionListener(popupListener);
		menuItemAdd.addActionListener(popupListener);
		menuItemEdit.addActionListener(popupListener);
		menuItemRemove.addActionListener(popupListener);
		menuItemRefresh.addActionListener(popupListener);
		
		table.addMouseListener(new MouseAdapter() {
	        @Override
			public void mouseClicked(MouseEvent event) {
	            int currentRow = table.rowAtPoint(event.getPoint());
	            table.setRowSelectionInterval(currentRow, currentRow);

	            if (event.getButton() == java.awt.event.MouseEvent.BUTTON1) {
	                if(event.getClickCount() >= 2)
	                	viewItem();
	            }
	            if (event.getButton() == java.awt.event.MouseEvent.BUTTON3) {
	                popupMenu.show(table, event.getX(), event.getY());
	            }
			}
	        
		});
	    // pierwsze przesortowanie informacji z bazy danych i tym samym pobranie ich
	    setOrderSort(cbOrder.getSelectedIndex());
	    
	}
	
	/**
	 * Definicja filtrów sortowania
	 * @param orderNumber
	 */
	private void setOrderSort(int orderNumber) {
		logger.trace(".");
		
		cbOrder.setSelectedIndex(orderNumber);
		
		ArrayList<Order> order = new ArrayList<Order>();
		
        switch(orderNumber) {
        case 0: 
        	order.add(Order.asc("imie"));
        	break;
        case 1:
        	order.add(Order.desc("imie"));
	        break;
        case 2: 
        	order.add(Order.asc("nazwisko"));
        	break;
        case 3:
        	order.add(Order.desc("nazwisko"));
	        break; 
        case 4: 
        	order.add(Order.asc("m.nazwa"));
        	order.add(Order.asc("u.nazwa"));
        	order.add(Order.asc("w.nazwa"));
        	break;
        case 5:
        	order.add(Order.desc("m.nazwa"));
        	order.add(Order.desc("u.nazwa"));
        	order.add(Order.desc("w.nazwa"));
	        break;
        case 6: 
        	order.add(Order.asc("lekarz"));
        	break;
        case 7:
        	order.add(Order.desc("lekarz"));
	        break;  
	        
        }
        repository.setOrderBy(order);
        
        // gdy ciąg wyszukiwania jest > 2 znaki
        if(tfSearch.getText().length() > 2) {
        	repository.setSearchText(tfSearch.getText());
        } else {
        	repository.setSearchText(null);
        }
        
        if(this.chckbxTylkoLekarz.isSelected())
        	repository.setRodzajPracownika(PracownikEnt.TYP_LEKARZ);
        else 
        	repository.setRodzajPracownika(PracownikEnt.TYP_DOWOLNY);
        
		((PracownicyTableModel)table.getModel()).setModelData(repository.getPracownicy()); // przypisanie nowych danych
		logger.trace("SortBy id " + orderNumber);
	}
	
	/**
	 * Wyświetlanie danych pracownika bez możliwości ich edycji
	 */
	public void viewItem() {
		logger.trace("Widok pracownika");
		PracownikEnt pracownik = ((PracownicyTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		
		PracownikDialog pracDialog = new PracownikDialog((Frame)SwingUtilities.getWindowAncestor(this));
		pracDialog.DisplayDialog(pracownik, true);
			
		pracDialog.dispose();
		this.updateTable();
	}
	
	/**
	 * Akcja dodawania nowego elementu wywołana wewnątrz klasy
	 */
	public void addNewItem() {
		addNewItem((Frame)SwingUtilities.getWindowAncestor(this));
		this.updateTable();
	}
	
	/**
	 * Akcja dodawania nowego elementu wywołana z zewnątrz (bez obiektu)
	 * @param parent
	 */
	public static void addNewItem(Frame parent) {
		logger.trace("Dodawanie nowego pracownika lokalizacji");
		PracownikDialog pracDialog = new PracownikDialog(parent);
		pracDialog.DisplayDialog();

		if(pracDialog.result) {
			logger.trace("Próba utworzenia nowego pracownika " + pracDialog.pracownik.getImie() + " " + pracDialog.pracownik.getNazwisko());
			new PracownicyRep().setPracownik(new PracownikEnt(pracDialog.pracownik)).AddPracownik().commit();
		}
			
		pracDialog.dispose();
	}
	
	/**
	 * Edycja zaznaczonego elementu
	 * @param rowNum
	 */
	public void editItem() {
		logger.trace("Edycja pracownika");
		PracownikEnt pracownik = ((PracownicyTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		
		PracownikDialog pracDialog = new PracownikDialog((Frame)SwingUtilities.getWindowAncestor(this));
		pracDialog.DisplayDialog(pracownik);
		
		if(pracDialog.result) {
			logger.trace("Edytowany pracownik" + pracDialog.pracownik.getImie() + " " + pracDialog.pracownik.getNazwisko());
			new PracownicyRep().setPracownik(new PracownikEnt(pracDialog.pracownik)).EditPracownik().commit();;
		}
			
		pracDialog.dispose();
		this.updateTable();
	}
	
	/**
	 * Usuwanie zaznaczonego elementu
	 * @param rowNum
	 */
	public void removeItem() {
		logger.trace("Usuwanie pracownika");
		
		PracownikEnt pracownik = ((PracownicyTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		logger.trace("Usuwanie: " + pracownik.getImie() + " " + pracownik.getNazwisko());
		
		if(JOptionPane.showConfirmDialog((Frame)SwingUtilities.getWindowAncestor(this), 
				"Czy na pewno chesz usunąć " 
						+ pracownik.getImie() + " " + pracownik.getNazwisko()
						+ " z " + pracownik.getUlica().getMiasto().getNazwa(), 
				"Usuwanie pracownika", 
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			new PracownicyRep().setPracownik(new PracownikEnt(pracownik)).DeletePracownik().commit();;
			this.updateTable();
		}
			
	}
	
	/**
	 * Aktualizacja całej zawartości tabeli
	 */
	public void updateTable() {
		((PracownicyTableModel)table.getModel()).setModelData(repository.getPracownicy());
	}
	
	/**
	 * Model danych tabeli
	 */
	public class PracownicyTableModel extends AbstractTableModel {
		 
		private static final long serialVersionUID = 1L;
		
		private ArrayList<PracownikEnt> data = null; // lista danych
		private final Object[] columnNames = {"Imię", "Nazwisko", "Adres", "Lekarz"}; // nazwy w Header
	     
	    private final static int IMIE_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int NAZWISKO_IDX = 1;
	    private final static int ADRES_IDX = 2;
	    private final static int LEKARZ_IDX = 3;
	 
	    public PracownicyTableModel() {}
	     
	    @Override
	    public int getRowCount() {
	        if(data==null) return 0;
	        return data.size();
	    }
	 
	    @Override
	    public int getColumnCount() {
	        return columnNames.length;
	    }
	     
	    @Override // wyciąganie danych z listy
	    public Object getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        PracownikEnt pracownik = data.get(rowIndex);
	        switch (columnIndex) {
	            case IMIE_IDX:
	                return pracownik.getImie();
	            case NAZWISKO_IDX:
	                return pracownik.getNazwisko();
	            case ADRES_IDX:
	                return (
	                		pracownik.getUlica().getMiasto().getNazwa() 
	                		+ " " + pracownik.getUlica().getMiasto().getKod_pocztowy() 
	                		+ " (" + pracownik.getUlica().getMiasto().getWojewodztwo().getNazwa() + ")"
	                		+ ", " + pracownik.getUlica().getNazwa() 
	                		+ " " + pracownik.getBudynek() 
	                		+ " " + pracownik.getMieszkanie());
	            case LEKARZ_IDX:
	                return (pracownik.isLekarz()?"Tak":"Nie");
	            default:
	                return "";
	        }
	    }
	 
	    @Override
	    public String getColumnName(int column) {
	        return columnNames[column].toString();
	    }
	 
	    @Override
	    public boolean isCellEditable(int row, int column) {
	        return false;
	    }
	     
	    // ustawienie przekazanych danych 
	    public void setModelData(ArrayList<PracownikEnt> pracownicy) {
	       this.data =  pracownicy;
	       this.fireTableDataChanged();
	    }
	    
	    public PracownikEnt getDataRecord(int position) {
	        return this.data.get(position);
	    }
	}

}
