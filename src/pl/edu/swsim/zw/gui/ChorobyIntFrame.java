package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.ChorobyRep;
import pl.edu.swsim.zw.entities.ChorobaEnt;

/**
 * Lista chorób w oknie wewnętrzym
 */
public class ChorobyIntFrame extends JInternalFrame {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private ChorobyRep repository = new ChorobyRep();

	/**
	 * Sortowanie
	 */
	private final String[] sortOptions = {
			"Nazwa rosnąco [a-z]",
			"Nazwa malejąco [z-a]",
			"Przewlekła rosnąco [nie-tak]",
			"Przewlekła malejąco [tak-nie]",
	};
	
	private JTextField tfSearch;
	private JTable table;
	private JComboBox<String> cbOrder;

	/**
	 * Create the frame.
	 */
	public ChorobyIntFrame() {
		
		// szybkie obejście this - klasy anonimowe, 
		// nadal dostęp do tej klasy przez that
		ChorobyIntFrame that = this;
				
		setMaximizable(true);
		setTitle("Choroby");
		setResizable(true);
		setClosable(true);
		setBounds(100, 100, 640, 408);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut_1, BorderLayout.EAST);
		
		Component verticalStrut = Box.createVerticalStrut(3);
		panel.add(verticalStrut, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnR = new JButton("");
		btnR.setMargin(new Insets(0, 2, 0, 2));
		btnR.setIcon(new ImageIcon(ChorobyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/refresh2.png")));
		btnR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		GridBagConstraints gbc_btnR = new GridBagConstraints();
		gbc_btnR.insets = new Insets(0, 0, 0, 5);
		gbc_btnR.gridx = 0;
		gbc_btnR.gridy = 0;
		panel_1.add(btnR, gbc_btnR);
		
		JLabel lblSortujWedug = new JLabel("Sortuj wg");
		lblSortujWedug.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSortujWedug = new GridBagConstraints();
		gbc_lblSortujWedug.insets = new Insets(0, 0, 0, 5);
		gbc_lblSortujWedug.anchor = GridBagConstraints.EAST;
		gbc_lblSortujWedug.gridx = 1;
		gbc_lblSortujWedug.gridy = 0;
		panel_1.add(lblSortujWedug, gbc_lblSortujWedug);
		
		
		cbOrder = new JComboBox<String>();
		cbOrder.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cbOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOrderSort(cbOrder.getSelectedIndex()); 
			}
		});
		cbOrder.setModel(new DefaultComboBoxModel<String>(sortOptions));
		GridBagConstraints gbc_cbOrder = new GridBagConstraints();
		gbc_cbOrder.insets = new Insets(0, 0, 0, 5);
		gbc_cbOrder.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbOrder.gridx = 2;
		gbc_cbOrder.gridy = 0;
		panel_1.add(cbOrder, gbc_cbOrder);
		
		JLabel lblSzukaj = new JLabel("Szukaj");
		lblSzukaj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblSzukaj = new GridBagConstraints();
		gbc_lblSzukaj.insets = new Insets(0, 0, 0, 5);
		gbc_lblSzukaj.anchor = GridBagConstraints.EAST;
		gbc_lblSzukaj.gridx = 3;
		gbc_lblSzukaj.gridy = 0;
		panel_1.add(lblSzukaj, gbc_lblSzukaj);
		
		tfSearch = new JTextField();
		tfSearch.setFont(new Font("Tahoma", Font.PLAIN, 13));
		tfSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
		        	setOrderSort(cbOrder.getSelectedIndex());
			}
		});
		GridBagConstraints gbc_tfSearch = new GridBagConstraints();
		gbc_tfSearch.insets = new Insets(0, 0, 0, 5);
		gbc_tfSearch.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfSearch.gridx = 4;
		gbc_tfSearch.gridy = 0;
		panel_1.add(tfSearch, gbc_tfSearch);
		tfSearch.setColumns(10);
		
		JButton btnDodaj = new JButton("");
		btnDodaj.setIcon(new ImageIcon(ChorobyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/add.png")));
		btnDodaj.setMargin(new Insets(0, 2, 0, 2));
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewItem();
			}
		});
		GridBagConstraints gbc_btnDodaj = new GridBagConstraints();
		gbc_btnDodaj.insets = new Insets(0, 0, 0, 5);
		gbc_btnDodaj.gridx = 5;
		gbc_btnDodaj.gridy = 0;
		panel_1.add(btnDodaj, gbc_btnDodaj);
		
		JButton btnEdytuj = new JButton("");
		btnEdytuj.setIcon(new ImageIcon(ChorobyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/edit.png")));
		btnEdytuj.setMargin(new Insets(0, 2, 0, 2));
		btnEdytuj.setEnabled(false);
		btnEdytuj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editItem();
			}
		});
		
		GridBagConstraints gbc_btnEdytuj = new GridBagConstraints();
		gbc_btnEdytuj.insets = new Insets(0, 0, 0, 5);
		gbc_btnEdytuj.gridx = 6;
		gbc_btnEdytuj.gridy = 0;
		panel_1.add(btnEdytuj, gbc_btnEdytuj);
		
		JButton btnUsun = new JButton("");
		btnUsun.setIcon(new ImageIcon(ChorobyIntFrame.class.getResource("/pl/edu/swsim/zw/icon/del.png")));
		btnUsun.setMargin(new Insets(0, 2, 0, 2));
		btnUsun.setEnabled(false);
		btnUsun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeItem();
			}
		});
		GridBagConstraints gbc_btnUsun = new GridBagConstraints();
		gbc_btnUsun.gridx = 7;
		gbc_btnUsun.gridy = 0;
		panel_1.add(btnUsun, gbc_btnUsun);
		
		Component verticalStrut_1 = Box.createVerticalStrut(5);
		panel.add(verticalStrut_1, BorderLayout.SOUTH);
    	
    	JPanel panel_2 = new JPanel();
    	getContentPane().add(panel_2, BorderLayout.CENTER);
    	panel_2.setLayout(new BorderLayout(0, 0));
    	
    	JScrollPane scrollPane = new JScrollPane();
    	panel_2.add(scrollPane, BorderLayout.CENTER);
    	
    	table = new JTable();
    	table.setFont(new Font("Tahoma", Font.PLAIN, 11));
    	table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    	scrollPane.setViewportView(table);
    	table.setModel(new ChorobyTableModel());
    	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    	
    	Component verticalStrut_2 = Box.createVerticalStrut(5);
    	panel_2.add(verticalStrut_2, BorderLayout.NORTH);
    	
    	Component verticalStrut_3 = Box.createVerticalStrut(5);
    	panel_2.add(verticalStrut_3, BorderLayout.SOUTH);
    	
    	Component horizontalStrut_2 = Box.createHorizontalStrut(5);
    	panel_2.add(horizontalStrut_2, BorderLayout.WEST);
    	
    	Component horizontalStrut_3 = Box.createHorizontalStrut(5);
    	panel_2.add(horizontalStrut_3, BorderLayout.EAST);
    	table.getTableHeader().setReorderingAllowed(false);
    	scrollPane.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				// domyślne szerokości kolumn
			    table.getColumnModel().getColumn(0).setPreferredWidth((int)(that.getWidth() * 0.32));
			    table.getColumnModel().getColumn(1).setPreferredWidth((int)(that.getWidth() * 0.12));
			    table.getColumnModel().getColumn(2).setPreferredWidth((int)(that.getWidth() * 0.50));
			}
		});
		
    	// klinięcie w menu sortowania
	    table.getTableHeader().addMouseListener(new MouseAdapter() {
	        @Override
	        public void mouseClicked(MouseEvent e) {
	            int col = table.columnAtPoint(e.getPoint());
	            
	            if(cbOrder.getModel().getSize() < (col*2)+2)
	            	return;
	            if(col*2 == cbOrder.getSelectedIndex())
	            	setOrderSort((col*2)+1);
	            else 
	            	setOrderSort(col*2);
	        }
	    });
	    
	    // aktywacja i deaktywacja przycisków edycji i usuwania
	    table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(table.getSelectedRow() > -1) {
	            	btnEdytuj.setEnabled(true);
	            	btnUsun.setEnabled(true);
				} else {
	            	btnEdytuj.setEnabled(false);
	            	btnUsun.setEnabled(false);
				}
			}
		});


	    // menu kontekstowe
		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem menuItemAdd = new JMenuItem("Dodaj nowy rekord");
		JMenuItem menuItemEdit = new JMenuItem("Edytuj");
		JMenuItem menuItemRemove = new JMenuItem("Usuń");
		JMenuItem menuItemRefresh = new JMenuItem("Odśwież");
		popupMenu.add(menuItemAdd);
		popupMenu.add(menuItemEdit);
		popupMenu.add(menuItemRemove);
		popupMenu.add(menuItemRefresh);
		menuItemAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
	        	addNewItem();
			}
		});
		menuItemEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				editItem();
			}
		});
		menuItemRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				removeItem();
			}
		});
		menuItemRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info(".");
				updateTable();
			}
		});
		
	    // wyświetlenie po przycisnięciu prawego przycisku
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event) {
	            int currentRow = table.rowAtPoint(event.getPoint());
	            table.setRowSelectionInterval(currentRow, currentRow);

	            if (event.getButton() == java.awt.event.MouseEvent.BUTTON3) {
	                popupMenu.show(table, event.getX(), event.getY());
	            }
			}
		});
		
		setOrderSort(cbOrder.getSelectedIndex()); // startowe wyświetlenie z repozytorium

	}
		
	/**
	 * Aktualizuje aktualne opcje sortowania i filtrowania oraz pobiera dane z repozytorium
	 * @param orderNumber
	 */
	private void setOrderSort(int orderNumber) {
		
		logger.trace(".");
		
		cbOrder.setSelectedIndex(orderNumber);
		
		ArrayList<Order> order = new ArrayList<Order>();
		
        switch(orderNumber) {
        case 0: 
        	order.add(Order.asc("nazwa"));
        	break;
        case 1:
        	order.add(Order.desc("nazwa"));
	        break;
        case 2: 
        	order.add(Order.asc("przewlekla"));
        	break;
        case 3:
        	order.add(Order.desc("przewlekla"));
	        break; 
        }
        
        repository.setOrderBy(order);
        
        // gdy ciąg wyszukiwania jest > 2 znaki
        if(tfSearch.getText().length() > 2) {
        	repository.setSearchText(tfSearch.getText());
        } else {
        	repository.setSearchText(null);
        }
        
        this.updateTable();
	}
	
	/**
	 * Aktualizacja tabeli
	 */
	public void updateTable() {
		((ChorobyTableModel)table.getModel()).setModelData(repository.getChoroby());
	}
	
	/**
	 * Akcja dodawania nowego elementu wywołana wewnątrz klasy
	 */
	public void addNewItem() {
		addNewItem((Frame)SwingUtilities.getWindowAncestor(this));
		this.updateTable();
	}
	
	/**
	 * Akcja dodawania nowego elementu wywołana z zewnątrz (bez obiektu)
	 * @param parent
	 */
	public static void addNewItem(Frame parent) {
		logger.trace("Dodawanie nowej choroby");
		ChorobaDialog chorDialog = new ChorobaDialog(parent);
		chorDialog.DisplayDialog();

		if(chorDialog.result) {
			logger.trace("Próba utworzenia nowej choroby " + chorDialog.choroba.getNazwa() );
			new ChorobyRep().setChoroba(chorDialog.choroba).AddChoroba();
		}
			
		chorDialog.dispose();
	}
	
	/**
	 * Edycja zaznaczonego elementu
	 * @param rowNum
	 */
	public void editItem() {
		logger.trace("Edycja choroby");
		ChorobaEnt choroba = ((ChorobyTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
	
		ChorobaDialog chorDialog = new ChorobaDialog((Frame)SwingUtilities.getWindowAncestor(this));
		chorDialog.DisplayDialog(choroba);
		
		if(chorDialog.result) {
			logger.trace("Edytowana choroba" + chorDialog.choroba.getNazwa() );
			new ChorobyRep().setChoroba(new ChorobaEnt(chorDialog.choroba)).EditChoroba();
		}
			
		chorDialog.dispose();
		this.updateTable();
	}
	
	/**
	 * Usuwanie zaznaczonego elementu
	 * @param rowNum
	 */
	public void removeItem() {
		logger.trace("Usuwanie choroby");
		
		ChorobaEnt choroba = ((ChorobyTableModel)table.getModel()).getDataRecord(table.getSelectedRow());
		logger.trace("Usuwanie: " + choroba.getNazwa());
		
		if(JOptionPane.showConfirmDialog((Frame)SwingUtilities.getWindowAncestor(this), 
				"Czy na pewno chesz usunąć " 
						+ choroba.getNazwa(), 
				"Usuwanie choroby", 
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
			ChorobyRep repository = new ChorobyRep().setChoroba(new ChorobaEnt(choroba)).DeleteChoroba();
			
			if(repository == null) {
				if(JOptionPane.showConfirmDialog((Frame)SwingUtilities.getWindowAncestor(this), 
						"Nie udało się usunąć " + choroba.getNazwa() + ". Prawdopodobnie jest używana w którymś z  ", 
						"Usuwanie choroby", 
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					
				}
				
			}
			
			
		}
		this.updateTable();
	}	
	
	/**
	 * Model danych tabeli
	 */
	public class ChorobyTableModel extends AbstractTableModel {
		 
		private static final long serialVersionUID = 1L;
		
		private ArrayList<ChorobaEnt> data = null; // lista danych
		private final Object[] columnNames = {"Nazwa", "Przewlekła", "Opis",}; // nazwy w Header
	     
	    private final static int NAZWA_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int PRZEWLEKLA_IDX = 1;
	    private final static int OPIS_IDX = 2;
	 
	    public ChorobyTableModel() {}
	     
	    @Override
	    public int getRowCount() {
	        if(data==null) return 0;
	        return data.size();
	    }
	 
	    @Override
	    public int getColumnCount() {
	        return columnNames.length;
	    }
	     
	    @Override // wyciąganie danych z listy
	    public Object getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        ChorobaEnt choroba = data.get(rowIndex);
	        switch (columnIndex) {
	            case NAZWA_IDX:
	                return choroba.getNazwa();
	            case PRZEWLEKLA_IDX:
	                return (choroba.isPrzewlekla()?"Tak":"Nie");
	            case OPIS_IDX:
	            	return choroba.getOpis();
	            default:
	                return "";
	        }
	    }
	 
	    @Override
	    public String getColumnName(int column) {
	        return columnNames[column].toString();
	    }
	 
	    @Override
	    public boolean isCellEditable(int row, int column) {
	        return false;
	    }	     
	    
	    // ustawienie przekazanych danych 
	    public void setModelData(ArrayList<ChorobaEnt> choroba) {
	    	logger.trace( choroba.size());
	    	this.data =  choroba;
	    	this.fireTableDataChanged();
	    }
	    
	    public ChorobaEnt getDataRecord(int position) {
	        return this.data.get(position);
	    }
	}
}