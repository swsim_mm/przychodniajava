package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.ChorobyRep;
import pl.edu.swsim.zw.dao.TerminyRep;
import pl.edu.swsim.zw.entities.ChorobaEnt;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.RozpoznanieEnt;
import pl.edu.swsim.zw.entities.TerminEnt;

/**
 * Okno dodawania/modyfikacji rozpoznania
 */
public class RozpoznanieDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	public RozpoznanieEnt rozpoznanie = null;
	public boolean result = false;
	private boolean dialogLocked = false;
	
	public PacjentEnt pacjent = null;
	public ChorobaEnt choroba = null;
	public TerminEnt termin = null;
	
	private ArrayList<TerminEnt> terminyWizyt = new ArrayList<TerminEnt>();
	private ArrayList<ChorobaEnt> choroby = new ArrayList<ChorobaEnt>();
	
	private final JPanel contentPanel = new JPanel();

	private JButton okButton;
	private JButton cancelButton;
	private Component horizontalStrut;
	private Component horizontalStrut_1;
	private Component verticalStrut;
	private Component verticalStrut_1;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JButton btnNewButton;
	private JTextArea taDanePacjenta;
	private JLabel lblChoroba;
	private JComboBox<String> cbChoroba;
	private JLabel lblOpis;
	private JTextArea taOpis;
	private JList<String> listTerminy;

	/**
	 * Create the dialog.
	 */
	public RozpoznanieDialog (Frame parent) {
		super(parent);
		setResizable(false);
		setModal(true);

		setTitle("Rozpoznanie");
		setBounds(100, 100, 630, 426);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.WEST);
		}
		{
			horizontalStrut_1 = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut_1, BorderLayout.EAST);
		}
		{
			verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.SOUTH);
		}
		{
			verticalStrut_1 = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut_1, BorderLayout.NORTH);
		}
		{
			panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 5, 216, 0};
			gbl_panel.rowHeights = new int[]{0, 0};
			gbl_panel.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				panel_1 = new JPanel();
				GridBagConstraints gbc_panel_1 = new GridBagConstraints();
				gbc_panel_1.insets = new Insets(0, 0, 0, 5);
				gbc_panel_1.fill = GridBagConstraints.BOTH;
				gbc_panel_1.gridx = 0;
				gbc_panel_1.gridy = 0;
				panel.add(panel_1, gbc_panel_1);
				GridBagLayout gbl_panel_1 = new GridBagLayout();
				gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0};
				gbl_panel_1.rowHeights = new int[]{0, 77, 0, 0, 0, 0, 0};
				gbl_panel_1.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
				gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
				panel_1.setLayout(gbl_panel_1);
				{
					lblNewLabel_1 = new JLabel("Pacjent");
					lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
					GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
					gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
					gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
					gbc_lblNewLabel_1.gridx = 0;
					gbc_lblNewLabel_1.gridy = 0;
					panel_1.add(lblNewLabel_1, gbc_lblNewLabel_1);
				}
				{
					btnNewButton = new JButton("Wybierz");
					btnNewButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							selectPacjentAction();
						}
					});
					GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
					gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
					gbc_btnNewButton.gridx = 1;
					gbc_btnNewButton.gridy = 0;
					panel_1.add(btnNewButton, gbc_btnNewButton);
				}
				{
					taDanePacjenta = new JTextArea();
					taDanePacjenta.setFont(new Font("Tahoma", Font.PLAIN, 12));
					taDanePacjenta.setBorder(UIManager.getBorder("TextField.border"));
					GridBagConstraints gbc_taDanePacjenta = new GridBagConstraints();
					gbc_taDanePacjenta.insets = new Insets(0, 0, 5, 0);
					gbc_taDanePacjenta.gridwidth = 3;
					gbc_taDanePacjenta.fill = GridBagConstraints.BOTH;
					gbc_taDanePacjenta.gridx = 0;
					gbc_taDanePacjenta.gridy = 1;
					panel_1.add(taDanePacjenta, gbc_taDanePacjenta);
				}
				{
					lblChoroba = new JLabel("Choroba");
					lblChoroba.setFont(new Font("Tahoma", Font.PLAIN, 15));
					GridBagConstraints gbc_lblChoroba = new GridBagConstraints();
					gbc_lblChoroba.gridwidth = 2;
					gbc_lblChoroba.anchor = GridBagConstraints.WEST;
					gbc_lblChoroba.insets = new Insets(0, 0, 5, 5);
					gbc_lblChoroba.gridx = 0;
					gbc_lblChoroba.gridy = 2;
					panel_1.add(lblChoroba, gbc_lblChoroba);
				}
				{
					cbChoroba = new JComboBox<String>();
					GridBagConstraints gbc_cbChoroba = new GridBagConstraints();
					gbc_cbChoroba.insets = new Insets(0, 0, 5, 0);
					gbc_cbChoroba.gridwidth = 3;
					gbc_cbChoroba.fill = GridBagConstraints.HORIZONTAL;
					gbc_cbChoroba.gridx = 0;
					gbc_cbChoroba.gridy = 3;
					panel_1.add(cbChoroba, gbc_cbChoroba);
				}
				{
					lblOpis = new JLabel("Opis");
					lblOpis.setFont(new Font("Tahoma", Font.PLAIN, 15));
					GridBagConstraints gbc_lblOpis = new GridBagConstraints();
					gbc_lblOpis.anchor = GridBagConstraints.WEST;
					gbc_lblOpis.insets = new Insets(0, 0, 5, 5);
					gbc_lblOpis.gridx = 0;
					gbc_lblOpis.gridy = 4;
					panel_1.add(lblOpis, gbc_lblOpis);
				}
				{
					taOpis = new JTextArea();
					taOpis.setBorder(UIManager.getBorder("TextField.border"));
					taOpis.setFont(new Font("Tahoma", Font.PLAIN, 12));
					GridBagConstraints gbc_taOpis = new GridBagConstraints();
					gbc_taOpis.gridwidth = 3;
					gbc_taOpis.fill = GridBagConstraints.BOTH;
					gbc_taOpis.gridx = 0;
					gbc_taOpis.gridy = 5;
					panel_1.add(taOpis, gbc_taOpis);
				}
			}
			{
				panel_3 = new JPanel();
				panel_3.setPreferredSize(new Dimension(2, 10));
				panel_3.setMinimumSize(new Dimension(2, 10));
				panel_3.setBackground(UIManager.getColor("Button.darkShadow"));
				GridBagConstraints gbc_panel_3 = new GridBagConstraints();
				gbc_panel_3.insets = new Insets(0, 0, 0, 5);
				gbc_panel_3.fill = GridBagConstraints.BOTH;
				gbc_panel_3.gridx = 1;
				gbc_panel_3.gridy = 0;
				panel.add(panel_3, gbc_panel_3);
			}
			{
				panel_2 = new JPanel();
				GridBagConstraints gbc_panel_2 = new GridBagConstraints();
				gbc_panel_2.fill = GridBagConstraints.BOTH;
				gbc_panel_2.gridx = 2;
				gbc_panel_2.gridy = 0;
				panel.add(panel_2, gbc_panel_2);
				GridBagLayout gbl_panel_2 = new GridBagLayout();
				gbl_panel_2.columnWidths = new int[]{0, 0};
				gbl_panel_2.rowHeights = new int[]{0, 0, 0};
				gbl_panel_2.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl_panel_2.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
				panel_2.setLayout(gbl_panel_2);
				{
					lblNewLabel = new JLabel("Wizyty");
					lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
					GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
					gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
					gbc_lblNewLabel.gridx = 0;
					gbc_lblNewLabel.gridy = 0;
					panel_2.add(lblNewLabel, gbc_lblNewLabel);
				}
				{
					listTerminy = new JList<String>();
					//-------------------------------------------------------------------------------------------
					listTerminy.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							if(listTerminy.getSelectedIndex() > -1) {
								termin = terminyWizyt.get(listTerminy.getSelectedIndex());
							}
						}
					});
					listTerminy.setBorder(UIManager.getBorder("TextField.border"));
					GridBagConstraints gbc_listTerminy = new GridBagConstraints();
					gbc_listTerminy.fill = GridBagConstraints.BOTH;
					gbc_listTerminy.gridx = 0;
					gbc_listTerminy.gridy = 1;
					panel_2.add(listTerminy, gbc_listTerminy);
				}
			}
		}

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okAction();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelAction();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				cancelAction();
			}
		});
	}

	
	private void setDialogModal() {
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
		//this.setModal(true); --> konstruktor
		this.setVisible(true);
	}
	
	/**
	 * Reguluje formę wyświetlania okna
	 */
	public void DisplayDialog() {
		this.rozpoznanie = new RozpoznanieEnt();
		setTitle("Nowe Rozpoznanie");
		this.lockDialog(false);
		
		this.updateChoroba();
		
		this.setDialogModal();
	}
	
	public void DisplayDialog(RozpoznanieEnt rozpoznanieInit) {
		ReadyData(rozpoznanieInit);
		setTitle("Edycja Rozpoznania: " /*+ rozpoznanieInit.getChoroba().getNazwa()*/);
		this.lockDialog(false);
		this.setDialogModal();
	}
	
	public void DisplayDialog(RozpoznanieEnt rozpoznanieInit,  boolean lockDialog) {
		ReadyData(rozpoznanieInit);
		setTitle("Rozpoznanie: "/* + rozpoznanieInit.getChoroba().getNazwa()*/);
		this.lockDialog(lockDialog);
		this.setDialogModal();
	}
	
	/**
	 * Przygotowanie danych na oknie
	 * @param rozpoznanieInit
	 */
	public void ReadyData(RozpoznanieEnt rozpoznanieInit) {
		logger.trace(".");
		rozpoznanie = rozpoznanieInit; // uwolnieie od instancji sesji Hibernate
		
		this.pacjent = rozpoznanieInit.getWizyta().getPacjent();
		this.presetPacjent(this.pacjent);
		
		this.choroba = rozpoznanieInit.getChoroba();
		this.presetChoroba(this.choroba);
		
		this.taOpis.setText(rozpoznanieInit.getOpis());
		
		this.termin = rozpoznanieInit.getWizyta();
		this.presetTermin(this.termin);

	}
	

	
	
	/**
	 * wybranie przygotwanej choroby (z instiejącej wartości rozpoznania)
	 * @param choroba
	 */
	public void presetChoroba(ChorobaEnt choroba) {
		logger.trace(".");
		updateChoroba();

		if (choroba == null)
			return;

		long id = choroba.getId();

		for (int i = 0; i < choroby.size(); i++) {
			if (choroby.get(i).getId() == id) {
				cbChoroba.setSelectedIndex(i);
				break;
			}
		}

	}

	/**
	 * Aktualizuje listę lekarzy
	 */	
	public void updateChoroba() {
		logger.trace(".");
		((DefaultComboBoxModel<String>) cbChoroba.getModel()).removeAllElements();

		ArrayList<Order> order = new ArrayList<Order>();
		Order.asc("nazwa");

		choroby = new ChorobyRep().setOrderBy(order).getChoroby();
		for (ChorobaEnt rec : choroby) {
			cbChoroba.addItem(rec.getNazwa() + (rec.isPrzewlekla()?" (Przewlekła)":""));
		}
	}
	
	/**
	 * Przesłanie pacjenta przez okno wyboru
	 * @param pacjent
	 */
	public void presetPacjent(PacjentEnt pacjent) {
		logger.trace(".");
		if(pacjent != null) {
			
			logger.trace(pacjent.toString());
			
			String textData = "";
	
			textData += pacjent.getNazwisko() + " " + pacjent.getImie()
					+ System.lineSeparator();
			textData += pacjent.getUlica().getNazwa() + " " + pacjent.getBudynek()
					+ " " + pacjent.getMieszkanie() + System.lineSeparator();
			textData += pacjent.getUlica().getMiasto().getKod_pocztowy() + " "
					+ pacjent.getUlica().getMiasto().getNazwa()
					+ System.lineSeparator();
			textData += pacjent.getUlica().getMiasto().getWojewodztwo().getNazwa();
	
			this.taDanePacjenta.setText(textData);		
		}

	}
	
	/**
	 * Okno wyboru pacjenta 
	 */
	private void selectPacjentAction() {
		logger.trace(".");
		SelectPacjentDialog spd = new SelectPacjentDialog();
		spd.setVisible(true);
		
		if(spd.pacjent != null) {
			presetPacjent(spd.pacjent);
			this.pacjent = spd.pacjent;
		}
		
		updateTerminyList();
	}
	
	
	private void updateTerminyList() {
		logger.trace(".");
		
		DefaultListModel<String> model = new DefaultListModel<String>();
		listTerminy.setModel(model);
		
		if(this.pacjent == null)
			return;
		
		this.terminyWizyt = new TerminyRep().setPacjent(this.pacjent).getTerminy();
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format1;
		
		for(int i=0; i<this.terminyWizyt.size(); i++) {
        	
    		cal.setTime(this.terminyWizyt.get(i).getTermin());
    		format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");	
    		
    		String text = "";
    		text += format1.format(cal.getTime());
    		text += " - ";
    		text += this.terminyWizyt.get(i).isZabieg()?"Zabieg":"Wizyta";
    		
    		model.addElement(text);
		}
		
		//listTerminy.setModel(model);
		
	}
	
	/**
	 * Zaznacznie wcześniejszego wyboru
	 * @param termin
	 */
	private void presetTermin(TerminEnt termin) {
		
		updateTerminyList();
		
		for(int i=0; i<this.terminyWizyt.size(); i++) {
			if(this.terminyWizyt.get(i).getTermin().getTime() == termin.getTermin().getTime()) {
				listTerminy.setSelectedIndex(i);
			}
		}
		
	}
	
	/**
	 * Zatwiedzenie zmian
	 */
	public void okAction() {
		logger.trace(".");
		result = true;
		
		if(dialogLocked) { // dialog tylko do wyświetlenia informacji
			setVisible(false); 
			return; 
		}
		
		Frame parent = (Frame)SwingUtilities.getWindowAncestor(this);
		
		if(this.pacjent == null) {
			JOptionPane.showMessageDialog(parent,
				    "Należy wybrać pacjenta!",
				    "Nie wszystkie dane zostały podane",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("Brak wybranego pacjenta");
			return;
		}
		
		if(this.cbChoroba.getSelectedIndex() == -1) {
			JOptionPane.showMessageDialog(parent,
				    "Należy wybrać chorobę!",
				    "Nie wszystkie dane zostały podane",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("Brak wybranej choroby");
			return;
		}
		
		if(this.termin == null) {
			JOptionPane.showMessageDialog(parent,
				    "Należy wybrać termin!",
				    "Nie wszystkie dane zostały podane",
				    JOptionPane.ERROR_MESSAGE);	
			result = false;
			logger.info("Brak wybranego terminu");
			return;
		}

	
		if(result) {
			
			this.rozpoznanie.setChoroba(this.choroby.get(this.cbChoroba.getSelectedIndex()));
			this.rozpoznanie.setWizyta(this.termin);
			this.rozpoznanie.setOpis(this.taOpis.getText());
		
			setVisible(false);
		}
	}
	
	/**
	 * Blokuje kotrolki edycji jeśli okno jest tylko do podglądu
	 * 
	 * @param doLock
	 */
	private void lockDialog(boolean doLock) {
		this.dialogLocked = doLock;

		boolean enableControl = !doLock;


		this.cancelButton.setEnabled(enableControl);
	}
	
	
	
	public class DateLabelFormatter extends AbstractFormatter {

		private static final long serialVersionUID = 1L;
		private String datePattern = "yyyy-MM-dd";
		private SimpleDateFormat dateFormatter = new SimpleDateFormat(
				datePattern);

		@Override
		public Object stringToValue(String text) throws ParseException {
			return dateFormatter.parseObject(text);
		}

		@Override
		public String valueToString(Object value) throws ParseException {
			if (value != null) {
				Calendar cal = (Calendar) value;
				return dateFormatter.format(cal.getTime());
			}

			return "";
		}
	}

		
	public void cancelAction() {
		result = false;
		setVisible(false);
	}
	
	public boolean isDialogLocked() {
		return dialogLocked;
	}
	
}
