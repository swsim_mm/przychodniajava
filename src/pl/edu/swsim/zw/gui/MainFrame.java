package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.zw.MainApp;
import javax.swing.ImageIcon;

/**
 * Główna ramka (frame) zewnętrzna aplikacji
 * @author Michał Żak & Mariusz Witkowicz
 *
 */
public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private JPanel contentPane;

	public ArrayList<JInternalFrame> internalFrames = new ArrayList<JInternalFrame>();
	private JDesktopPane desktopPane;
	
	/**
	 * Create the frame.
	 */
	public MainFrame() {
		//MainFrame that = this;
		logger.trace(".");
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				MainApp.QuitApp();
			}
		});

		//Frame thisFrame = (Frame)SwingUtilities.getWindowAncestor(that);
		ToolTipManager.sharedInstance().setInitialDelay(1000);

		setTitle("Projekt Aplikacji Bazodanowej - Michał Żak, Mariusz Witkowicz");
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 647, 510);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnPlik = new JMenu("Plik");
		menuBar.add(mnPlik);
		
		JMenuItem mntmZakocz = new JMenuItem("Zakończ");
		mntmZakocz.setIcon(new ImageIcon(MainFrame.class.getResource("/pl/edu/swsim/zw/icon/close.png")));
		mntmZakocz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainApp.QuitApp();
			}
		});
		mnPlik.add(mntmZakocz);
		
		JMenu mnDaneSystemu = new JMenu("Dane systemu");
		menuBar.add(mnDaneSystemu);
		
		JMenuItem mntmLokalizacje = new JMenuItem("Lokalizacje");
		mntmLokalizacje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OpenInternalFrame(LokalizacjeIntFrame.class);
				//OpenNewInternalFrame(new LokalizacjeIntFrame());
			}
		});
		mnDaneSystemu.add(mntmLokalizacje);
		
		JSeparator separator_5 = new JSeparator();
		mnDaneSystemu.add(separator_5);
		
		JMenuItem mnChoroby = new JMenuItem("Choroby");
		mnChoroby.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OpenInternalFrame(ChorobyIntFrame.class);
				//OpenNewInternalFrame(new ChorobyIntFrame());
			}
		});
		mnDaneSystemu.add(mnChoroby);
		
		JMenuItem mnGabinety = new JMenuItem("Gabinety");
		mnGabinety.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OpenInternalFrame(GabinetyIntFrame.class);
			}
		});
		mnDaneSystemu.add(mnGabinety);
		
		JMenuItem mnRodzajeWizyt = new JMenuItem("Rodzaje wizyt");
		mnRodzajeWizyt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpenInternalFrame(RodzajeWizytIntFrame.class);
			}
		});
		
		JSeparator separator_1 = new JSeparator();
		mnDaneSystemu.add(separator_1);
		mnDaneSystemu.add(mnRodzajeWizyt);
		
		JMenuItem mnRodzajeZabiegw = new JMenuItem("Rodzaje zabiegów");
		mnRodzajeZabiegw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpenInternalFrame(RodzajeZabiegowIntFrame.class);
			}
		});
		mnDaneSystemu.add(mnRodzajeZabiegw);
		
		JMenu mnDanePersonalne = new JMenu("Dane personalne");
		menuBar.add(mnDanePersonalne);

		JMenuItem mntmPacjenci_1 = new JMenuItem("Pacjenci");
		mntmPacjenci_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpenInternalFrame(PacjenciIntFrame.class);
			}
		});
		mnDanePersonalne.add(mntmPacjenci_1);
		
		JMenuItem mntmPracownicy_1 = new JMenuItem("Pracownicy");
		mntmPracownicy_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpenInternalFrame(PracownicyIntFrame.class);
			}
		});
		mnDanePersonalne.add(mntmPracownicy_1);
		
		JMenu mnWizyty = new JMenu("Terminy i rozpoznania");
		menuBar.add(mnWizyty);
		
		JMenuItem mntmDziennikWizyt = new JMenuItem("Dziennik terminów");
		mntmDziennikWizyt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OpenInternalFrame(TerminIntFrame.class);
			}
		});
		mnWizyty.add(mntmDziennikWizyt);
		
		JSeparator separator = new JSeparator();
		mnWizyty.add(separator);
		
		JMenuItem mntmRozpoznania = new JMenuItem("Rozpoznania");
		mntmRozpoznania.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OpenInternalFrame(RozpoznaniaIntFrame.class);
			}
		});
		mnWizyty.add(mntmRozpoznania);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		menuBar.add(horizontalGlue);
		
		JMenu mnOProgramie = new JMenu("O Programie");
		mnOProgramie.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				mnOProgramie.setSelected(false);
				OProgramie op = new OProgramie();
				op.setVisible(true);
				op.dispose();
			}
		});
		menuBar.add(mnOProgramie);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		menuBar.add(horizontalStrut);
		
		JMenu menu = new JMenu(" _ ");
		menu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				menu.setSelected(false);
				logger.trace("Przełączenie okna menu");
				JInternalFrame selFrame = desktopPane.getSelectedFrame();
				if(selFrame != null) {
					try {
						if(selFrame.isMaximum())
							selFrame.setMaximum(false);
						else
							selFrame.setMaximum(true);
					} catch (PropertyVetoException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		menuBar.add(menu);
		
		JMenu mnX = new JMenu("");
		mnX.setIcon(new ImageIcon(MainFrame.class.getResource("/pl/edu/swsim/zw/icon/close.png")));
		mnX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				logger.trace("Click zamknij okno menu");
				mnX.setSelected(false);
				JInternalFrame selFrame = desktopPane.getSelectedFrame();
				if(selFrame != null) {
					selFrame.setVisible(false);
				}
			}
		});
		menuBar.add(mnX);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		desktopPane = new JDesktopPane();
		desktopPane.setBackground(SystemColor.controlHighlight);
		contentPane.add(desktopPane);
		desktopPane.setLayout(null);
		
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
	}

	/**
	 * Otwiera samodzielne InternalFrame po wskazanej klasie typu InternalFrame 
	 * @param intFrameClass
	 */
	public void OpenInternalFrame(Class<?> intFrameClass) {
		MainFrame that = this;
		
		JInternalFrame intFrame;

		// zakładane popełnienie błędu przez programistę 
		try {
			
			intFrame = (JInternalFrame) intFrameClass.getConstructor().newInstance();
			
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {

			logger.error("Problem z utworzeniem nowego okna, sprawdz przekazaną klasę");
			logger.error("Te okno nie powstanie");
			e.printStackTrace();
			return;
		}
		
		that.internalFrames.add(intFrame);
		
		this.desktopPane.add(intFrame);
		
		intFrame.setVisible(true);
		
		//intFrame.moveToFront(); // nie działa tak jak powinno

	}
}
