package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.zw.dao.GabinetyRep;
import pl.edu.swsim.zw.entities.GabinetEnt;
import pl.edu.swsim.zw.entities.RodzajZabieguEnt;

/**
 * Okno modyfikacji/dodawania rodzaju zabiegu
 * @author Michał
 *
 */
public class RodzajZabieguDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	public RodzajZabieguEnt rodzajZabiegu = null;
	private ArrayList<GabinetEnt> gabinety = null;
	public boolean result = false;
	private boolean dialogLocked = false;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField tfNazwa;
	private JTextArea taOpis;
	private JCheckBox chckbxRefundacja;
	private JCheckBox chckbxSkierowanie;
	private JButton okButton;
	private JButton cancelButton;
	private JTextField tfRodzaj;
	//private JTextField tfCzas;
	private JSpinner spGodziny;
	private JSpinner spMinuty;
	private JSpinner spPunkty;
	private JTextField ftfCena;
	private JComboBox<String> cbGabinety;
	
	/**
	 * Create the dialog.
	 */
	public RodzajZabieguDialog (Frame parent) {
		super(parent);
		setTitle("Zabieg");
		setModal(true);
		setResizable(false);
		
		setBounds(100, 100, 569, 297);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			Component verticalStrut = Box.createVerticalStrut(12);
			contentPanel.add(verticalStrut, BorderLayout.NORTH);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.SOUTH);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.WEST);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.EAST);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{96, 17, 0, 0, 0, 0, 40, 0};
			gbl_panel.rowHeights = new int[]{26, 0, 0, 0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				JLabel lblNazwa = new JLabel("Nazwa");
				lblNazwa.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNazwa = new GridBagConstraints();
				gbc_lblNazwa.anchor = GridBagConstraints.EAST;
				gbc_lblNazwa.insets = new Insets(0, 0, 5, 5);
				gbc_lblNazwa.gridx = 0;
				gbc_lblNazwa.gridy = 0;
				panel.add(lblNazwa, gbc_lblNazwa);
			}
			{
				tfNazwa = new JTextField();
				tfNazwa.setFont(new Font("Tahoma", Font.PLAIN, 13));
				tfNazwa.setColumns(10);
				GridBagConstraints gbc_tfNazwa = new GridBagConstraints();
				gbc_tfNazwa.insets = new Insets(0, 0, 5, 0);
				gbc_tfNazwa.gridwidth = 6;
				gbc_tfNazwa.anchor = GridBagConstraints.SOUTH;
				gbc_tfNazwa.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfNazwa.gridx = 1;
				gbc_tfNazwa.gridy = 0;
				panel.add(tfNazwa, gbc_tfNazwa);
			}
			{
				JLabel lblCzasTrwania = new JLabel("Czas trwania");
				lblCzasTrwania.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblCzasTrwania = new GridBagConstraints();
				gbc_lblCzasTrwania.anchor = GridBagConstraints.EAST;
				gbc_lblCzasTrwania.insets = new Insets(0, 0, 5, 5);
				gbc_lblCzasTrwania.gridx = 0;
				gbc_lblCzasTrwania.gridy = 1;
				panel.add(lblCzasTrwania, gbc_lblCzasTrwania);
			}
			{
				JPanel panel_1 = new JPanel();
				GridBagConstraints gbc_panel_1 = new GridBagConstraints();
				gbc_panel_1.insets = new Insets(0, 0, 5, 5);
				gbc_panel_1.gridwidth = 3;
				gbc_panel_1.fill = GridBagConstraints.HORIZONTAL;
				gbc_panel_1.gridx = 1;
				gbc_panel_1.gridy = 1;
				panel.add(panel_1, gbc_panel_1);
				GridBagLayout gbl_panel_1 = new GridBagLayout();
				gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0};
				gbl_panel_1.rowHeights = new int[]{0, 0};
				gbl_panel_1.columnWeights = new double[]{1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
				gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
				panel_1.setLayout(gbl_panel_1);
				{
					spGodziny = new JSpinner();
					spGodziny.setFont(new Font("Tahoma", Font.PLAIN, 13));
					GridBagConstraints gbc_spGodziny = new GridBagConstraints();
					gbc_spGodziny.anchor = GridBagConstraints.EAST;
					gbc_spGodziny.insets = new Insets(0, 0, 0, 5);
					gbc_spGodziny.gridx = 0;
					gbc_spGodziny.gridy = 0;
					panel_1.add(spGodziny, gbc_spGodziny);
					spGodziny.setModel(new SpinnerNumberModel(0, 0, 23, 1));
				}
				{
					JLabel lblGodz = new JLabel("godz.");
					lblGodz.setFont(new Font("Tahoma", Font.PLAIN, 13));
					GridBagConstraints gbc_lblGodz = new GridBagConstraints();
					gbc_lblGodz.insets = new Insets(0, 0, 0, 5);
					gbc_lblGodz.gridx = 1;
					gbc_lblGodz.gridy = 0;
					panel_1.add(lblGodz, gbc_lblGodz);
				}
				{
					spMinuty = new JSpinner();
					spMinuty.setFont(new Font("Tahoma", Font.PLAIN, 13));
					GridBagConstraints gbc_spMinuty = new GridBagConstraints();
					gbc_spMinuty.insets = new Insets(0, 0, 0, 5);
					gbc_spMinuty.gridx = 2;
					gbc_spMinuty.gridy = 0;
					panel_1.add(spMinuty, gbc_spMinuty);
					spMinuty.setModel(new SpinnerNumberModel(0, 0, 59, 1));
				}
				{
					JLabel lblNewLabel = new JLabel("min.");
					lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
					GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
					gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
					gbc_lblNewLabel.gridx = 3;
					gbc_lblNewLabel.gridy = 0;
					panel_1.add(lblNewLabel, gbc_lblNewLabel);
				}
			}
			{
				JLabel lblGabinet = new JLabel("Gabinet");
				lblGabinet.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblGabinet = new GridBagConstraints();
				gbc_lblGabinet.anchor = GridBagConstraints.WEST;
				gbc_lblGabinet.insets = new Insets(0, 0, 5, 5);
				gbc_lblGabinet.gridx = 4;
				gbc_lblGabinet.gridy = 1;
				panel.add(lblGabinet, gbc_lblGabinet);
			}
			{
				cbGabinety = new JComboBox<String>();
				cbGabinety.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_cbGabinety = new GridBagConstraints();
				gbc_cbGabinety.insets = new Insets(0, 0, 5, 0);
				gbc_cbGabinety.gridwidth = 2;
				gbc_cbGabinety.fill = GridBagConstraints.HORIZONTAL;
				gbc_cbGabinety.gridx = 5;
				gbc_cbGabinety.gridy = 1;
				panel.add(cbGabinety, gbc_cbGabinety);
			}
			{
				JLabel lblRodzaj = new JLabel("Rodzaj");
				lblRodzaj.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblRodzaj = new GridBagConstraints();
				gbc_lblRodzaj.anchor = GridBagConstraints.EAST;
				gbc_lblRodzaj.insets = new Insets(0, 0, 5, 5);
				gbc_lblRodzaj.gridx = 0;
				gbc_lblRodzaj.gridy = 2;
				panel.add(lblRodzaj, gbc_lblRodzaj);
			}
			{
				tfRodzaj = new JTextField();
				tfRodzaj.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_tfRodzaj = new GridBagConstraints();
				gbc_tfRodzaj.gridwidth = 6;
				gbc_tfRodzaj.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfRodzaj.insets = new Insets(0, 0, 5, 0);
				gbc_tfRodzaj.gridx = 1;
				gbc_tfRodzaj.gridy = 2;
				panel.add(tfRodzaj, gbc_tfRodzaj);
				tfRodzaj.setColumns(10);
			}
			{
				JLabel lblNfz = new JLabel("NFZ");
				lblNfz.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNfz = new GridBagConstraints();
				gbc_lblNfz.anchor = GridBagConstraints.EAST;
				gbc_lblNfz.insets = new Insets(0, 0, 5, 5);
				gbc_lblNfz.gridx = 0;
				gbc_lblNfz.gridy = 3;
				panel.add(lblNfz, gbc_lblNfz);
			}
			{
				chckbxRefundacja = new JCheckBox("Refundacja");
				chckbxRefundacja.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_chckbxRefundacja = new GridBagConstraints();
				gbc_chckbxRefundacja.insets = new Insets(0, 0, 5, 5);
				gbc_chckbxRefundacja.anchor = GridBagConstraints.NORTHWEST;
				gbc_chckbxRefundacja.gridx = 1;
				gbc_chckbxRefundacja.gridy = 3;
				panel.add(chckbxRefundacja, gbc_chckbxRefundacja);
			}
			{
				JLabel lblPunkty = new JLabel("Punkty");
				lblPunkty.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblPunkty = new GridBagConstraints();
				gbc_lblPunkty.anchor = GridBagConstraints.EAST;
				gbc_lblPunkty.insets = new Insets(0, 0, 5, 5);
				gbc_lblPunkty.gridx = 3;
				gbc_lblPunkty.gridy = 3;
				panel.add(lblPunkty, gbc_lblPunkty);
			}
			{
				spPunkty = new JSpinner();
				spPunkty.setFont(new Font("Tahoma", Font.PLAIN, 13));
				spPunkty.setModel(new SpinnerNumberModel(0, 0, 1000000, 1));
				GridBagConstraints gbc_spPunkty = new GridBagConstraints();
				gbc_spPunkty.gridwidth = 2;
				gbc_spPunkty.anchor = GridBagConstraints.WEST;
				gbc_spPunkty.insets = new Insets(0, 0, 5, 5);
				gbc_spPunkty.gridx = 4;
				gbc_spPunkty.gridy = 3;
				panel.add(spPunkty, gbc_spPunkty);
			}
			{
				JLabel lblCena = new JLabel("Cena");
				lblCena.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblCena = new GridBagConstraints();
				gbc_lblCena.anchor = GridBagConstraints.EAST;
				gbc_lblCena.insets = new Insets(0, 0, 5, 5);
				gbc_lblCena.gridx = 0;
				gbc_lblCena.gridy = 4;
				panel.add(lblCena, gbc_lblCena);
			}
			{			
				ftfCena = new JTextField();
				ftfCena.setFont(new Font("Tahoma", Font.PLAIN, 13));
				ftfCena.setHorizontalAlignment(SwingConstants.RIGHT);
				
				GridBagConstraints gbc_ftfCena = new GridBagConstraints();
				gbc_ftfCena.fill = GridBagConstraints.HORIZONTAL;
				gbc_ftfCena.insets = new Insets(0, 0, 5, 5);
				gbc_ftfCena.gridx = 1;
				gbc_ftfCena.gridy = 4;
				panel.add(ftfCena, gbc_ftfCena);
			}
			{
				JLabel lblNewLabel_1 = new JLabel("zł");
				lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
				gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_1.gridx = 2;
				gbc_lblNewLabel_1.gridy = 4;
				panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
			}
			{
				chckbxSkierowanie = new JCheckBox("Wym. Skierowanie");
				chckbxSkierowanie.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_chckbxSkierowanie = new GridBagConstraints();
				gbc_chckbxSkierowanie.insets = new Insets(0, 0, 5, 0);
				gbc_chckbxSkierowanie.gridwidth = 3;
				gbc_chckbxSkierowanie.anchor = GridBagConstraints.WEST;
				gbc_chckbxSkierowanie.gridx = 4;
				gbc_chckbxSkierowanie.gridy = 4;
				panel.add(chckbxSkierowanie, gbc_chckbxSkierowanie);
			}
			{
				JLabel lblOpis = new JLabel("Opis");
				lblOpis.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblOpis = new GridBagConstraints();
				gbc_lblOpis.anchor = GridBagConstraints.NORTHEAST;
				gbc_lblOpis.insets = new Insets(0, 0, 0, 5);
				gbc_lblOpis.gridx = 0;
				gbc_lblOpis.gridy = 5;
				panel.add(lblOpis, gbc_lblOpis);
			}
			{
				taOpis = new JTextArea();
				taOpis.setFont(new Font("Tahoma", Font.PLAIN, 13));
				taOpis.setBorder(UIManager.getBorder("TextField.border"));
				taOpis.setLineWrap(true);
				taOpis.setWrapStyleWord(true);
				GridBagConstraints gbc_taOpis = new GridBagConstraints();
				gbc_taOpis.gridwidth = 6;
				gbc_taOpis.fill = GridBagConstraints.BOTH;
				gbc_taOpis.gridx = 1;
				gbc_taOpis.gridy = 5;
				panel.add(taOpis, gbc_taOpis);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okAction();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelAction();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
			
	}
	
	/**
	 * Reguluje formę wyświetlania okna
	 */	
	public void DisplayDialog() {
		this.rodzajZabiegu = new RodzajZabieguEnt();
		setTitle("Nowy rodzaj zabiegu");
		presetGabinet(null);
		this.lockDialog(false);
		this.setDialogModal();
	}
	
	public void DisplayDialog(RodzajZabieguEnt zabiegInit) {
		ReadyData(zabiegInit);
		setTitle("Edycja zabiegu " + zabiegInit.getNazwa());
		this.lockDialog(false);
		this.setDialogModal();
	}
	
	public void DisplayDialog(RodzajZabieguEnt zabiegInit, boolean lockDialog) {
		ReadyData(zabiegInit);
		setTitle("Podgląd Zbiegu " + zabiegInit.getNazwa());
		this.lockDialog(lockDialog);
		this.setDialogModal();
	}
	
	private void setDialogModal() {
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
		//this.setModal(true); --> konstruktor
		this.setVisible(true);
	}

	/**
	 * Przygotowanie okna z danymi
	 * @param zabiegInit
	 */
	public void ReadyData(RodzajZabieguEnt zabiegInit) {
		logger.trace(".");
		rodzajZabiegu = new RodzajZabieguEnt(zabiegInit); // uwolnieie od instancji sesji Hibernate
				
		this.tfNazwa.setText(rodzajZabiegu.getNazwa());
		this.taOpis.setText(rodzajZabiegu.getOpis());
		this.tfRodzaj.setText(rodzajZabiegu.getRodzaj());
			
		this.ftfCena.setText( String.format("%.2f", rodzajZabiegu.getCena()));
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(zabiegInit.getCzastrwania().getTime()));
		this.spGodziny.setValue(cal.get(Calendar.HOUR_OF_DAY));
		this.spMinuty.setValue(cal.get(Calendar.MINUTE));
		//this.tfPunkty.setText(Integer.toString(rodzajZabiegu.getPunkty()));
	
		this.spPunkty.setValue(rodzajZabiegu.getPunkty());
		this.chckbxRefundacja.setSelected(rodzajZabiegu.isRefundacja());	
		this.chckbxSkierowanie.setSelected(rodzajZabiegu.isSkierowanie());
		
		presetGabinet(rodzajZabiegu.getGabinet());
		
	}
	
	/**
	 * Wybranie odpowiedniego gabinetu
	 * @param gabinet
	 */
	private void presetGabinet(GabinetEnt gabinet) {

		updateGabinety();

		if (gabinet == null)
			return;

		long pracID = gabinet.getId();

		for (int i = 0; i < gabinety.size(); i++) {
			if (gabinety.get(i).getId() == pracID) {
				cbGabinety.setSelectedIndex(i);
				break;
			}
		}

	}

	/** 
	 * Aktualizacja listy gabinetów
	 */
	public void updateGabinety() {
		((DefaultComboBoxModel<String>) cbGabinety.getModel())
				.removeAllElements();

		ArrayList<Order> order = new ArrayList<Order>();
		Order.asc("numer");

		gabinety = new GabinetyRep().setOrderBy(order).getGabinety();
		for (GabinetEnt gab : gabinety) {
			cbGabinety.addItem("(" + gab.getNumer() + ") " + gab.getNazwa());
		}
	}

	/**
	 * Zatwierdzenie zmian
	 */
	public void okAction() {
		
		if(dialogLocked) { // dialog tylko do wyświetlenia informacji
			setVisible(false);
			return;
		}
		
		result = true;
		Frame parent = (Frame)SwingUtilities.getWindowAncestor(this);
		
		if(tfNazwa.getText().length()<1) {
			// okno uzupelnienie danych
			JOptionPane.showMessageDialog(parent,
				    "Nazwa musi zostać podna!",
				    "Brak zawartości pól",
				    JOptionPane.ERROR_MESSAGE);	
			logger.info("brak zawartości pól");
			result = false;
			return;
		}
				
		if(this.chckbxRefundacja.isSelected() && (int)this.spPunkty.getValue() <= 0) {
			// okno uzupelnienie danych
			JOptionPane.showMessageDialog(parent,
				    "Jeśli zabieg jest refundowany konieczne jest określenie ilości punktów",
				    "Brak zawartości pola punktów",
				    JOptionPane.ERROR_MESSAGE);	
			logger.info("brak zawartości pola punktów");
			result = false;
			return;
		}

		if(result) {
			rodzajZabiegu.setNazwa(this.tfNazwa.getText());
			rodzajZabiegu.setOpis(this.taOpis.getText());
			rodzajZabiegu.setRodzaj(this.tfRodzaj.getText());
			
			rodzajZabiegu.setRefundacja(this.chckbxRefundacja.isSelected());
			rodzajZabiegu.setSkierowanie(this.chckbxSkierowanie.isSelected());
			
			rodzajZabiegu.setPunkty((int)this.spPunkty.getValue());
					
			float cena;
			try {
				cena = Float.parseFloat(this.ftfCena.getText().replace(',', '.'));
			} catch(Exception e) {
				cena = 0;
			}
			rodzajZabiegu.setCena(cena);
			
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, (int)this.spGodziny.getValue());
			cal.set(Calendar.MINUTE, (int)this.spMinuty.getValue());
			cal.set(Calendar.SECOND, 0);
			rodzajZabiegu.setCzastrwania(new java.sql.Time(cal.getTimeInMillis()));
			
			rodzajZabiegu.setGabinet(gabinety.get(this.cbGabinety.getSelectedIndex()));

			setVisible(false);
		}
		
	}
		
	public void cancelAction() {
		result = false;
		setVisible(false);
	}
	
	/**
	 * Podgląd
	 * @param doLock
	 */
	private void lockDialog(boolean doLock) {
		this.dialogLocked = doLock;
		
		boolean enableControl = !doLock;
		
		this.tfNazwa.setEditable(enableControl);
		this.tfRodzaj.setEditable(enableControl);
		this.taOpis.setEditable(enableControl);
		
		this.spGodziny.setEnabled(enableControl);
		this.spMinuty.setEnabled(enableControl);
		
		this.chckbxRefundacja.setEnabled(enableControl);
		this.chckbxSkierowanie.setEnabled(enableControl);
		
		this.ftfCena.setEditable(enableControl);
		this.spPunkty.setEnabled(enableControl);
		
		this.cbGabinety.setEnabled(enableControl);
		
		this.cancelButton.setEnabled(enableControl);

	}
	
}