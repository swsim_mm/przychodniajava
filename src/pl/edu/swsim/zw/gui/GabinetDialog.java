package pl.edu.swsim.zw.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.zw.entities.GabinetEnt;
import java.awt.Font;

/**
 * Okno dodawania, edycji oraz podglądu gabinetu
 */
public class GabinetDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	public GabinetEnt gabinet = null;
	public boolean result = false;

	private final JPanel contentPanel = new JPanel();
	private JTextField tfNazwa;
	private JTextField tfNumer;

	/**
	 * Create the dialog.
	 */
	public GabinetDialog(Frame parent) {
		super(parent);
		setTitle("Gabinet");
		setResizable(false);
			
		this.setModal(true);
		setBounds(100, 100, 450, 135);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.WEST);
		}
		{
			Component horizontalStrut = Box.createHorizontalStrut(5);
			contentPanel.add(horizontalStrut, BorderLayout.EAST);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.NORTH);
		}
		{
			Component verticalStrut = Box.createVerticalStrut(5);
			contentPanel.add(verticalStrut, BorderLayout.SOUTH);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				JLabel lblNumer = new JLabel("Numer");
				lblNumer.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNumer = new GridBagConstraints();
				gbc_lblNumer.anchor = GridBagConstraints.EAST;
				gbc_lblNumer.insets = new Insets(0, 0, 5, 5);
				gbc_lblNumer.gridx = 0;
				gbc_lblNumer.gridy = 0;
				panel.add(lblNumer, gbc_lblNumer);
			}
			{
				tfNumer = new JTextField();
				GridBagConstraints gbc_tfNumer = new GridBagConstraints();
				gbc_tfNumer.insets = new Insets(0, 0, 5, 5);
				gbc_tfNumer.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfNumer.gridx = 1;
				gbc_tfNumer.gridy = 0;
				panel.add(tfNumer, gbc_tfNumer);
				tfNumer.setColumns(64);
			}
			{
				JLabel lblNazwa = new JLabel("Nazwa");
				lblNazwa.setFont(new Font("Tahoma", Font.PLAIN, 13));
				GridBagConstraints gbc_lblNazwa = new GridBagConstraints();
				gbc_lblNazwa.anchor = GridBagConstraints.EAST;
				gbc_lblNazwa.insets = new Insets(0, 0, 0, 5);
				gbc_lblNazwa.gridx = 0;
				gbc_lblNazwa.gridy = 1;
				panel.add(lblNazwa, gbc_lblNazwa);
			}
			{
				tfNazwa = new JTextField();
				GridBagConstraints gbc_tfNazwa = new GridBagConstraints();
				gbc_tfNazwa.gridwidth = 2;
				gbc_tfNazwa.insets = new Insets(0, 0, 0, 5);
				gbc_tfNazwa.fill = GridBagConstraints.HORIZONTAL;
				gbc_tfNazwa.gridx = 1;
				gbc_tfNazwa.gridy = 1;
				panel.add(tfNazwa, gbc_tfNazwa);
				tfNazwa.setColumns(64);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okAction();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelAction();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		this.setLocationRelativeTo(null); 
		
	}
	
	public void ReadyData(GabinetEnt gabinetInit) {
		logger.trace(".");
				
		this.gabinet = gabinetInit;
		this.tfNazwa.setText(gabinetInit.getNazwa());	
		this.tfNumer.setText(gabinetInit.getNumer());

	}
	
	private void setDialogModal() {
		this.setLocationRelativeTo(null); // wyśrodkowanie okna
		//this.setModal(true); --> konstruktor
		this.setVisible(true);
		
	}
	
	/**
	 * Reguluje formę wyświetlania okna
	 */	
	public void DisplayDialog() {
		this.gabinet = new GabinetEnt();
		setTitle("Nowy Gabinet");
		this.setDialogModal();
	}
	
	public void DisplayDialog(GabinetEnt gabinetInit) {
		ReadyData(gabinetInit);
		setTitle(gabinetInit.getNumer() + " " + gabinetInit.getNazwa() + " (Gabinet)");
		this.setDialogModal();
	}
	
	/**
	 * Zatwierdzenie
	 */
	public void okAction() {
		result = true;
		Frame parent = (Frame)SwingUtilities.getWindowAncestor(this);
		
		if(tfNazwa.getText().length()<1 
				|| tfNumer.getText().length()<1 ) {
			// okno uzupelnienie danych
			JOptionPane.showMessageDialog(parent,
				    "Wszystkie pola muszą zostać uzupełnione!",
				    "Brak zawartości pól",
				    JOptionPane.ERROR_MESSAGE);	
			logger.info("brak zawartości pól");
			result = false;
			return;
		}		
		
		if(result) {
			this.gabinet.setNazwa(this.tfNazwa.getText());
			this.gabinet.setNumer(this.tfNumer.getText());
			setVisible(false);
		}
	}
	
	/**
	 * Anulowanie
	 */
	public void cancelAction() {
		result = false;
		setVisible(false);
	}
}
