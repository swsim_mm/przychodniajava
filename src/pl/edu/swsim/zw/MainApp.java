package pl.edu.swsim.zw;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.zw.gui.MainFrame;
import pl.edu.swsim.zw.mutils.MyEventQueue;

/**
 * 
 * @author Michał Żak & Mariusz Witkowicz
 *
 */
public class MainApp {
	// uniwersalny sposób wydostania klasy i przesłania do loggera aktualnej klasy ;)
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	
	public static MainFrame mainFrame;

	public static void main(String[] args) {
		logger.info(".");
		
		// TODO GUI ładowania - łączenia z bazą :D	
		
		if(HibernateFactory.getSession() == null) {
			logger.fatal("Nie można połaczyć się z bazą danych - błąd konfiguracji lub niedostępna baza.");
			logger.fatal("Zamykanie aplikacji.");
			
			JOptionPane.showMessageDialog(null,
				    "Brak połaczenia z bazą danych.\n\nSprawdz konfigurację bazy danych oraz jej status.",
				    "Brak połaczenia z bazą danych.",
				    JOptionPane.ERROR_MESSAGE);
			
			return;
		}
		
		logger.trace("Uruchomienie okna głównego");
		
		// Defaultowy sposób wyświetlenia okna
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Toolkit.getDefaultToolkit().getSystemEventQueue().push(new MyEventQueue()); 
					MainApp.mainFrame = new MainFrame();
					MainApp.mainFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}
	
	/**
	 * Kontrolowany sposób opuszczenia aplikacji z zamknięciem bazy danych
	 */
	public static void QuitApp() {
		
		MainApp.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MainApp.mainFrame.setVisible(false);
		HibernateFactory.close();
		System.exit(0);
		
	}

}
